package de.derplaner.functions;

import android.os.StrictMode;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Ice on 01.12.2015.
 */
public class UrlReader {

    public Boolean ReadURL(String... urls) {
        Date dt = new Date();

        if(dt.getDay() < 6) {
            if (dt.getHours() > 17) {
                try {
                    //startet automatisch am nächsten Tag 13 Uhr per Service
                    return false;
                } catch (Exception ex) {

                }
            } else if(dt.getHours() < 13){
                try {
                    //startet 13 Uhr
                    Thread.currentThread().sleep((780 - (dt.getHours() * 60) - dt.getMinutes()) * 60 * 1000);
                    ReadURL();
                } catch (Exception ex) {
                    //Toast.makeText(getApplicationContext(), ex.getMessage().toString(), Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            return false;
        }

        HttpResponse response = null;
        HttpGet httpGet = null;
        HttpClient mHttpClient = null;
        String s = "";

        try {
            if (mHttpClient == null) {
                mHttpClient = new DefaultHttpClient();
            }


            httpGet = new HttpGet(urls[0]);

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);


            response = mHttpClient.execute(httpGet);
            s = EntityUtils.toString(response.getEntity(), "UTF-8");


        } catch (IOException e) {
            e.printStackTrace();
        }

        Calendar c = Calendar.getInstance();

        switch(dt.getDay()){
            case 1:
            case 2:
            case 3:
            case 4:
                c.add(Calendar.DATE, 1);
                dt = c.getTime();
                break;
            case 5:
                c.add(Calendar.DATE, 3);
                dt = c.getTime();
                break;
        }

        SimpleDateFormat dateFormatDate = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
        SimpleDateFormat dateFormatDay = new SimpleDateFormat("cc", Locale.GERMANY);
        String datum = dateFormatDate.format(dt);
        String day = dateFormatDay.format(dt);
        String sURL = "";

        try{
            datum = datum.substring(0, 2).toString() + "-" + datum.substring(3, 5)+" "+day;
        }
        catch (Exception ex)
        {
            long i = 0;
        }

        int iPos = s.indexOf("/images/Vertretungsplan/Huegelschule/H "+datum+".pdf");

        //erneuter Aufruf bis um 17 Uhr o. der Vertretungslan online ist
        if (iPos > 0) {
            return true;
        }
        else {
            try{
                Thread.sleep(60 * 5000);
                ReadURL();
            }
            catch (Exception ex){
                //Helper
            }
        }
        return true;
    }
}
