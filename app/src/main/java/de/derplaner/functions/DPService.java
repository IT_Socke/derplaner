package de.derplaner.functions;

import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.derplaner.R;
import de.derplaner.SQLite.SQL_PlanerDataSource;
import de.derplaner.dataObjects.DS_StudIP;

public class DPService extends Service {
    // Deklaration
    Boolean                 bHelper;
    SQL_PlanerDataSource    ds;
    ThreadStudIP            myStud;
    Thread                  thStud;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {
        // Initialisierung
        ds      = new SQL_PlanerDataSource(getApplicationContext());
        myStud  = new ThreadStudIP();
        thStud  = new Thread(myStud);

        // Thread starten
        thStud.start();

        //Async Aufruf Erinnerung an Termine
        /*if(ds.getOneEntries_SERVICE("RemindDate").equals("TRUE"))
            new AsyncTaskRemindExecute().execute();

        //Async Aufruf AGS Vertretungsplan
        if(ds.getOneEntries_SERVICE("AGSstudent").equals("TRUE"))
            new AsyncTaskAGSExecute().execute();*/
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Service automatisch neustarten
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        thStud.interrupt();
        stopSelf();
    }

    //Async Class for AGS Students
    private class AsyncTaskRemindExecute extends AsyncTask<String, Context, String> {
        @Override
        protected String doInBackground(String... urls) {
            // Deklaration
            Notification            notify;
            NotificationManager     notiManage;
            PendingIntent           pending;
            long                    interval;

            // wird ausgefuehrt sobald ein Termin am naechsten Tag ist
            if (bHelper) {
                // Thread pausieren bis 17 Uhr
                try {
                    // Entscheidung ob es vor 17 Uhr ist
                    if(System.currentTimeMillis() - (1000*60*60*17) > 0)
                        interval = System.currentTimeMillis() - (1000*60*60*17);
                    else
                        interval = 0;

                    Thread.sleep(interval);
                }
                catch (Exception ex){
                    // do nothing and exit
                }

                notiManage  = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                pending     = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 0);

                // die Notification zusammensetzen
                notify = new NotificationCompat.Builder(DPService.this)
                        .setContentTitle("Termin")
                        .setContentText("Sie haben Morgen einen Termin!")
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentIntent(pending)
                        .build();

                // die Notification anzeigen
                notify.flags |= Notification.FLAG_NO_CLEAR;
                notiManage.notify(0, notify);
            }

            return null;
        }
    }

    //Async Class for AGS Students
    private class AsyncTaskAGSExecute extends AsyncTask<String, Context, String> {
        @Override
        protected String doInBackground(String... urls) {
            // Deklaration
            String url = "http://www.ags-erfurt.de/de/vertretungsplan";
            UrlReader serviceUrlReader = new UrlReader();
            bHelper =  serviceUrlReader.ReadURL(url);

            Notification            notify;
            NotificationManager     notiManage;
            PendingIntent           pending;

            if(bHelper) {
                notiManage  = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                pending     = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 0);

                // die Notification zusammensetzen
                notify = new NotificationCompat.Builder(DPService.this)
                        .setContentTitle("AGS")
                        .setContentText("Vertretungsplan online!")
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentIntent(pending)
                        .build();

                // die Notification anzeigen
                notify.flags |= Notification.FLAG_NO_CLEAR;
                notiManage.notify(1, notify);
            }
            return null;
        }
    }

    //Async Class for AGS Students
    private class ThreadStudIP implements Runnable {
        @Override
        public  void run() {
            // Deklaration:
            final ArrayList<DS_StudIP> lEvents = new ArrayList<>();
            String sHelp, sHelp2, sInterval, sDate, path, line;
            URL u;
            DS_StudIP evItem;
            HttpURLConnection c;
            BufferedReader r;
            long lInterval;


            // Initialisierung TODO noch von Nöten
            sHelp       = ds.SETTINGS_getOneEntries("STUDIP");
            sHelp2      = ds.SETTINGS_getOneEntries("STUDIPHELP");

            //
            if(sHelp != null) {
                while (sHelp.equals("TRUE")) {
                    // Initialiserung
                    sInterval = ds.SETTINGS_getOneEntries("STUDIPINTERVAL");

                    if(!sHelp2.equals("TRUE")) {
                        // Deklaration & Initialisierung
                        SimpleDateFormat sdfRebuild = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss", Locale.GERMAN);
                        Date dRebuild;
                        long lSleep;
                        String sDateRebu;

                        // Initialiserung
                        Calendar cal1 = Calendar.getInstance();
                        Calendar cal2 = Calendar.getInstance();
                        sDateRebu = ds.SETTINGS_getOneEntries("TIMESTUDIP");

                        // Datum konvertieren
                        try {
                            dRebuild = sdfRebuild.parse(sDateRebu);
                        } catch (ParseException e) {
                            dRebuild = Calendar.getInstance().getTime();
                        }

                        // Calendertime setzen
                        cal2.setTime(dRebuild);

                        // Find Sleeptime
                        if (cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR)) {
                            lInterval = Long.parseLong(sInterval);
                            lSleep = (lInterval * 1000 * 60) - (cal1.getTimeInMillis() - cal2.getTimeInMillis());
                        } else {
                            // TODO wurde nicht getestet
                            lInterval = Long.parseLong(sInterval);
                            lSleep = (lInterval * 1000 * 60) -
                                    ((cal1.getTimeInMillis() * (cal2.get(Calendar.DAY_OF_YEAR) - cal1.get(Calendar.DAY_OF_YEAR)))
                                            - cal2.getTimeInMillis());
                        }

                        // Thread sleep for next
                        if (lSleep >= 10000) {
                            try {
                                Thread.sleep(lSleep);
                            } catch (InterruptedException e) {
                                return;
                            }
                        }
                    }
                    else
                        ds.SETTINGS_UpdateOrCreateEntry("STUDIPHELP", "FALSE");

                    // Termine auslesen
                    try {
                        // Initialisierung
                        path        = ds.SETTINGS_getOneEntries("STUDIPLINK");
                        evItem      = new DS_StudIP();
                        u           = new URL(path);
                        c           = (HttpURLConnection) u.openConnection();
                        c.setRequestMethod("GET");
                        c.connect();
                        r           = new BufferedReader(new InputStreamReader(c.getInputStream()));

                        // Bufferreader auslesen
                        while ((line = r.readLine()) != null) {
                            if (line.contains("BEGIN:VEVENT"))
                                evItem = new DS_StudIP();
                            else if (line.contains("SUMMARY:")) {
                                String test = line.substring(line.indexOf(":") + 1);
                                test = test.substring(test.lastIndexOf("\\\\") + 3);
                                evItem.setSubject(test);
                            }
                            else if (line.contains("LOCATION:"))
                                evItem.setRoom(line.substring(line.indexOf(":") + 1));
                            else if (line.contains("DTSTART;")) {
                                evItem.setDay(Integer.parseInt(line.substring(line.indexOf(":") + 1, line.indexOf(":") + 9)));
                                evItem.setStartTime(Integer.parseInt(line.substring(line.length() - 6, line.length() - 2)));
                            } else if (line.contains("DTEND;")) {
                                //evItem.dateEnd = line.substring(line.indexOf(":")+1, line.indexOf(":") + 9);
                                evItem.setEndTime(Integer.parseInt(line.substring(line.length() - 6, line.length() - 2)));
                            } else if (line.contains("END:VEVENT")) {
                                if (!evItem.getRoom().isEmpty())
                                    lEvents.add(evItem);
                            }
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (ProtocolException e) {
                        //e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // Termine in DB speichern
                    for (DS_StudIP item : lEvents) {
                        ds.STUDIP_UpdateOrCreateEntry(item);
                    }

                    // Speicherzeit in DB speichern
                    sDate = (new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss", Locale.GERMAN).format(Calendar.getInstance().getTime()));
                    ds.SETTINGS_UpdateOrCreateEntry("TIMESTUDIP", sDate);

                    // Thread bis naechsten Durchlauf
                    try {
                        lInterval = Long.parseLong(sInterval);
                        lInterval = lInterval * (1000 * 60);
                        Thread.sleep(lInterval);
                        ds.SETTINGS_UpdateOrCreateEntry("STUDIPHELP", "TRUE");
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        }
    }
}
