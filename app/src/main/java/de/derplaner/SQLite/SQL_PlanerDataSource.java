package de.derplaner.SQLite;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import de.derplaner.R;
import de.derplaner.dataObjects.DS_Child;
import de.derplaner.dataObjects.DS_Event;
import de.derplaner.dataObjects.DS_Grade;
import de.derplaner.dataObjects.DS_Hour;
import de.derplaner.dataObjects.DS_Subject;
import de.derplaner.dataObjects.DS_NewTimes;
import de.derplaner.dataObjects.DS_Setting;
import de.derplaner.dataObjects.DS_StudIP;
import de.derplaner.dataObjects.DS_TimeEdit;
import de.derplaner.dataObjects.Event_DSs;


public class SQL_PlanerDataSource {
    // Globale Variablen
    private Context cont;
    // Konstruktor
    public SQL_PlanerDataSource(Context context) {
        this.cont = context;
        dbHelper = new SQL_MySQLiteHelper(context);
    }

    // Deklaration
    private SQLiteDatabase database;
    private SQL_MySQLiteHelper dbHelper;

    // Spaltenabfragen
    private String[] allColumns_SUBJECTS = { "ID", "CHILD_ID", "SUBJECT", "NC", "WISH_NC", "FINAL_GRADE", "WERTUNG", "CLASS"};
    private String[] allColumns_GRADES = {"ID", "CHILD_ID", "SUBJECT", "THEMA", "TEACHER", "DATE", "GRADE", "INFO"};
    private String[] allColumns_HOURS = {"ID", "CHILD_ID", "SUBJECT", "SHORT", "ROOM", "TEACHER", "HOUR", "DAY", "COLOR", "WEEK"};
    private String[] allColumns_STUDIP = {"ID", "CHILD_ID", "SUBJECT", "ROOM", "STARTTIME", "ENDTIME", "DAY"};
    private String[] allColumns_EDITTIMES = {"ID", "HOUR", "FROM_TIME", "TO_TIME"};
    private String[] allColumns_SETTINGS = {"ID", "NAME", "VALUE"};
    private String[] allColumns_EVENT = {"ID", "CHILD_ID", "TITLE", "DATE", "COLOR", "DRAWABLE", "SUBJECT", "HOUR", "THEMA", "INFO"};
    private String[] allColumns_CALENDAR = {"ID", "DATE", "INFO", "HOUR", "THEMA", "NOTIZ"};

    private String[] allColumns_HOURS_FORTT = {"ID", "CHILD_ID", "SUBJECT", "SHORT", "ROOM", "TEACHER", "HOUR", "DAY", "COLOR", "BORDER", "TV", "WEEK"};

    // ALTE Spaltenabfrage
    private String[] OLDallColumns_SUBJECTS = { "ID", "SUBJECT", "NC"};
    private String[] OLDallColumns_GRADES = {"ID", "SUBJECT", "THEMA", "TEACHER", "DATE", "GRADE", "INFO"};
    private String[] OLDallColumns_HOURS = {"ID", "SUBJECT", "SHORT", "ROOM", "TEACHER", "HOUR", "DAY"};

    // DB oeffnen
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    SQLiteDatabase getDB(){
        return database;
    }

    final String selectAllFromSUBJECTS       = "SELECT * FROM SUBJECTS";
    final String selectAllFromGRADES         = "SELECT * FROM GRADES";
    final String selectAllFromHOURS          = "SELECT * FROM HOURS";
    final String selectAllFromSTUDIP         = "SELECT * FROM STUDIP";
    final String selectAllFromEDITTIMES      = "SELECT * FROM EDITTIMES";
    final String selectAllFromSETTINGS       = "SELECT * FROM SETTINGS";


    // DB schliessen
    public void close() {
        dbHelper.close();
    }

    // DB neu erstellen
    public void newCreate(){
        dbHelper.resetDB(database);
    }


    //region CHILD
    /**
     * @see String  Ersteller - Tobias Häuser
     * @return      Liste von Subject Datensätzen
     */
    public ArrayList<DS_Child> getAllEntries_CHILD() {
        ArrayList<DS_Child> lEntries = new ArrayList<>();
        DS_Child child;
        int anzahl;

        Cursor cursor = database.query("CHILD", allColumns_SUBJECTS, null , null, null, null, "CHILD_ID");
        cursor.moveToFirst();

        if(cursor.getCount() == 0)
            return lEntries;

        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            child = new DS_Child();
            child.setId(cursor.getInt(0));
            child.setName(cursor.getString(1));
            child.setVorname(cursor.getString(2));
            lEntries.add(child);
            cursor.moveToNext();
        }

        cursor.close();

        return lEntries;
    }
    //endregion

    //region SUBJECT
    public void SUBJECTS_UpdateOrCreateEntry(DS_Subject subi) {
        Cursor cursor;
        ContentValues values = new ContentValues();
        values.put("CHILD_ID", subi.getChildId());
        values.put("SUBJECT", subi.getSubject());
        values.put("NC", subi.getNC());
        values.put("WISH_NC", subi.getWishNC());
        values.put("FINAL_GRADE", subi.getFinalGradeB());
        values.put("WERTUNG", subi.getWertung());
        values.put("CLASS", subi.getCurClass());

        // DB oeffnen
        open();

        // DS updaten falls vorhanden
        cursor = database.query("SUBJECTS", allColumns_SUBJECTS, "ID=" + subi.getId(),
                null, null, null, null);

        // Entscheidung ob update oder insert
        if(cursor.getCount() > 0)
            database.update("SUBJECTS", values, "ID = " + subi.getId(), null);
        else
            database.insert("SUBJECTS", null, values);


        // DB schliessen
        close();
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @return      Liste von Subject Datensätzen
     */
    public ArrayList<String> SUBJECTS_getClasses(String childID) {
        ArrayList<String> lEntries = new ArrayList<>();
        String sClass;
        int anzahl;

        open();

        Cursor cursor = database.rawQuery("SELECT DISTINCT CLASS FROM SUBJECTS WHERE CHILD_ID = " + childID + " ORDER BY 'SUBJECT'", null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0)
            return lEntries;

        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            lEntries.add(cursor.getString(0));
            cursor.moveToNext();
        }

        cursor.close();

        close();

        return lEntries;
    }

    /**
     * @see String  Ersteller - Tobias Häuser
     * @return      Liste von Subject Datensätzen
     */
    public ArrayList<String> SUBJECTS_getSubject(String childID) {
        ArrayList<String> lEntries = new ArrayList<>();
        String sClass;
        int anzahl;

        open();

        Cursor cursor = database.rawQuery("SELECT DISTINCT SUBJECT FROM SUBJECTS WHERE CHILD_ID = " + childID + " ORDER BY 'SUBJECT'", null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0)
            return lEntries;

        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            lEntries.add(cursor.getString(0));
            cursor.moveToNext();
        }

        cursor.close();

        close();

        return lEntries;
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @return      Liste von Subject Datensätzen
     */
    public ArrayList<DS_Subject> SUBJECTS_getAllEntries(String childID) {
        ArrayList<DS_Subject> lEntries = new ArrayList<>();
        DS_Subject subject;
        int anzahl;

        open();

        Cursor cursor = database.query("SUBJECTS", allColumns_SUBJECTS, "CHILD_ID = " + childID , null, null, null, "SUBJECT");
        cursor.moveToFirst();

        if(cursor.getCount() == 0)
            return lEntries;

        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            subject = new DS_Subject();
            subject.setId(cursor.getInt(0));
            subject.setChildId(cursor.getLong(1));
            subject.setSubject(cursor.getString(2));
            subject.setNC(cursor.getInt(3));
            subject.setWishNC(cursor.getInt(4));
            if(cursor.getString(5).toUpperCase().equals("1"))
                subject.setFinalGradeB(true);
            else
                subject.setFinalGradeB(false);
            subject.setWertung(cursor.getDouble(6));
            subject.setCurClass(cursor.getString(7));
            lEntries.add(subject);
            cursor.moveToNext();
        }

        cursor.close();

        close();

        return lEntries;
    }

    public ArrayList<DS_Subject> getAllEntries_SUBJECTS(String childID, String sClass) {
        ArrayList<DS_Subject> lEntries = new ArrayList<>();
        DS_Subject subject;
        int anzahl;

        open();
        Cursor cursor;

        if(sClass.equals("Alle")) {
             cursor = database.query("SUBJECTS", allColumns_SUBJECTS, "CHILD_ID = " + childID,
                    null, null, null, "SUBJECT");
        }
        else {
             cursor = database.query("SUBJECTS", allColumns_SUBJECTS, "CHILD_ID = " + childID + " AND CLASS = '" + sClass + "'",
                    null, null, null, "SUBJECT");
        }
        cursor.moveToFirst();

        if(cursor.getCount() == 0)
            return lEntries;

        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            subject = new DS_Subject();
            subject.setId(cursor.getInt(0));
            subject.setChildId(cursor.getLong(1));
            subject.setSubject(cursor.getString(2));
            subject.setNC(cursor.getDouble(3));
            subject.setWishNC(cursor.getDouble(4));
            if(cursor.getString(5).toUpperCase().equals("1"))
                subject.setFinalGradeB(true);
            else
                subject.setFinalGradeB(false);
            subject.setWertung(cursor.getDouble(6));
            subject.setCurClass(cursor.getString(7));
            lEntries.add(subject);
            cursor.moveToNext();
        }

        cursor.close();

        close();

        return lEntries;
    }

    public ArrayList<String> getAllEntries_SUBJECTS() {
        ArrayList<String> lEntries = new ArrayList<String>();
        int anzahl;

        Cursor cursor = database.query("SUBJECTS", OLDallColumns_SUBJECTS, null, null, null, null, "SUBJECT");
        cursor.moveToFirst();

        if(cursor.getCount() == 0) return lEntries;
        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            lEntries.add(cursor.getString(1));
            lEntries.add(cursor.getString(2));
            cursor.moveToNext();
        }

        cursor.close();

        return lEntries;
    }

    public void deleteEntry_SUBJECTS(DS_Subject dSubject) {

        open();

        database.delete("SUBJECTS", "ID = '" + dSubject.getId() + "'", null);
        /* TODO database.delete("GRADES", "CHILD_ID='" + dSubject.getChildId() + "' AND CLASS='" + dSubject.getSubject() + "'",
                null);*/

        close();
    }

    public void deleteEntry_SUBJECTS(String sFach) {

        database.delete("SUBJECTS", "SUBJECT='" + sFach + "'", null);

        database.delete("GRADES", "SUBJECT='" + sFach + "'", null);
    }
    //endregion

    //region GRADE
    public DS_Grade createEntry_GRADES(DS_Grade Subject) {
        ContentValues values = new ContentValues();
        values.put("SUBJECT", Subject.getSubject());
        if(!Subject.getThema().equals(""))
            values.put("THEMA", Subject.getThema());
        else
            values.put("THEMA", "-");
        values.put("TEACHER", Subject.getTeacher());
        values.put("DATE", Subject.getDate());
        values.put("GRADE", Subject.getGrade());
        values.put("INFO", Subject.getInfo());
        values.put("CHILD_ID", Subject.getChildId());

        long insertId = database.insert("GRADES", null,
                values);

        Cursor cursor = database.query("GRADES", allColumns_GRADES, "ID = " + insertId, null, null, null, null);
        cursor.moveToFirst();

        return cursorToEntry_GRADES(cursor);
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @return      Liste von Grades Datensätzen
     */
    public ArrayList<DS_Grade> getAllEntries_GRADES2(String childID) {
        ArrayList<DS_Grade> lEntries = new ArrayList<>();
        DS_Grade grade;
        int anzahl;

        Cursor cursor = database.query("GRADES", allColumns_GRADES, "CHILD_ID = " + childID , null, null, null, "SUBJECT");
        cursor.moveToFirst();

        if(cursor.getCount() == 0)
            return lEntries;

        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            grade = new DS_Grade();
            grade.setId(cursor.getInt(0));
            grade.setChildId(cursor.getLong(1));
            grade.setSubject(cursor.getString(2));
            grade.setThema(cursor.getString(3));
            grade.setTeacher(cursor.getString(4));
            grade.setDate(cursor.getString(5));
            grade.setGrade(cursor.getInt(6));
            grade.setInfo(cursor.getString(7));
            lEntries.add(grade);
            cursor.moveToNext();
        }

        cursor.close();

        return lEntries;
    }

    public ArrayList<DS_Grade> getAllEntries_GRADES(String childID, String sFach) {
        ArrayList<DS_Grade> lEntries = new ArrayList<>();
        DS_Grade grade;
        int anzahl;

        open();

        Cursor cursor = database.query("GRADES", allColumns_GRADES, "CHILD_ID = " + childID + " AND SUBJECT='" + sFach + "'" , null, null, null, "DATE, THEMA");
        cursor.moveToFirst();

        if(cursor.getCount() == 0)
            return lEntries;

        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            grade = new DS_Grade();
            grade.setId(cursor.getInt(0));
            grade.setChildId(cursor.getLong(1));
            grade.setSubject(cursor.getString(2));
            grade.setThema(cursor.getString(3));
            grade.setTeacher(cursor.getString(4));
            grade.setDate(cursor.getString(5));
            grade.setGrade(cursor.getInt(6));
            grade.setInfo(cursor.getString(7));
            lEntries.add(grade);
            cursor.moveToNext();
        }

        cursor.close();

        close();

        return lEntries;
    }


    public ArrayList<String> getAllEntries_GRADES(String sFach, String ignore, String ignor2) {
        ArrayList<String> lEntries = new ArrayList<String>();
        int anzahl;

        Cursor cursor = database.query("GRADES", OLDallColumns_GRADES, "SUBJECT='" + sFach + "'", null, null, null, "DATE");
        cursor.moveToFirst();


        if(cursor.getCount() == 0) return lEntries;
        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            lEntries.add(cursor.getString(0));
            lEntries.add(cursor.getString(2));
            lEntries.add(cursor.getString(5));
            cursor.moveToNext();
        }

        cursor.close();

        return lEntries;
    }

    public ArrayList<String> getOneEntrie_GRADES(int iID) {
        ArrayList<String> lEntries = new ArrayList<String>();
        int anzahl;

        Cursor cursor = database.query("GRADES", OLDallColumns_GRADES, "ID='" + iID + "'", null, null, null, null);
        cursor.moveToFirst();


        if(cursor.getCount() == 0) return lEntries;
        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            lEntries.add(cursor.getString(1));
            lEntries.add(cursor.getString(2));
            lEntries.add(cursor.getString(3));
            lEntries.add(cursor.getString(4));
            lEntries.add(cursor.getString(5));
            lEntries.add(cursor.getString(6));
            cursor.moveToNext();
        }

        cursor.close();

        return lEntries;
    }

    private DS_Grade cursorToEntry_GRADES(Cursor cursor) {
        DS_Grade entry = new DS_Grade();
        entry.setId(cursor.getInt(0));
        entry.setSubject(cursor.getString(1));
        entry.setThema(cursor.getString(2));
        entry.setTeacher(cursor.getString(3));
        entry.setDate(cursor.getString(4));
        entry.setGrade(cursor.getInt(5));
        entry.setInfo(cursor.getString(6));

        return entry;
    }

    public void updateEntry_GRADES(DS_Grade grade) {
        ContentValues values = new ContentValues();
        values.put("SUBJECT", grade.getSubject());
        values.put("THEMA", grade.getThema());
        values.put("TEACHER", grade.getTeacher());
        values.put("DATE", grade.getDate());
        values.put("GRADE", grade.getGrade());
        values.put("INFO", grade.getInfo());

        database.update("GRADES", values, "ID=" + grade.getId(), null);
    }

    public void deleteEntry_GRADE(int iID) {

        database.delete("GRADES", "ID=" + iID, null);
    }
    //endregion

    //region HOUR
    public DS_Hour createEntry_HOURS(DS_Hour Subject) {
        ContentValues values = new ContentValues();
        values.put("SUBJECT", Subject.getSubject());
        values.put("SHORT", Subject.getShort());
        values.put("ROOM", Subject.getRoom());
        values.put("TEACHER", Subject.getTeacher());
        values.put("HOUR", Subject.getHour());
        values.put("DAY", Subject.getDay());

        long insertId = database.insert("HOURS", null, values);

        Cursor cursor = database.query("HOURS", OLDallColumns_HOURS, "ID = " + insertId, null, null, null, null);
        cursor.moveToFirst();

        return cursorToEntry_HOURS(cursor);
    }


    public void HOUR_UpdateOrCreateEntry(DS_Hour hour) {
        // Deklaration
        ContentValues values    = new ContentValues();
        Cursor cursor;

        // ContentVlues fuellen
        values.put("CHILD_ID",  hour.getChildId());
        values.put("SUBJECT",   hour.getSubject());
        values.put("SHORT",     hour.getShort());
        values.put("ROOM",      hour.getRoom());
        values.put("TEACHER",   hour.getTeacher());
        values.put("HOUR",      hour.getHour());
        values.put("DAY",       hour.getDay());
        values.put("COLOR",     hour.getColor());
        values.put("BORDER",    hour.getBorder());
        values.put("TV",        hour.getTV());
        values.put("WEEK",      hour.getWeek());

        // DB oeffnen
        open();

        // DS updaten falls vorhanden
        cursor = database.query("HOURS", allColumns_HOURS, "CHILD_ID=" + hour.getChildId() + " AND HOUR='" + hour.getHour() + "'"
                                + " AND DAY='" + hour.getDay() + "'" , null, null, null, null);

        // Entscheidung ob update oder insert
        if(cursor.getCount() > 0)
            database.update("HOURS", values, "CHILD_ID=" + hour.getChildId() + " AND HOUR='" + hour.getHour() + "'"
                            + " AND DAY='" + hour.getDay() + "'", null);
        else
            database.insert("HOURS", null, values);


        // DB schliessen
        close();
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @return      Liste von Hours Datensätzen fuer TT
     */
    public ArrayList<DS_Hour> getAllEntries_HOURS_forTT(String childID, Integer iFirstHour, ArrayList<DS_Hour> lEntries) {
        // Deklaration
        DS_Hour hour;

        // DB oeffnen
        open();

        // DS auslesen
        Cursor cursor = database.query("HOURS", allColumns_HOURS_FORTT, "CHILD_ID = " + childID, null, null, null, null);
        cursor.moveToFirst();

        // Leere Liste zurueckgeben, falls keine DS vorhanden
        if (cursor.getCount() == 0)
            return lEntries;

        // Liste befuellen und ersetzen
        for (int i = 0; i < cursor.getCount(); i++) {
            hour = new DS_Hour();

            // Daten setzen
            hour.setId(cursor.getInt(0));
            hour.setChildId(cursor.getLong(1));
            hour.setSubject(cursor.getString(2));
            hour.setShort(cursor.getString(3));
            hour.setRoom(cursor.getString(4));
            hour.setTeacher(cursor.getString(5));
            hour.setHour(cursor.getInt(6));
            hour.setDay(cursor.getInt(7));
            hour.setColor(cursor.getInt(8));
            hour.setBorder(cursor.getString(9));
            hour.setTV(cursor.getString(10));
            hour.setWeek(cursor.getString(11));

            // DS aus Liste ersetzen
            lEntries.remove((hour.getHour()-iFirstHour)*5+hour.getDay());

            // Datensatz hinzufuegen
            lEntries.add((hour.getHour()-iFirstHour)*5+hour.getDay(), hour);
            cursor.moveToNext();
        }

        // Cursor und DB schliessen
        cursor.close();
        close();

        return lEntries;
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @return      Liste von Hours Datensätzen fuer TT
     */
    public ArrayList<DS_Hour> getAllEntries_HOURS(String childID, Integer iFirstHour, String whereInDay) {
        // Deklaration
        DS_Hour hour;
        ArrayList<DS_Hour> lEntries = new ArrayList<>();

        // DB oeffnen
        open();

        // DS auslesen
        Cursor cursor = database.query("HOURS", allColumns_HOURS, "CHILD_ID = " + childID
                        + " AND DAY in " + whereInDay, null, null, null, null);
        cursor.moveToFirst();

        // Leere Liste zurueckgeben, falls keine DS vorhanden
        if (cursor.getCount() == 0)
            return lEntries;

        // Liste befuellen und ersetzen
        for (int i = 0; i < cursor.getCount(); i++) {
            hour = new DS_Hour();

            // Daten setzen
            hour.setId(cursor.getInt(0));
            hour.setChildId(cursor.getLong(1));
            hour.setSubject(cursor.getString(2));
            hour.setShort(cursor.getString(3));
            hour.setRoom(cursor.getString(4));
            hour.setTeacher(cursor.getString(5));
            hour.setHour(cursor.getInt(6));
            hour.setDay(cursor.getInt(7));
            hour.setColor(cursor.getInt(8));
            hour.setWeek(cursor.getString(9));

            // Datensatz hinzufuegen
            lEntries.add(hour);
            cursor.moveToNext();
        }

        // Cursor und DB schliessen
        cursor.close();
        close();

        return lEntries;
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @return      Liste von Hours Datensätzen
     */
    public ArrayList<DS_Hour> getAllEntries_HOURS(String childID, String sWeek) {
        // Deklaration
        ArrayList<DS_Hour> lEntries = new ArrayList<>();
        DS_Hour hour;

        // DB oeffnen
        open();

        // DS auslesen
        Cursor cursor = database.query("HOURS", allColumns_HOURS, "CHILD_ID = " + childID +
                        " AND WEEK = '" + sWeek + "'", null, null, null, null);
        cursor.moveToFirst();

        // Leere Liste zurueckgeben, falls keine DS vorhanden
        if (cursor.getCount() == 0)
            return lEntries;

        // Liste befuellen und ersetzen
        for (int i = 0; i < cursor.getCount(); i++) {
            hour = new DS_Hour();
            hour.setId(cursor.getInt(0));
            hour.setChildId(cursor.getLong(1));
            hour.setSubject(cursor.getString(2));
            hour.setShort(cursor.getString(3));
            hour.setRoom(cursor.getString(4));
            hour.setTeacher(cursor.getString(5));
            hour.setHour(cursor.getInt(6));
            hour.setDay(cursor.getInt(7));
            hour.setColor(cursor.getInt(8));
            hour.setWeek(cursor.getString(9));

            // Zur Liste hinzufuegen
            lEntries.add(hour);
            cursor.moveToNext();
        }

        // Cursor und DB schliessen
        cursor.close();
        close();

        return lEntries;
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @return      Liste von Hours Datensätzen
     */
    public ArrayList<DS_Hour> getUniqeEntries_HOURS(String childID) {
        // Deklaration
        ArrayList<DS_Hour> lEntries = new ArrayList<>();
        DS_Hour hour;
        String sql = "SELECT DISTINCT CHILD_ID, SUBJECT, ROOM, TEACHER, COLOR FROM HOURS WHERE CHILD_ID = " + childID;

        // DB oeffnen
        open();

        // DS auslesen
        Cursor cursor = database.rawQuery(sql, null);
        cursor.moveToFirst();

        // Leere Liste zurueckgeben, falls keine DS vorhanden
        if (cursor.getCount() == 0)
            return lEntries;

        // Liste befuellen und ersetzen
        for (int i = 0; i < cursor.getCount(); i++) {
            hour = new DS_Hour();
            hour.setChildId(cursor.getLong(0));
            hour.setSubject(cursor.getString(1));
            hour.setRoom(cursor.getString(2));
            hour.setTeacher(cursor.getString(3));
            hour.setColor(cursor.getInt(4));

            // Zur Liste hinzufuegen
            lEntries.add(hour);
            cursor.moveToNext();
        }

        // Cursor und DB schliessen
        cursor.close();
        close();

        return lEntries;
    }

    public void updateEntry_HOURS(DS_Hour Subject) {
        ContentValues values = new ContentValues();
        values.put("SUBJECT", Subject.getSubject());
        values.put("SHORT", Subject.getShort());
        values.put("ROOM", Subject.getRoom());
        values.put("TEACHER", Subject.getTeacher());
        values.put("HOUR", Subject.getHour());
        values.put("DAY", Subject.getDay());

        database.update("HOURS", values, "ID = " + Subject.getId(), null);
    }

    public ArrayList<String> getAllEntries_HOURS() {
        ArrayList<String> lEntries = new ArrayList<String>();
        int anzahl;

        Cursor cursor = database.query("HOURS", OLDallColumns_HOURS, null, null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0) return lEntries;
        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            lEntries.add(cursor.getString(1));
            lEntries.add(cursor.getString(2));
            lEntries.add(cursor.getString(3));
            lEntries.add(cursor.getString(4));
            lEntries.add(cursor.getString(5));
            lEntries.add(cursor.getString(6));
            cursor.moveToNext();
        }

        cursor.close();

        return lEntries;
    }

    public ArrayList<String> getOneEntries_HOURS(int iID) {
        ArrayList<String> lEntries = new ArrayList<String>();
        int anzahl;

        Cursor cursor = database.query("HOURS", OLDallColumns_HOURS, "ID = " + iID, null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0) return lEntries;
        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            lEntries.add(cursor.getString(1));
            lEntries.add(cursor.getString(2));
            lEntries.add(cursor.getString(3));
            lEntries.add(cursor.getString(4));
            lEntries.add(cursor.getString(5));
            lEntries.add(cursor.getString(6));
            cursor.moveToNext();
        }

        cursor.close();

        return lEntries;
    }

    private DS_Hour cursorToEntry_HOURS(Cursor cursor) {
        DS_Hour entry = new DS_Hour();
        entry.setSubject(cursor.getString(1));
        entry.setShort(cursor.getString(2));
        entry.setRoom(cursor.getString(3));
        entry.setTeacher(cursor.getString(4));
        entry.setHour(cursor.getInt(5));
        entry.setDay(cursor.getInt(6));

        return entry;
    }
    //endregion

    //region STUDIP

    /**
     * @see String      Ersteller - Tobias Häuser
     * @param studip    Termin der erstellt werden soll.
     */
    public void STUDIP_UpdateOrCreateEntry(DS_StudIP studip) {
        // Deklaration
        ContentValues values    = new ContentValues();
        Cursor cursor;

        // ContentVlues fuellen
        values.put("CHILD_ID",  studip.getChildId());
        values.put("SUBJECT",   studip.getSubject());
        values.put("ROOM",      studip.getRoom());
        values.put("STARTTIME", studip.getStartTime());
        values.put("ENDTIME",   studip.getEndTime());
        values.put("DAY",       studip.getDay());

        // DB oeffnen
        open();

        // DS updaten falls vorhanden
        cursor = database.query("STUDIP", allColumns_STUDIP, "CHILD_ID=" + studip.getChildId() + " AND SUBJECT='" + studip.getSubject() + "'"
                + " AND STARTTIME='" + studip.getStartTime() + "'" + " AND ENDTIME='" + studip.getEndTime() + "'"
                + " AND DAY='" + studip.getDay() + "'" , null, null, null, null);

        // Entscheidung ob update oder insert
        if(cursor.getCount() > 0)
            database.update("STUDIP", values, "CHILD_ID=" + studip.getChildId() + " AND SUBJECT='" + studip.getSubject() + "'"
                    + " AND STARTTIME='" + studip.getStartTime() + "'" + " AND ENDTIME='" + studip.getEndTime() + "'"
                    + " AND DAY='" + studip.getDay() + "'", null);
        else
            database.insert("STUDIP", null, values);


        // DB schliessen
        close();
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @return      Liste von Hours Datensätzen fuer TT
     */
    public ArrayList<DS_StudIP> STUDIP_getAllEntries(String childID, String day) {
        // Deklaration
        DS_StudIP studip;
        ArrayList<DS_StudIP> lEntries = new ArrayList<>();

        // DB oeffnen
        open();

        // DS auslesen
        Cursor cursor = database.query("STUDIP", allColumns_STUDIP, "CHILD_ID = " + childID
                + " AND DAY='" + day + "'", null, null, null, null);
        cursor.moveToFirst();

        // Leere Liste zurueckgeben, falls keine DS vorhanden
        if (cursor.getCount() == 0)
            return lEntries;

        // Liste befuellen und ersetzen
        for (int i = 0; i < cursor.getCount(); i++) {
            studip = new DS_StudIP();

            // Daten setzen
            studip.setId(cursor.getInt(0));
            studip.setChildId(cursor.getLong(1));
            studip.setSubject(cursor.getString(2));
            studip.setRoom(cursor.getString(3));
            studip.setStartTime(cursor.getInt(4));
            studip.setEndTime(cursor.getInt(5));
            studip.setDay(cursor.getInt(6));

            // Datensatz hinzufuegen
            lEntries.add(studip);
            cursor.moveToNext();
        }

        // Cursor und DB schliessen
        cursor.close();
        close();

        return lEntries;
    }
    //endregion

    //region EDITTIMES
    public void EDITTIMES_UpdateOrCreateEntry(DS_TimeEdit EditTime) {
        // Deklaration
        ContentValues values    = new ContentValues();
        Cursor cursor;

        // ContentVlues fuellen
        values.put("HOUR",      EditTime.getHour());
        values.put("FROM_TIME", EditTime.getFromTime());
        values.put("TO_TIME",   EditTime.getToTime());

        // DB oeffnen
        open();

        // DS updaten falls vorhanden
        cursor = database.query("EDITTIMES", allColumns_EDITTIMES, "HOUR='" + EditTime.getHour() + "'", null, null, null, null);


        // Entscheidung ob update oder insert
        if(cursor.getCount() > 0)
            database.update("EDITTIMES", values, "HOUR='" + EditTime.getHour() + "'", null);
        else
            database.insert("EDITTIMES", null, values);


        // DB schliessen
        cursor.close();
        close();
    }


    public void EDITTIMES_DeleteTimes(int firstHour, int lastHour) {
        // Deklaration
        String sIn = "('" + firstHour + "'";

        for (int i = firstHour+1; i <= lastHour; i++) {
            sIn += ", '" + i + "'";

        }
        sIn += ")";

        // DB oeffnen
        open();

        // DS updaten falls vorhanden
        database.delete("EDITTIMES", "HOUR NOT IN " + sIn, null);

        // DB schliessen
        close();
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @return      Liste von Hours Datensätzen
     */
    public ArrayList<DS_TimeEdit> getAllEntries_EDITTIMES() {
        ArrayList<DS_TimeEdit> lEntries = new ArrayList<>();
        DS_TimeEdit editTime;
        int anzahl;

        open();
        Cursor cursor = database.query("EDITTIMES", allColumns_EDITTIMES, null, null, "HOUR", null, null);

        cursor.moveToFirst();

        if (cursor.getCount() == 0)
            return lEntries;

        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++) {
            editTime = new DS_TimeEdit();
            editTime.setId(cursor.getInt(0));
            editTime.setHour(cursor.getString(1));
            editTime.setFromTime(cursor.getString(2));
            editTime.setToTime(cursor.getString(3));
            lEntries.add(editTime);
            cursor.moveToNext();
        }

        cursor.close();

        close();

        return lEntries;
    }
    //endregion

    //region SETTINGS
    // TODO Umbau auf CreateOrUpdate
    public void SETTINGS_createEntry(String name, String value) {
        ContentValues values = new ContentValues();
        values.put("NAME", name);
        values.put("VALUE", value);

        database.insert("SETTINGS", null, values);
    }

    /**
     * @see String  Ersteller - Tobias Häuser
     * @return      Liste von Setting Datensätzen
     */
    public ArrayList<DS_Setting> getAllEntries_SETTINGS() {
        // Deklaration & Init
        ArrayList<DS_Setting> lSettings     = new ArrayList<>();
        Cursor cursor;
        DS_Setting setting;

        // Oefnnen der DB
        open();

        // Auslesen des Settings
        cursor = database.query("SETTINGS", allColumns_SETTINGS, null, null, null, null, null);

        // Pruefung auf Eintraege
        if(cursor.getCount() == 0)
            return lSettings;

        // Setzen des Cursors an erster Stelle
        cursor.moveToFirst();

        // Auslesen & Einfuegen
        for (int i = 0; i < cursor.getCount(); i++)
        {
            setting = new DS_Setting();
            setting.setId(cursor.getInt(0));
            setting.setName(cursor.getString(1));
            setting.setValue(cursor.getString(2));

            lSettings.add(setting);
            cursor.moveToNext();
        }

        // Cursor & DB schliessen
        cursor.close();
        close();

        return lSettings;
    }



    public boolean SETTINGS_UpdateOrCreateEntry(String name, String value) {
        // Deklaration
        ContentValues values    = new ContentValues();
        Cursor cursor           = null;

        // ContentVlues fuellen
        values.put("NAME", name);
        values.put("VALUE", value);

        // DB oeffnen
        open();

        // DS updaten falls vorhanden
        cursor = database.query("SETTINGS", allColumns_SETTINGS, "NAME='" + name + "'", null, null, null, null);


        // Entscheidung ob update oder insert
        if(cursor.getCount() > 0)
            database.update("SETTINGS", values, "NAME='" + name + "'", null);
        else
            database.insert("SETTINGS", null, values);


        // DB schliessen
        close();
        return true;
    }

    public String SETTINGS_getOneEntries(String Name) {
        // Deklaration & Init
        Cursor cursor;
        String sValue   = "";

        // Oefnnen der DB
        open();

        // Auslesen des Settings
        cursor = database.query("SETTINGS", allColumns_SETTINGS, "NAME='" + Name + "'", null, null, null, null);

        // Pruefung auf Eintraege
        if(cursor.getCount() == 0)
            return sValue;

        // Setzen des Cursors an erster Stelle
        cursor.moveToFirst();

        // Auslesen & Einfuegen
        for (int i = 0; i < cursor.getCount(); i++)
        {
            sValue = cursor.getString(2);
            cursor.moveToNext();
        }

        // Cursor & DB schliessen
        cursor.close();
        close();

        return sValue;
    }

    public ArrayList<String> SETTINGS_getEntries(String... names) {
        // Deklaration & Init
        Cursor cursor               = null;
        String Name                 = "";
        int CountNames              = 0;
        ArrayList<String> alNames   = new ArrayList<>();

        // Bildung von 'Name' fuer die Abfrage
        for (CountNames = 0; CountNames < names.length -1; CountNames++)
        {
            Name += "'" + names[CountNames] + "', ";
        }
        Name += "'" + names[CountNames] + "'";

        // Oefnnen der DB
        open();

        // Auslesen des Settings
        cursor = database.query("SETTINGS", allColumns_SETTINGS, "NAME in (" + Name + ")", null, null, null, null);

        // Pruefung auf Eintraege
        if(cursor.getCount() == 0)
            return alNames;

        // Setzen des Cursors an erster Stelle
        cursor.moveToFirst();

        // Auslesen & Einfuegen
        for (int i = 0; i < cursor.getCount(); i++)
        {
            alNames.add(i,cursor.getString(2));
            cursor.moveToNext();
        }

        // Cursor & DB schliessen
        cursor.close();
        close();

        return alNames;
    }

    public ArrayList<String> SETTINGS_countEntrys(String Name) {
        // Deklaration
        ArrayList<String> lEntries  = new ArrayList<>();
        Cursor cursor               = null;

        // Datenbank oeffnen
        open();

        // Versuch Daten auszulesen
        try {
            cursor = database.query("SETTINGS", allColumns_SETTINGS, "NAME='" + Name + "'", null, null, null, null);
        }
        catch (Exception Ex)
        {
            return lEntries;
        }

        // Cursor an erste Stelle setzen
        cursor.moveToFirst();

        // Abfrage ob Eintraege vorhanden sind
        if(cursor.getCount() == 0)
            return lEntries;

        // Entries fuellen
        for (int i = 0; i < cursor.getCount(); i++)
        {
            lEntries.add(cursor.getString(2));
            cursor.moveToNext();
        }

        // Datenbank & Cursor schliessen
        cursor.close();
        close();

        return lEntries;
    }

    public void updateEntry_SETTINGS(DS_Setting Setting) {
        ContentValues values = new ContentValues();
        values.put("NAME", Setting.getName());
        values.put("VALUE", Setting.getValue());

        open();
        try {
            database.update("SETTINGS", values, "NAME='" + Setting.getName() + "'", null);
        }catch (Exception ex){
            long i = 0;
        }

        close();
    }

    public boolean SETTINGS_updateEntry(String name, String value) {
        ContentValues values = new ContentValues();
        values.put("NAME", name);
        values.put("VALUE", value);

        open();
        try {
            database.update("SETTINGS", values, "NAME='" + name + "'", null);
        }catch (Exception ex){
            return false;
        }

        close();

        return true;
    }

    public void deleteEntry_SETTINGS(int iID) {

        database.delete("SETTINGS", "ID=" + iID, null);
    }
    //endregion

    //region CALENDAR
    public DS_NewTimes createEntry_CALENDAR(DS_NewTimes Subject) {
        ContentValues values = new ContentValues();
        values.put("DATE", Subject.getDate());
        values.put("INFO", Subject.getInfo());
        values.put("HOUR", Subject.getHour());
        values.put("THEMA", Subject.getThema());
        values.put("NOTIZ", Subject.getNotiz());

        long insertId = database.insert("CALENDAR", null,
                values);

        Cursor cursor = database.query("CALENDAR", allColumns_CALENDAR, "ID = " + insertId, null, null, null, null);
        cursor.moveToFirst();

        return cursorToEntry_CALENDAR(cursor);
    }

    public boolean CALENDAR_createEntry(DS_NewTimes Subject) {
        // Deklaration & Init
        ContentValues values = new ContentValues();

        // ContentValues befuellen
        values.put("DATE", Subject.getDate());
        values.put("INFO", Subject.getInfo());
        values.put("HOUR", Subject.getHour());
        values.put("THEMA", Subject.getThema());
        values.put("NOTIZ", Subject.getNotiz());

        // DB oeffnen
        open();

        // DS insert
        try {
            database.insert("CALENDAR", null, values);
        }catch (Exception ex){
            return false;
        }

        // DB schliessen
        close();

        return true;
    }

    public ArrayList<String> getAllEntries_CALENDAR() {
        ArrayList<String> lEntries = new ArrayList<String>();
        int anzahl;

        Cursor cursor = database.query("CALENDAR", allColumns_CALENDAR, null, null, null, null, "DATE ASC");
        cursor.moveToFirst();


        if(cursor.getCount() == 0) return lEntries;
        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            lEntries.add(cursor.getString(1));
            lEntries.add(cursor.getString(2));
            lEntries.add(cursor.getString(3));
            lEntries.add(cursor.getString(4));
            lEntries.add(cursor.getString(5));
            cursor.moveToNext();
        }

        cursor.close();

        return lEntries;
    }

    public ArrayList<String> CALENDAR_getOneEntrie(int iID) {
        // Deklaration & Init
        ArrayList<String> lEntries  = new ArrayList<>();

        // DB oeffnen
        open();

        // Daten auslesen & an erste Stelle setzen
        Cursor cursor = database.query("CALENDAR", allColumns_CALENDAR, "ID='" + iID + "'", null, null, null, null);
        cursor.moveToFirst();

        // TODO
        if(cursor.getCount() == 0)
            return lEntries;

        // TODO
        for (int i = 0; i < cursor.getCount(); i++)
        {
            lEntries.add(cursor.getString(0));
            lEntries.add(cursor.getString(1));
            lEntries.add(cursor.getString(2));
            lEntries.add(cursor.getString(3));
            lEntries.add(cursor.getString(4));
            lEntries.add(cursor.getString(5));
            cursor.moveToNext();
        }

        // DB & Cursor schliessen
        close();
        cursor.close();

        return lEntries;
    }

    public ArrayList<String> getDayEntries_CALENDAR(String date) {
        ArrayList<String> lEntries = new ArrayList<String>();
        int anzahl;

        Cursor cursor = database.query("CALENDAR", allColumns_CALENDAR, "DATE='" + date + "'", null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0) return lEntries;
        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            lEntries.add(cursor.getString(0));
            lEntries.add(cursor.getString(1));
            lEntries.add(cursor.getString(2));
            lEntries.add(cursor.getString(3));
            lEntries.add(cursor.getString(4));
            lEntries.add(cursor.getString(5));
            cursor.moveToNext();
        }

        cursor.close();

        return lEntries;
    }

    public ArrayList<String> getDayALLEntries_CALENDAR(String date) {
        ArrayList<String> lEntries = new ArrayList<String>();
        int anzahl;

        Cursor cursor = database.query("CALENDAR", allColumns_CALENDAR, "DATE='" + date + "'", null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0) return lEntries;
        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            lEntries.add(cursor.getString(0));
            lEntries.add(cursor.getString(1));
            lEntries.add(cursor.getString(2));
            lEntries.add(cursor.getString(3));
            lEntries.add(cursor.getString(4));
            lEntries.add(cursor.getString(5));
            cursor.moveToNext();
        }

        cursor.close();

        return lEntries;
    }

    private DS_NewTimes cursorToEntry_CALENDAR(Cursor cursor) {
        DS_NewTimes entry = new DS_NewTimes();
        entry.setId(cursor.getInt(0));
        entry.setDate(cursor.getString(1));
        entry.setInfo(cursor.getString(2));
        entry.setHour(cursor.getInt(3));
        entry.setThema(cursor.getString(4));
        entry.setNotiz(cursor.getString(5));

        return entry;
    }

    public boolean CALENDAR_updateEntry(DS_NewTimes Time) {
        // Deklaration & Init
        ContentValues values    = new ContentValues();
        values.put("DATE", Time.getDate());
        values.put("INFO", Time.getInfo());
        values.put("HOUR", Time.getHour());
        values.put("THEMA", Time.getThema());
        values.put("NOTIZ", Time.getNotiz());

        // DB oeffnen
        open();

        // DS updaten
        try {
            database.update("CALENDAR", values, "ID=" + Time.getId(), null);
        }catch (Exception ex){
            return false;
        }

        // DB schliessen
        return true;
    }

    public void CALENDAR_deleteEntry(int iID) {
        // DB oeffnen
        open();

        // DS loeschen
        database.delete("CALENDAR", "ID=" + iID, null);

        // DB schliessen
        close();
    }
    //endregion


    // region EVENT
    /**
     * @see String  Ersteller - Tobias Häuser
     * @return      Liste von Grades Datensätzen
     */
    public ArrayList<DS_Event> EVENT_getAllEntries(String childID, String sSubject) {
        ArrayList<DS_Event> lEntries = new ArrayList<>();
        DS_Event event;
        int anzahl;

        open();

        Cursor cursor = database.query("EVENT", allColumns_EVENT, "CHILD_ID = " + childID + " AND SUBJECT = '" + sSubject + "'",
                null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0)
            return lEntries;

        anzahl = cursor.getCount();

        for (int i = 0; i < anzahl; i++)
        {
            event = new DS_Event();
            event.setId(cursor.getInt(0));
            event.setChildId(cursor.getInt(1));
            event.setTitle(cursor.getString(2));
            event.setDate(cursor.getInt(3));
            event.setColor(cursor.getInt(4));
            event.setDrw(cursor.getInt(5));
            event.setSubject(cursor.getString(6));
            event.setHour(cursor.getInt(7));
            event.setThema(cursor.getString(8));
            event.setInfo(cursor.getString(9));
            lEntries.add(event);
            cursor.moveToNext();
        }

        cursor.close();
        close();

        return lEntries;
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @return      Liste von Grades Datensätzen
     */
    public DS_Event EVENT_getOneEntrie(Integer iId) {
        DS_Event event = new DS_Event();

        open();

        Cursor cursor = database.query("EVENT", allColumns_EVENT, "ID = " + iId,
                null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0)
            return event;

        event.setId(cursor.getInt(0));
        event.setChildId(cursor.getInt(1));
        event.setTitle(cursor.getString(2));
        event.setDate(cursor.getInt(3));
        event.setColor(cursor.getInt(4));
        event.setDrw(cursor.getInt(5));
        event.setSubject(cursor.getString(6));
        event.setHour(cursor.getInt(7));
        event.setThema(cursor.getString(8));
        event.setInfo(cursor.getString(9));

        cursor.close();
        close();

        return event;
    }


    /**
     * @see String      Ersteller - Tobias Häuser
     * @param event     Termin der erstellt werden soll.
     */
    public void Event_UpdateOrCreateEntry(DS_Event event) {
        // Deklaration
        ContentValues values    = new ContentValues();
        Cursor cursor;

        // ContentVlues fuellen
        values.put("CHILD_ID",  event.getChildId());
        values.put("TITLE",     event.getTitle());
        values.put("DATE",      event.getDate());
        values.put("COLOR",     event.getColor());
        values.put("DRAWABLE",  event.getDrw());
        values.put("SUBJECT",   event.getSubject());
        values.put("HOUR",      event.getHour());
        values.put("THEMA",     event.getThema());
        values.put("INFO",      event.getInfo());

        // DB oeffnen
        open();

        // DS updaten falls vorhanden
        cursor = database.query("EVENT", allColumns_EVENT, "ID='" + event.getId() + "'",
                null, null, null, null);

        // Entscheidung ob update oder insert
        if(cursor.getCount() > 0)
            database.update("EVENT", values, "ID='" + event.getId() + "'", null);
        else
            database.insert("EVENT", null, values);


        // DB schliessen
        cursor.close();
        close();
    }


    /**
     * @see String      Ersteller - Tobias Häuser
     * @see String      Die Farbe der Haupticon Termine aktualisieren
     * @param icon      Das Haupticon wessen Farbe geändert werden soll
     * @param color     Die ersetzende Farbe
     */
    public void Event_UpdateEntrys(String icon, int color) {
        // Deklaration
        ContentValues values    = new ContentValues();
        Cursor cursor;

        // DB oeffnen
        open();

        // DS updaten falls vorhanden
        cursor = database.query("EVENT", allColumns_EVENT, "DRAWABLE='" + icon + "'",
                null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0)
            return;

        for (int i = 0; i < cursor.getCount(); i++) {
            // ContentVlues fuellen
            values.put("CHILD_ID",  cursor.getInt(1));
            values.put("TITLE",     cursor.getString(2));
            values.put("DATE",      cursor.getInt(3));
            values.put("COLOR",     color);
            values.put("DRAWABLE",  cursor.getString(5));
            values.put("SUBJECT",   cursor.getString(6));
            values.put("HOUR",      cursor.getInt(7));
            values.put("THEMA",     cursor.getString(8));
            values.put("INFO",      cursor.getString(9));

            database.update("EVENT", values, "ID='" + cursor.getInt(0) + "'", null);
            cursor.moveToNext();
        }

        // DB schliessen
        cursor.close();
        close();
    }


    /**
     * @see String          Ersteller - Tobias Häuser
     * @param childID       Termin der erstellt werden soll.
     */
    public ArrayList<Event_DSs.FirstStrTwArTw> Event_GetAllEntrys(String childID) {
        // Deklaration
        Cursor cursor1, cursor2 = null, cursor3 = null;

        ArrayList<Event_DSs.ThirdTwoString>         thiEbene;
        ArrayList<Event_DSs.SecondTwoStrArLTwStr>   secEbene;
        ArrayList<Event_DSs.FirstStrTwArTw>         firEbene = new ArrayList<>();

        // DB oeffnen
        open();

        try {
            cursor1 = database.rawQuery("SELECT DISTINCT DATE FROM EVENT WHERE CHILD_ID = " + childID + " ORDER BY DATE", null);
            cursor1.moveToFirst();

            if (cursor1.getCount() == 0)
                return firEbene;


            for (int i = 0; i < cursor1.getCount(); i++) {
                cursor2 = database.rawQuery("SELECT DISTINCT ID, TITLE, DATE, COLOR, DRAWABLE FROM EVENT WHERE DATE = "
                        + cursor1.getInt(0) + " ORDER BY DATE, TITLE", null);
                cursor2.moveToFirst();

                secEbene = new ArrayList<>();

                for (int i2 = 0; i2 < cursor2.getCount(); i2++) {
                    cursor3 = database.rawQuery("SELECT DISTINCT SUBJECT, HOUR, THEMA, INFO FROM EVENT " +
                            "WHERE ID = " + cursor2.getInt(0) + " ORDER BY 'DATE', 'TITLE'", null);
                    cursor3.moveToFirst();

                    thiEbene = new ArrayList<>();


                    if (!cursor3.getString(0).isEmpty())
                        thiEbene.add(new Event_DSs.ThirdTwoString(cont.getString(R.string.SUBJECT),
                                cursor3.getString(0)));
                    if (!cursor3.getString(1).isEmpty())
                        thiEbene.add(new Event_DSs.ThirdTwoString(cont.getString(R.string.HOUR),
                                cursor3.getString(1)));
                    if (!cursor3.getString(2).isEmpty())
                        thiEbene.add(new Event_DSs.ThirdTwoString(cont.getString(R.string.THEMA),
                                cursor3.getString(2)));
                    if (!cursor3.getString(3).isEmpty())
                        thiEbene.add(new Event_DSs.ThirdTwoString(cont.getString(R.string.INFO),
                                cursor3.getString(3)));

                    thiEbene.add(new Event_DSs.ThirdTwoString("FOOTER", ""));

                    secEbene.add(new Event_DSs.SecondTwoStrArLTwStr(
                            new Event_DSs.ThirdTwoString(cursor2.getInt(0), cursor2.getString(1), cursor2.getInt(2),
                                    cursor2.getInt(3), cursor2.getInt(4)), thiEbene));


                    cursor2.moveToNext();
                }

                firEbene.add(new Event_DSs.FirstStrTwArTw(cursor1.getInt(0), secEbene));

                cursor1.moveToNext();
            }


            // DB schliessen
            cursor1.close();
            if (cursor2 != null)
                cursor2.close();
            if (cursor3 != null)
                cursor3.close();
            close();
        }
        catch (Exception ex) {
            int i = 0;
        }

        return firEbene;
    }



    /**
     * @see String          Ersteller - Tobias Häuser
     * @param childID       Termin der erstellt werden soll.
     */
    public int Event_GetCountEntrysOfDay(String childID, String sDay) {
        // Deklaration
        Cursor cursor;
        int iReturn = 0;

        // DB oeffnen
        open();

        // SQL ausfuehren
        cursor = database.rawQuery("SELECT DISTINCT COUNT(*) FROM EVENT WHERE CHILD_ID = " + childID +
                " AND DATE = '" + sDay + "'", null);
        cursor.moveToFirst();

        // Daten auslesen
        if (cursor.getCount() == 0)
            iReturn = -1;
        else
            iReturn = cursor.getInt(0);

        // DB & Cursor schliessen
        cursor.close();
        close();

        return iReturn;
    }



    /**
     * @see String          Ersteller - Tobias Häuser
     * @param childID       Termin der erstellt werden soll.
     */
    public ArrayList<Event_DSs.FirstStrTwArTw> Event_GetAllEntrys(String childID, Calendar cal0, Calendar cal1) {
        // Deklaration
        Cursor cursor1, cursor2 = null, cursor3 = null;
        Integer pastTime, currTime;

        ArrayList<Event_DSs.ThirdTwoString>         thiEbene;
        ArrayList<Event_DSs.SecondTwoStrArLTwStr>   secEbene;
        ArrayList<Event_DSs.FirstStrTwArTw>         firEbene = new ArrayList<>();

        // DB oeffnen
        open();

        // Datumsnagaben als Integer beziehen
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd", Locale.GERMANY);
        pastTime = Integer.parseInt(dateFormat.format(cal0.getTime()));
        currTime = Integer.parseInt(dateFormat.format(cal1.getTime()));

        cursor1 = database.rawQuery("SELECT DISTINCT DATE FROM EVENT WHERE CHILD_ID = " + childID +
                " AND DATE BETWEEN " + pastTime + " AND " + currTime + " ORDER BY DATE", null);
        cursor1.moveToFirst();

        if (cursor1.getCount() == 0)
            return firEbene;


        for (int i = 0; i < cursor1.getCount(); i++) {
            cursor2 = database.rawQuery("SELECT DISTINCT ID, TITLE, DATE, COLOR, DRAWABLE FROM EVENT WHERE DATE = "
                    + cursor1.getInt(0) + " ORDER BY DATE, TITLE", null);
            cursor2.moveToFirst();

            secEbene = new ArrayList<>();

            for (int i2 = 0; i2 < cursor2.getCount(); i2++) {
                cursor3 = database.rawQuery("SELECT DISTINCT SUBJECT, HOUR, THEMA, INFO FROM EVENT " +
                        "WHERE ID = " + cursor2.getInt(0) + " ORDER BY 'DATE', 'TITLE'", null);
                cursor3.moveToFirst();

                thiEbene = new ArrayList<>();

                if (!cursor3.getString(0).isEmpty())
                    thiEbene.add(new Event_DSs.ThirdTwoString(cont.getString(R.string.SUBJECT),
                            cursor3.getString(0)));
                if (!cursor3.getString(1).isEmpty())
                    thiEbene.add(new Event_DSs.ThirdTwoString(cont.getString(R.string.HOUR),
                            cursor3.getString(1)));
                if (!cursor3.getString(2).isEmpty())
                    thiEbene.add(new Event_DSs.ThirdTwoString(cont.getString(R.string.THEMA),
                            cursor3.getString(2)));
                if (!cursor3.getString(3).isEmpty())
                    thiEbene.add(new Event_DSs.ThirdTwoString(cont.getString(R.string.INFO),
                            cursor3.getString(3)));

                thiEbene.add(new Event_DSs.ThirdTwoString("FOOTER", ""));

                secEbene.add(new Event_DSs.SecondTwoStrArLTwStr(
                        new Event_DSs.ThirdTwoString(cursor2.getInt(0), cursor2.getString(1), cursor2.getInt(2),
                                cursor2.getInt(3), cursor2.getInt(4)), thiEbene));


                cursor2.moveToNext();
            }

            firEbene.add(new Event_DSs.FirstStrTwArTw(cursor1.getInt(0), secEbene));

            cursor1.moveToNext();
        }


        // DB schliessen
        cursor1.close();
        if (cursor2 != null)
            cursor2.close();
        if (cursor3 != null)
            cursor3.close();
        close();

        return firEbene;
    }



    public void EVENT_deleteEntry(int iID) {
        // DB oeffnen
        open();

        // DS loeschen
        database.delete("EVENT", "ID=" + iID, null);

        // DB schliessen
        close();
    }
    // endregion
}

