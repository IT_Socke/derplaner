package de.derplaner.SQLite;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import de.derplaner.R;
import de.derplaner.dataObjects.DS_Subject;

public class OpenFileActivity extends AppCompatActivity
        implements OnClickListener, OnItemClickListener {

    ListView LvList;
    ArrayList<String> listItems = new ArrayList<>();
    ArrayAdapter<String> adapter;

    Button BtnOK;
    Button BtnCancel;

    String currentPath = null;
    String selectedFilePath = null; /* Full path, i.e. /mnt/sdcard/folder/file.txt */
    String selectedFileName = null; /* File Name Only, i.e file.txt */

    // DataSourse
    private SQL_PlanerDataSource ds;
    // DataSets
    ArrayList<DS_Subject> lSubjects = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_file);

        ds              = new SQL_PlanerDataSource(this);

        try {
            /* Initializing Widgets */
            LvList = (ListView) findViewById(R.id.LvList);
            BtnOK = (Button) findViewById(R.id.BtnOK);
            BtnCancel = (Button) findViewById(R.id.BtnCancel);

            /* Initializing Event Handlers */

            LvList.setOnItemClickListener(this);

            BtnOK.setOnClickListener(this);
            BtnCancel.setOnClickListener(this);

            //

            setCurrentPath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/");
        } catch (Exception ex) {
            Toast.makeText(this,
                    "Error in OpenFileActivity.onCreate: " + ex.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }

    void setCurrentPath(String path) {
        ArrayList<String> folders = new ArrayList<String>();

        ArrayList<String> files = new ArrayList<String>();

        currentPath = path;

        File[] allEntries = new File(path).listFiles();

        for (int i = 0; i < allEntries.length; i++) {
            if (allEntries[i].isDirectory()) {
                folders.add(allEntries[i].getName());
            } else if (allEntries[i].isFile()) {
                files.add(allEntries[i].getName());
            }
        }

        Collections.sort(folders, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });

        Collections.sort(files, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });

        listItems.clear();

        for (int i = 0; i < folders.size(); i++) {
            listItems.add(folders.get(i) + "/");
        }

        for (int i = 0; i < files.size(); i++) {
            listItems.add(files.get(i));
        }

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItems);
        adapter.notifyDataSetChanged();

        LvList.setAdapter(adapter);
    }

    @Override
    public void onBackPressed()
    {
        if (!currentPath.equals(Environment.getExternalStorageDirectory().getAbsolutePath() + "/")) {
            setCurrentPath(new File(currentPath).getParent() + "/");
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;

        switch (v.getId()) {
            case R.id.BtnOK:

                intent = new Intent();
                intent.putExtra("fileName", selectedFilePath);
                intent.putExtra("shortFileName", selectedFileName);
                setResult(RESULT_OK, intent);
                Scanner input = null;
                String sLine;
                try {
                    File filesDir = new File(selectedFilePath, "");
                    input = new Scanner(filesDir);
                }
                catch (Exception ex) {
                    //
                }

                if(input != null) {
                    while (input.hasNext()) {
                        sLine = input.nextLine();
                        sLine = sLine.replace("\"","");
                        switch (sLine) {
                            case "Tablename: SUBJECTS":
                                importSubjects(input);
                                break;
                            case "Tablename: EDITTIMES":
                                break;
                            case "Tablename: GRADES":
                                break;
                            case "Tablename: HOURS":
                                break;
                            case "Tablename: SETTINGS":
                                break;
                            case "Tablename: STUDIP":
                                break;
                        }
                    }
                }

                this.finish();

                break;
            case R.id.BtnCancel:

                intent = new Intent();
                intent.putExtra("fileName", "");
                intent.putExtra("shortFileName", "");
                setResult(RESULT_CANCELED, intent);

                this.finish();

                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        String entryName = (String)parent.getItemAtPosition(position);
        if (entryName.endsWith("/")) {
            setCurrentPath(currentPath + entryName);
        } else {
            selectedFilePath = currentPath + entryName;

            selectedFileName = entryName;

            this.setTitle("TEST open File: "
                    + "[" + entryName + "]");
        }
    }


    private Scanner importSubjects(Scanner input){
        // Deklaration
        String sLine;
        Boolean bHeader = true;
        int iColums = 0;
        DS_Subject mySubi;

        if(input != null) {
            while (input.hasNext()) {
                sLine = input.nextLine();
                sLine = sLine.replace("\"","");

                if(bHeader && !sLine.equals("")) {
                    try {
                        String[] mArray = sLine.split(";");
                        iColums = mArray.length;
                        bHeader = false;
                    } catch (Exception ex) {
                        iColums = 8;
                        bHeader = false;
                    }
                }
                else if(!sLine.equals("")){
                    try {
                        String[] mArray = sLine.split(";");
                        mySubi = new DS_Subject();

                        switch (iColums) {
                            case 8:
                                mySubi.setId(Integer.valueOf(mArray[0]));
                                mySubi.setChildId(Long.valueOf(mArray[1]));
                                mySubi.setSubject(mArray[2]);
                                mySubi.setWishNC(Double.valueOf(mArray[3]));
                                mySubi.setNC(Double.valueOf(mArray[4]));
                                if(mArray[5].equals("1"))
                                    mySubi.setFinalGradeB(true);
                                else
                                    mySubi.setFinalGradeB(false);
                                mySubi.setWertung(Double.valueOf(mArray[6]));
                                mySubi.setCurClass(mArray[7]);

                                lSubjects.add(mySubi);
                                break;
                        }
                    } catch (Exception ex) {
                        //
                    }
                }
                else {
                    break;
                }

            }

            for (DS_Subject item: lSubjects) {
                ds.SUBJECTS_UpdateOrCreateEntry(item);
            }
        }
        return input;
    }
}