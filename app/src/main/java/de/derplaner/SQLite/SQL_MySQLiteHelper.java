package de.derplaner.SQLite;

/**
 * Created by Ice on 25.11.2015.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQL_MySQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "planer.db";
    private static final int DATABASE_VERSION = 1;


    //region Create
    private static final String TABLE_CREATE_CHILD = ""
            +   "CREATE TABLE IF NOT EXISTS CHILD("
            +   "CHILD_ID integer primary key autoincrement, "
            +   "NAME text)";

    private static final String TABLE_CREATE_SUBJECTS = ""
            +   "CREATE TABLE IF NOT EXISTS SUBJECTS("
            +   "ID integer primary key autoincrement, "
            +   "CHILD_ID long, "
            +   "SUBJECT text, "
            +   "WISH_NC numeric, "
            +   "NC numeric, "
            +   "FINAL_GRADE boolean, "
            + 	"WERTUNG numeric, "
            +   "CLASS text)";

    private static final String TABLE_CREATE_GRADES = ""
            +   "CREATE TABLE IF NOT EXISTS GRADES("
            +   "ID integer primary key autoincrement, "
            +   "CHILD_ID integer, "
            +   "SUBJECT text, "
            +   "THEMA text, "
            +   "TEACHER text, "
            +   "DATE date, "
            +   "GRADE integer, "
            + 	"INFO text)";

    private static final String TABLE_CREATE_HOUR = ""
            +   "CREATE TABLE IF NOT EXISTS HOURS("
            +   "ID integer primary key autoincrement, "
            +   "CHILD_ID integer, "
            +   "SUBJECT text, "
            +   "SHORT text, "
            +   "ROOM text, "
            +   "TEACHER text, "
            +   "HOUR integer, "
            +   "DAY integer, "
            +   "BORDER text, "
            +   "TV text, "
            + 	"COLOR integer, "
            +   "WEEK text)";

    private static final String TABLE_CREATE_STUDIP = ""
            +   "CREATE TABLE IF NOT EXISTS STUDIP("
            +   "ID integer primary key autoincrement, "
            +   "CHILD_ID integer, "
            +   "SUBJECT text, "
            +   "ROOM text, "
            +   "STARTTIME integer, "
            +   "ENDTIME integer, "
            +   "DAY integer, "
            + 	"COLOR integer)";

    private static final String TABLE_CREATE_EDITTIMES = ""
            +   "CREATE TABLE IF NOT EXISTS EDITTIMES("
            +   "ID integer primary key autoincrement, "
            +   "HOUR text, "
            +   "FROM_TIME text, "
            +   "TO_TIME text)";

    private static final String TABLE_CREATE_EVENT = ""
            +   "CREATE TABLE IF NOT EXISTS EVENT("
            +   "ID integer primary key autoincrement, "
            +   "CHILD_ID integer, "
            +   "TITLE text, "
            +   "DATE integer, "
            +   "COLOR integer, "
            +   "DRAWABLE text, "
            +   "SUBJECT text, "
            +   "HOUR integer, "
            +   "THEMA text, "
            +   "INFO text)";

    private static final String TABLE_CREATE_CALENDAR = ""
            +   "CREATE TABLE IF NOT EXISTS CALENDAR("
            +   "ID integer primary key autoincrement, "
            +   "DATE date, "
            +   "INFO text, "
            +   "HOUR integer, "
            +   "THEMA text, "
            +   "NOTIZ text)";

    private static final String TABLE_CREATE_SETTINGS = ""
            +   "CREATE TABLE IF NOT EXISTS SETTINGS("
            +   "ID integer primary key autoincrement, "
            +   "NAME text, "
            +   "VALUE text)";

    private static final String TABLE_CREATE_SERVICE = ""
            +   "CREATE TABLE IF NOT EXISTS SERVICE("
            +   "ID integer primary key autoincrement, "
            +   "NAME text, "
            +   "VALUE text)";
    //endregion

    //region Drop
    private static final String TABLE_DROP_CHILD = ""
            +"DROP TABLE IF EXISTS CHILD";

    private static final String TABLE_DROP_SUBJECTS = ""
            +"DROP TABLE IF EXISTS SUBJECTS";

    private static final String TABLE_DROP_GRADES = ""
            +"DROP TABLE IF EXISTS GRADES";

    private static final String TABLE_DROP_HOUR = ""
            +"DROP TABLE IF EXISTS HOURS";

    private static final String TABLE_DROP_STUDIP = ""
            +"DROP TABLE IF EXISTS STUDIP";

    private static final String TABLE_DROP_EDITTIMES = ""
            +"DROP TABLE IF EXISTS EDITTIMES";

    private static final String TABLE_DROP_EVENT = ""
            +"DROP TABLE IF EXISTS EVENT";

    private static final String TABLE_DROP_CALENDAR = ""
            +"DROP TABLE IF EXISTS CALENDAR";

    private static final String TABLE_DROP_SETTINGS = ""
            +"DROP TABLE IF EXISTS SETTINGS";

    private static final String TABLE_DROP_SERVICE = ""
            +"DROP TABLE IF EXISTS SERVICE";

    //endregion


    public SQL_MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        // Erstellen der Tabellen
        database.execSQL(TABLE_CREATE_SUBJECTS);
        database.execSQL(TABLE_CREATE_GRADES);
        database.execSQL(TABLE_CREATE_HOUR);
        database.execSQL(TABLE_CREATE_STUDIP);
        database.execSQL(TABLE_CREATE_EDITTIMES);
        database.execSQL(TABLE_CREATE_EVENT);
        database.execSQL(TABLE_CREATE_CALENDAR);
        database.execSQL(TABLE_CREATE_SETTINGS);
        database.execSQL(TABLE_CREATE_SERVICE);
        database.execSQL(TABLE_CREATE_CHILD);
    }

    public void resetDB(SQLiteDatabase database) {
        // Löschen der Tabellen
        database.execSQL(TABLE_DROP_SUBJECTS);
        database.execSQL(TABLE_DROP_GRADES);
        database.execSQL(TABLE_DROP_HOUR);
        database.execSQL(TABLE_DROP_STUDIP);
        database.execSQL(TABLE_DROP_EDITTIMES);
        database.execSQL(TABLE_DROP_EVENT);
        database.execSQL(TABLE_DROP_CALENDAR);
        database.execSQL(TABLE_DROP_SETTINGS);
        database.execSQL(TABLE_DROP_SERVICE);
        database.execSQL(TABLE_DROP_CHILD);

        // Tabellen neu erzeugen
        onCreate(database);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Log.w(SQL_MySQLiteHelper.class.getName(),
        //        "Upgrading database from version " + oldVersion + " to "
        //                + newVersion + ", which will destroy all old data");
        //db.execSQL("DROP TABLE IF EXISTS SCANITEM");
        //onCreate(db);
    }
}