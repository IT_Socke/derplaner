package de.derplaner.SQLite;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;


public class SQL_ExportImportDB {
    private SQL_PlanerDataSource ds;
    private Activity acParent;

    public SQL_ExportImportDB(SQL_PlanerDataSource db, Activity parent) {
        this.ds = db;
        this.acParent = parent;
    }

    // Datenbank importieren
    public void importDB() {

        if (shouldAskPermissions()) {
            askPermissions();
        }

        try {
            acParent.startActivity(new Intent(acParent, OpenFileActivity.class));
        }
        catch (Exception ex) {
            int i = 0;
        }
    }

    // Datenbank exportieren
    public void exportDB() {
        // Deklaration & Initialisierung
        //String ChildID = "0";

        // Kinder auslesen
        //ArrayList<DS_Child> lChild = ds.getAllEntries_CHILD();

        if (shouldAskPermissions()) {
            askPermissions();
        }

        File exportDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "");
        boolean isDirectoryCreated=exportDir.exists();
        if (!isDirectoryCreated) {
            isDirectoryCreated= exportDir.mkdir();
        }

        if(isDirectoryCreated) {

            File file = new File(exportDir, "DerPlanerExport.csv");
            try {
                CSVWriter csvWrite = new CSVWriter(new FileWriter(file), ';');

                csvWrite.writeNext(new String[]{"Der Planer - Export"});
                csvWrite.writeNext(new String[]{""});
                /*csvWrite.writeNext(new String[]{"Kind-ID: 0"});
                csvWrite.writeNext(new String[]{""});*/

                writeCSV(csvWrite, ds, "SUBJECTS", ds.selectAllFromSUBJECTS);
                writeCSV(csvWrite, ds, "EDITTIMES", ds.selectAllFromEDITTIMES);
                writeCSV(csvWrite, ds, "GRADES", ds.selectAllFromGRADES);
                writeCSV(csvWrite, ds, "HOURS", ds.selectAllFromHOURS);
                writeCSV(csvWrite, ds, "SETTINGS", ds.selectAllFromSETTINGS);
                writeCSV(csvWrite, ds, "STUDIP", ds.selectAllFromSTUDIP);
                csvWrite.close();
            } catch (Exception sqlEx) {
                Log.e("MainActivity", sqlEx.getMessage(), sqlEx);
            }
        }
    }

    private void writeCSV(CSVWriter csvWrite, SQL_PlanerDataSource ds, String sTableName, String sSelectAll) {
        try {
            ds.open();
            SQLiteDatabase db = ds.getDB();
            Cursor curCSV = db.rawQuery(sSelectAll, null);
            csvWrite.writeNext(new String[]{"Tablename: " + sTableName});
            csvWrite.writeNext(curCSV.getColumnNames());
            while (curCSV.moveToNext()) {
                //Which column you want to exprort
                String arrStr[] = new String[curCSV.getColumnCount()];
                for(int i = 0; i < curCSV.getColumnCount(); i++) {
                    arrStr[i] = curCSV.getString(i);
                }
                csvWrite.writeNext(arrStr);
            }
            csvWrite.writeNext(new String[]{""});
            curCSV.close();
            ds.close();
        } catch (Exception sqlEx) {
            Log.e("MainActivity", sqlEx.getMessage(), sqlEx);
        }
    }

    private boolean shouldAskPermissions() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(23)
    private void askPermissions() {
        String[] permissions = {
                "android.permission.READ_EXTERNAL_STORAGE",
                "android.permission.WRITE_EXTERNAL_STORAGE"
        };
        int requestCode = 200;
        acParent.requestPermissions(permissions, requestCode);
    }
}
