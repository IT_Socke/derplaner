package de.derplaner;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import java.util.Locale;

import de.derplaner.SQLite.SQL_PlanerDataSource;
import de.derplaner.activities.menu.SideBar;
import de.derplaner.another.constant;


public class Main extends AppCompatActivity{
    // Datenbank
    private SQL_PlanerDataSource ds;

    // AltertDialoge.Bulter & AlertDialog
    private AlertDialog alert;
    private Locale locale = null;

    // UI
    EditText edWishGrade;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Layout setzen
        setContentView(R.layout.act_blank);

        // Init
        ds                          = new SQL_PlanerDataSource(this);
        LayoutInflater inflater     = this.getLayoutInflater();
        SharedPreferences settings  = PreferenceManager.getDefaultSharedPreferences(this);
        String lang                 = settings.getString("LOCAL", "");

        // Sprache setzen
        if(!lang.isEmpty())
            setLocale(lang);

        // Abfrage ob die App schonmal gestartet wurde
        if(ds.SETTINGS_getOneEntries("FirstStart") != null) {
            if (ds.SETTINGS_getOneEntries("FirstStart").toUpperCase().equals("TRUE")){
                startActivity(new Intent(this, SideBar.class));
                return;
            }
        }

        // AD & Dismiss bilden - FirstStart Text
        alert = ADStartLongClick(ds);
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                alert.show();
            }
        });

        // AD FirstStart aufrufen
        alert.show();

        // AD & Dismiss bilden - FirstConfig Text
        alert = ADStartConfig(inflater);
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                startActivity(new Intent(getApplication(), SideBar.class));
            }
        });
    }

    private void setLocale(String lang) {
        String sLangShort;
        if (lang.toUpperCase().equals("DEUTSCH"))
            sLangShort = "de";
        else if (lang.toUpperCase().equals("ENGLISCH"))
            sLangShort = "en";
        else
            sLangShort = "en";

        locale = new Locale(sLangShort);
        Locale.setDefault(locale);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        settings.edit().putString("LOCAL",lang).apply();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        if (locale != null)
        {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }


    /*public void wdgUpdate()
    {
        Intent intent = new Intent(this, WidgetProvider1.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
// Use an array and EXTRA_APPWIDGET_IDS instead of AppWidgetManager.EXTRA_APPWIDGET_ID,
// since it seems the onUpdate() is only fired on that:
        int ids[] = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(new ComponentName(getApplication(), WidgetProvider1.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
        sendBroadcast(intent);
    }*/

    private AlertDialog ADStartConfig(LayoutInflater inflater)
    {
        @SuppressLint("InflateParams")
        final View vStart    = inflater.inflate(R.layout.scv_first_config, null);
        AlertDialog.Builder builder;

        edWishGrade = (EditText) vStart.findViewById(R.id.edWishGrade);

        // Befuellen der Spinner
        // Nutzer waehlen
        Spinner sp_user = (Spinner)vStart.findViewById(R.id.spNutzer);
        String[] items_user = new String[]{"Schüler"};
        ArrayAdapter<String> adapter_user = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items_user);
        sp_user.setAdapter(adapter_user);

        // Sprache waehlen
        Spinner sp_language = (Spinner)vStart.findViewById(R.id.spSprache);
        final String[] items_language = new String[]{"Deutsch", "Englisch"};
        ArrayAdapter<String> adapter_language = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items_language);
        sp_language.setAdapter(adapter_language);

        // Notensystem waehlen
        Spinner sp_grades = (Spinner)vStart.findViewById(R.id.spNotensystem);
        String[] items_grade = new String[]{"Noten 1-6", "Punkte 1-15"};
        ArrayAdapter<String> adapter_grades = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items_grade);
        sp_grades.setAdapter(adapter_grades);

        // Klasse waehlen
        Spinner sp_class = (Spinner)vStart.findViewById(R.id.spAktKlasse);
        String[] items_class = new String[]{"1","2","3","4","5","6","7","8","9","10","11","12","13","14"};
        ArrayAdapter<String> adapter_class = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items_class);
        sp_class.setAdapter(adapter_class);

        // Erste Stunde waehlen
        final Spinner sp_firstHour = (Spinner)vStart.findViewById(R.id.spErsteStunde);
        String[] items_firstHour = new String[]{"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"};
        ArrayAdapter<String> adapter_firstHour = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items_firstHour);
        sp_firstHour.setAdapter(adapter_firstHour);

        // Letzte Stunde waehlen
        final Spinner sp_lastHour = (Spinner)vStart.findViewById(R.id.spLetzteStunde);
        String[] items_lastHour = new String[]{"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"};
        ArrayAdapter<String> adapter_lastHour = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items_lastHour);
        sp_lastHour.setAdapter(adapter_lastHour);

        sp_language.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String test = items_language[position];
                setLocale(test);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Eigene RadioGroup bauen
        final RadioButton cbNON = (RadioButton) vStart.findViewById(R.id.rdKeineBW);
        final RadioButton cbGUG = (RadioButton) vStart.findViewById(R.id.rdBwGUG);
        final RadioButton cbABC = (RadioButton) vStart.findViewById(R.id.rdBwABC);
        cbNON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRadioButton(v.getId(), vStart);
            }
        });
        cbGUG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRadioButton(v.getId(), vStart);
            }
        });
        cbABC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRadioButton(v.getId(), vStart);
            }
        });


        // AlertDialog zusammensetzen
        // TODO Beschreibung hinzufügen was alles gesetzt wird
        builder = new AlertDialog.Builder(inflater.getContext())
                .setCancelable(false)
                .setView(vStart)
                .setPositiveButton("Fertigstellen", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Click_btn_welcome(sp_firstHour.getSelectedItem().toString(), sp_lastHour.getSelectedItem().toString());
                        if(cbNON.isChecked())
                            ds.SETTINGS_UpdateOrCreateEntry(constant.SET_TTWeek, constant.TTWEEK[0]);
                        else if(cbGUG.isChecked())
                            ds.SETTINGS_UpdateOrCreateEntry(constant.SET_TTWeek, constant.TTWEEK[1]);
                        else if(cbABC.isChecked())
                            ds.SETTINGS_UpdateOrCreateEntry(constant.SET_TTWeek, constant.TTWEEK[2]);


                        dialog.dismiss();
                        dialog.cancel();

                    }
                })
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            dialog.dismiss();
                            dialog.cancel();
                        }
                        return true;
                    }
                });

        // Rueckgabe des AlertDialoges
        return builder.create();
    }


    public AlertDialog ADStartLongClick(SQL_PlanerDataSource db)
    {
        @SuppressLint("InflateParams")
        View vStart  = getLayoutInflater().inflate(R.layout.scv_welcome, null);
        ds      = db;

        // AlertDialog zusammensetzen
        // TODO Beschreibung hinzufügen was alles gesetzt wird
        AlertDialog.Builder builder = new AlertDialog.Builder(getLayoutInflater().getContext())
                .setCancelable(false)
                .setView(vStart)
                .setPositiveButton("Weiter", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        dialog.cancel();

                    }
                })
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            dialog.dismiss();
                            dialog.cancel();
                        }
                        return true;
                    }
                });

        // Erstellen des AlertDialoges
        alert = builder.create();

        return alert;
    }


    // Eigene RadioGroup
    private void setRadioButton(int id, View vStart)
    {
        ((RadioButton) vStart.findViewById(R.id.rdKeineBW)).setChecked(false);
        ((RadioButton) vStart.findViewById(R.id.rdBwABC)).setChecked(false);
        ((RadioButton) vStart.findViewById(R.id.rdBwGUG)).setChecked(false);
        ((RadioButton) vStart.findViewById(id)).setChecked(true);
    }


    // OnClickListener
    private void Click_btn_welcome(String txt1, String txt2) {
        // Init der DS in der DB
        // TODO Beschreibung
        ds.SETTINGS_UpdateOrCreateEntry("FirstStart", "TRUE");
        ds.SETTINGS_UpdateOrCreateEntry("AD_CalendarEntry","FLASE");

        ds.SETTINGS_UpdateOrCreateEntry("FirstHour", txt1);
        ds.SETTINGS_UpdateOrCreateEntry("LastHour", txt2);

        ds.SETTINGS_UpdateOrCreateEntry("Service_AGSstudent", "FLASE");
        ds.SETTINGS_UpdateOrCreateEntry("Service_RemindDate", "FLASE");

        ds.SETTINGS_UpdateOrCreateEntry("EveDra1", String.valueOf(R.drawable.speaking_black));
        ds.SETTINGS_UpdateOrCreateEntry("EveDra2", String.valueOf(R.drawable.pencil_black));
        ds.SETTINGS_UpdateOrCreateEntry("EveDra3", String.valueOf(R.drawable.document_black));
        ds.SETTINGS_UpdateOrCreateEntry("EveCol1", String.valueOf(Color.BLUE));
        ds.SETTINGS_UpdateOrCreateEntry("EveCol2", String.valueOf(Color.RED));
        ds.SETTINGS_UpdateOrCreateEntry("EveCol3", String.valueOf(Color.GREEN));



        if(!edWishGrade.getText().toString().isEmpty())
            ds.SETTINGS_UpdateOrCreateEntry(constant.SET_WishGrade, edWishGrade.getText().toString());
        else
            ds.SETTINGS_UpdateOrCreateEntry(constant.SET_WishGrade, edWishGrade.getHint().toString());
    }
}
