package de.derplaner.dataObjects;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DS_Grade {
    private int         _id;
    private long        _childId;
    private String      _subject;
    private String      _thema;
    private String      _teacher;
    private Date        _date;
    private double      _grade;
    private String      _info;

    public int getId() {
        return _id;
    }
    public void setId(int id) {
        this._id = id;
    }

    public long getChildId() {
        return _childId;
    }
    public void setChildId(long childId) {
        this._childId = childId;
    }

    public String getSubject () {
        return _subject;
    }
    public void setSubject (String subject){
        this._subject = subject;
    }

    public String getThema () {
        return _thema;
    }
    public void setThema (String thema){
        this._thema = thema;
    }

    public String getTeacher () {
        return _teacher;
    }
    public void setTeacher (String teacher){
        this._teacher = teacher;
    }

    public String getDate () {
        SimpleDateFormat simpleDate =  new SimpleDateFormat("dd.MM.yyyy");

        String sDate = simpleDate.format(_date);
        return sDate;
    }
    public void setDate (String sDate){
        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

        try {
            date = format.parse(sDate);
        } catch (ParseException e) {
            date = Calendar.getInstance().getTime();
        }


        this._date = date;
    }

    public double getGrade () {
        return _grade;
    }
    public void setGrade (double grade){
        this._grade = grade;
    }

    public String getInfo () {
        return _info;
    }
    public void setInfo (String info){
        this._info = info;
    }

}




