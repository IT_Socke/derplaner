package de.derplaner.dataObjects;

public class DS_Event {
    private int         _id             = -1;
    private long        _childId        = 0;
    private String      _title          = "";
    private Integer     _date           = 0;
    private Integer     _Color          = 0;
    private Integer     _Drw            = 0;
    private String      _subject        = "";
    private int         _hour           = 0;
    private String      _thema          = "";
    private String      _info           = "";

    public int getId() {
        return _id;
    }
    public void setId(int id) {
        this._id = id;
    }

    public long getChildId() {
        return _childId;
    }
    public void setChildId(long childId) {
        this._childId = childId;
    }

    public String getTitle () {
        return _title;
    }
    public void setTitle (String title){
        this._title = title;
    }

    public Integer getDate () {
        return _date;
    }
    public void setDate (Integer sDate){
        this._date = sDate;
    }

    public Integer getColor() {
        return _Color;
    }
    public void setColor(Integer Color) {
        this._Color = Color;
    }

    public Integer getDrw() {
        return _Drw;
    }
    public void setDrw(Integer Drw) {
        this._Drw = Drw;
    }

    public String getSubject () {
        return _subject;
    }
    public void setSubject (String subject){
        this._subject = subject;
    }

    public int getHour () {
        return _hour;
    }
    public void setHour (int hour){
        this._hour = hour;
    }

    public String getThema () {
        return _thema;
    }
    public void setThema (String thema){
        this._thema = thema;
    }

    public String getInfo () {
        return _info;
    }
    public void setInfo (String info){
        this._info = info;
    }
}




