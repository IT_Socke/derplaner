package de.derplaner.dataObjects;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DS_NewTimes {
    private int         _id;
    private Date        _date;
    private String      _info;
    private int         _hour;
    private String      _thema;
    private String      _notiz;

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        this._id = id;
    }

    public String getNotiz () {
        return _notiz;
    }

    public void setNotiz (String notiz){
        this._notiz = notiz;
    }

    public String getThema () {
        return _thema;
    }

    public void setThema (String thema){
        this._thema = thema;
    }

    public String getDate () {
        SimpleDateFormat simpleDate =  new SimpleDateFormat("dd.MM.yyyy");

        String sDate = simpleDate.format(_date);
        return sDate;
    }

    public void setDate (String sDate){
        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

        try {
            date = format.parse(sDate);
        } catch (ParseException e) {
            date = Calendar.getInstance().getTime();
        }


        this._date = date;
    }

    public int getHour () {
        return _hour;
    }

    public void setHour (int hour){
        this._hour = hour;
    }

    public String getInfo () {
        return _info;
    }

    public void setInfo (String info){
        this._info = info;
    }

}




