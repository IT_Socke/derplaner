package de.derplaner.dataObjects;

public class DS_StudIP {
    private int         _id;
    private long        _childId    = 0;
    private String      _subject    = "";
    private String      _room       = "";
    private int         _startTime  = 0;
    private int         _endTime    = 0;
    private int         _day        = 0;


    public int getId() {
        return _id;
    }
    public void setId(int id) {
        this._id = id;
    }


    public long getChildId() {
        return _childId;
    }
    public void setChildId(long childId) {
        this._childId = childId;
    }


    public String getSubject () {
        return _subject;
    }
    public void setSubject (String subject){
        this._subject = subject;
    }


    public String getRoom () {
        return _room;
    }
    public void setRoom (String room){
        this._room = room;
    }


    public int getStartTime () {
        return _startTime;
    }
    public void setStartTime (int startTime){
        this._startTime = startTime;
    }


    public int getEndTime () {
        return _endTime;
    }
    public void setEndTime (int endTime){
        this._endTime = endTime;
    }


    public int getDay () {
        return _day;
    }
    public void setDay (int day){
        this._day = day;
    }
}




