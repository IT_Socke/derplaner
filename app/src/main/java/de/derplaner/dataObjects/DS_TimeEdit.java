package de.derplaner.dataObjects;

public class DS_TimeEdit {
    private int             _id;
    private String         _hour        = "0";
    private String         _fromTime    = ":";
    private String         _toTime      = ":";


    public int getId() {
        return _id;
    }
    public void setId(int id) {
        this._id = id;
    }


    public String getHour () {
        return _hour;
    }
    public void setHour (String hour){
        this._hour = hour;
    }


    public String getFromTime () {
        return _fromTime;
    }
    public void setFromTime (String fromTime){
        this._fromTime = fromTime;
    }


    public String getToTime () {
        return _toTime;
    }
    public void setToTime (String toTime){
        this._toTime = toTime;
    }


}




