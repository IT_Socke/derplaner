package de.derplaner.dataObjects;

public class DS_Child {
    private int        _id;
    private String      _name;
    private String      _vorname;

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        this._id = id;
    }

    public String getName () {
        return _name;
    }

    public void setName (String name){
        this._name = name;
    }

    public String getVorname () {
        return _vorname;
    }

    public void setVorname (String vorname){
        this._vorname = vorname;
    }
}




