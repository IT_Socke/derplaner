package de.derplaner.dataObjects;

public class DS_Subject {
    private int _id;
    private long _childId           = 0;
    private String _subject         = "";
    private double _NC              = 0.00;
    private double _WishNC          = 0.00;
    private Boolean _FinalGradeB    = false;
    private double _Wertung         = 1.00;
    private String _class           = "";

    public int getId() {
        return _id;
    }
    public void setId(int id) {
        this._id = id;
    }

    public long getChildId() {
        return _childId;
    }
    public void setChildId(long childId) {
        this._childId = childId;
    }

    public String getSubject () {
        return _subject;
    }
    public void setSubject (String subject){
        this._subject = subject;
    }

    public double getNC () {
        return _NC;
    }
    public void setNC (double NC){
        this._NC = NC;
    }

    public double getWishNC () {
        return _WishNC;
    }
    public void setWishNC (double WishNC){
        this._WishNC = WishNC;
    }

    public Boolean getFinalGradeB () {
        return _FinalGradeB;
    }
    public void setFinalGradeB (Boolean FinalGradeB){
        this._FinalGradeB = FinalGradeB;
    }

    public Double getWertung () {
        return _Wertung;
    }
    public void setWertung (Double Wertung){
        this._Wertung = Wertung;
    }

    public String getCurClass () {
        return _class;
    }
    public void setCurClass (String curClass){
        this._class = curClass;
    }
}




