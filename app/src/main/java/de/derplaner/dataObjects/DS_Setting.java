package de.derplaner.dataObjects;

public class DS_Setting {
    private int        _id;
    private String      _name;
    private String      _value;

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        this._id = id;
    }

    public String getName () {
        return _name;
    }

    public void setName (String name){
        this._name = name;
    }

    public String getValue () {
        return _value;
    }

    public void setValue (String value){
        this._value = value;
    }
}




