package de.derplaner.dataObjects;


import android.graphics.Color;

public class DS_Hour {
    private int         _id;
    private long        _childId    = 0;
    private String      _subject    = "";
    private String      _sShort     = "";
    private String      _room       = "";
    private String      _teacher    = "";
    private int         _hour       = 0;
    private int         _day        = 0;
    private int         _color      = Color.parseColor("#EEEEEE");
    private String      _border     = "NO";
    private String      _tv         = "NO";
    private String      _week       = "";


    public int getId() {
        return _id;
    }
    public void setId(int id) {
        this._id = id;
    }


    public long getChildId() {
        return _childId;
    }
    public void setChildId(long childId) {
        this._childId = childId;
    }


    public String getSubject () {
        return _subject;
    }
    public void setSubject (String subject){
        this._subject = subject;
    }


    public String getShort () {
        return _sShort;
    }
    public void setShort (String sShort){
        this._sShort = sShort;
    }


    public String getRoom () {
        return _room;
    }
    public void setRoom (String room){
        this._room = room;
    }


    public String getTeacher () {
        return _teacher;
    }
    public void setTeacher (String teacher){
        this._teacher = teacher;
    }


    public int getHour () {
        return _hour;
    }
    public void setHour (int hour){
        this._hour = hour;
    }


    public int getDay () {
        return _day;
    }
    public void setDay (int day){
        this._day = day;
    }


    public int getColor () {
        return _color;
    }
    public void setColor (int color){
        this._color = color;
    }


    public String getBorder () {
        return _border;
    }
    public void setBorder (String border){
        this._border = border;
    }


    public String getTV () {
        return _tv;
    }
    public void setTV (String tv){
        this._tv = tv;
    }


    public String getWeek () {
        return _week;
    }
    public void setWeek (String week){
        this._week = week;
    }

}




