package de.derplaner.dataObjects;

import android.graphics.Color;

import java.util.ArrayList;


public class Event_DSs {
    public static class ThirdTwoString {

        private int _id         = -1;
        private String _str1    = "";
        private String _str2    = "";
        private Integer _Int    = 0;
        private Integer _Color  = Color.WHITE;
        private Integer _drw    = 0;

        // Konstruktor
        public ThirdTwoString(Integer id, String str1, Integer iInt, Integer iColor, Integer drw) {
            this._id    = id;
            this._str1  = str1;
            this._Int   = iInt;
            this._Color = iColor;
            this._drw   = drw;
        }

        public ThirdTwoString(String str1, String str2) {
            this._str1 = str1;
            this._str2 = str2;
        }

        ThirdTwoString() {
        }

        public int getId() {
            return _id;
        }
        public void setId(int id) {
            this._id = id;
        }


        public String get_str1() {
            return _str1;
        }
        public void set_str1(String _str1) {
            this._str1 = _str1;
        }


        public String get_str2() {
            return _str2;
        }
        public void set_str2(String _str2) {
            this._str2 = _str2;
        }


        public Integer getColor() {
            return _Color;
        }
        public void setColor(Integer _Color) {
            this._Color = _Color;
        }

        public Integer getDrw() {
            return _drw;
        }
        public void setDrw(Integer _drw) {
            this._drw = _drw;
        }
    }

    public static class SecondTwoStrArLTwStr{
        private ThirdTwoString twoStr                = new ThirdTwoString();
        private ArrayList<ThirdTwoString> alTwoStr   = new ArrayList<>();

        public SecondTwoStrArLTwStr(ThirdTwoString twoStr, ArrayList<ThirdTwoString> alTwoStr){
            this.twoStr     = twoStr;
            this.alTwoStr   = alTwoStr;
        }


        public ThirdTwoString getTwoStr() {
            return twoStr;
        }
        public void setTwoStr(ThirdTwoString twoStr) {
            this.twoStr = twoStr;
        }

        public ArrayList<ThirdTwoString> getAlTwoStr() {
            return alTwoStr;
        }
        public void setAlTwoStr(ArrayList<ThirdTwoString> alTwoStr) {
            this.alTwoStr = alTwoStr;
        }
    }

    public static class FirstStrTwArTw{
        private Integer iInt                                = -1;
        private ArrayList<SecondTwoStrArLTwStr> hashTwAlTw  = new ArrayList<>();

        public FirstStrTwArTw(Integer iInt, ArrayList<SecondTwoStrArLTwStr> hashTwAlTw){
            this.iInt        = iInt;
            this.setHashTwAlTw(hashTwAlTw);
        }


        public Integer getInt() {
            return iInt;
        }
        public void setInt(Integer iInt) {
            this.iInt = iInt;
        }

        public ArrayList<SecondTwoStrArLTwStr> getHashTwAlTw() {
            return hashTwAlTw;
        }
        public void setHashTwAlTw(ArrayList<SecondTwoStrArLTwStr> hashTwAlTw) {
            this.hashTwAlTw = hashTwAlTw;
        }
    }
}
