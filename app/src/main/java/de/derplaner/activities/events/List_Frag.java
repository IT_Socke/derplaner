package de.derplaner.activities.events;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import de.derplaner.R;
import de.derplaner.SQLite.SQL_PlanerDataSource;
import de.derplaner.another.constant;
import de.derplaner.dataObjects.DS_Event;
import de.derplaner.dataObjects.Event_DSs;

public class List_Frag extends Fragment {
    // Globale Deklaration
    private EventExpListAdapter     evAdapter;
    private ExpandableListView      eventList;
    private LayoutInflater          inflater;
    private ViewGroup               parent;
    private SQL_PlanerDataSource    ds;
    private TextView                tv1, tv2, tv3;
    private Integer                 int1 = 0, int2 = 0, int3 = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        // Deklaration
        ArrayList<Integer> times = new ArrayList<>();
        ArrayAdapter<String> adaDate;
        String[] sArr;

        // Initialisierung
        ds                              = new SQL_PlanerDataSource(getActivity());
        ArrayList<Event_DSs.FirstStrTwArTw> data        = ds.Event_GetAllEntrys("0");
        View rootView                   = inflater.inflate(R.layout.act_event_list, container, false);
        ImageView imgView1              = (ImageView) rootView.findViewById(R.id.imgDrw1);
        ImageView imgView2              = (ImageView) rootView.findViewById(R.id.imgDrw2);
        ImageView imgView3              = (ImageView) rootView.findViewById(R.id.imgDrw3);
        final Spinner spTime            = (Spinner) rootView.findViewById(R.id.spTime);
        this.inflater                   = inflater;
        this.parent                     = container;
        eventList                       = (ExpandableListView) rootView.findViewById(R.id.elvMain);
        tv1                             = (TextView) rootView.findViewById(R.id.textView11);
        tv2                             = (TextView) rootView.findViewById(R.id.textView12);
        tv3                             = (TextView) rootView.findViewById(R.id.textView13);

        // Tvs Initial setzen
        tv1.setText(String.valueOf(int1));
        tv2.setText(String.valueOf(int2));
        tv3.setText(String.valueOf(int3));

        // Datumsangaben setzen
        sArr = getActivity().getResources().getStringArray(R.array.event_times);

        // ???
        // BackgroundDrawable setzen
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            setBackground(imgView1, ContextCompat.getDrawable(getActivity(),
                    parseStringToInteger(ds.SETTINGS_getOneEntries("EveDra1"))));
            setBackground(imgView2, ContextCompat.getDrawable(getActivity(),
                    parseStringToInteger(ds.SETTINGS_getOneEntries("EveDra2"))));
            setBackground(imgView3, ContextCompat.getDrawable(getActivity(),
                    parseStringToInteger(ds.SETTINGS_getOneEntries("EveDra3"))));
        }
        else {
            setBackground16(imgView1, ContextCompat.getDrawable(getActivity(),
                    parseStringToInteger(ds.SETTINGS_getOneEntries("EveDra1"))));
            setBackground16(imgView2, ContextCompat.getDrawable(getActivity(),
                    parseStringToInteger(ds.SETTINGS_getOneEntries("EveDra2"))));
            setBackground16(imgView3, ContextCompat.getDrawable(getActivity(),
                    parseStringToInteger(ds.SETTINGS_getOneEntries("EveDra3"))));
        }

        // Datums Adapter erstellen & setzen
        adaDate = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, sArr);
        spTime.setAdapter(adaDate);
        spTime.setSelection(4);

        // ItemSelectListener setzen
        spTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        // Times auslesen, als Header der ExpList
        for (Event_DSs.FirstStrTwArTw item: data) {
            times.add(item.getInt());
        }

        // Keine Klickbare Gruppe erstellen
        eventList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPos, long id) {
                if(!parent.isGroupExpanded(groupPos))
                    parent.expandGroup(groupPos);
                return true;
            }
        });

        // Adapter erstellen & setzen
        evAdapter = new EventExpListAdapter(getActivity(), times, data, eventList);
        eventList.setAdapter(evAdapter);

        return rootView;
    }


    /**
     * @see String Ersteller - Tobias Häuser
     * @see String Anzeigen des Dialogs zum hinzufuegen eines Events
     */
    public void update(Bundle bundle) {
        // Variablen
        String key = "";
        if(bundle != null)
            key = bundle.getString("KEY");

        // Event hinzufuegen
        if(key != null && key.equals("ADD_EV"))
            addEvent(null);
        else
            update();
    }


    /**
     * @see String Ersteller - Tobias Häuser
     * @see String Daten neu auslesen und den Adapter der Fragmentlste aktualisieren
     */
    void update(){
        // Deklaration
        ArrayList<Event_DSs.FirstStrTwArTw> data;
        ArrayList<Integer>                  alData  = new ArrayList<>();

        // Daten auslesen
        data = ds.Event_GetAllEntrys("0");
        for (Event_DSs.FirstStrTwArTw item: data) {
            alData.add(item.getInt());
        }

        // Adapter setzen & aktualisieren
        int1 = 0; int2 = 0; int3 = 0;
        evAdapter.setData(alData, data);
        eventList.setAdapter(evAdapter);
    }


    /**
     * @see String Ersteller - Tobias Häuser
     * @see String Daten neu auslesen und den Adapter der Fragmentlste aktualisieren
     */
    void update(Integer iPos){
        // Deklaration
        ArrayList<Event_DSs.FirstStrTwArTw> data;

        // Initialisierung
        ArrayList<Integer> alData   = new ArrayList<>();
        Calendar cal0               = Calendar.getInstance();
        Calendar cal1               = Calendar.getInstance();

        // ENtscheidung nach selectieren Pos im Timespinner
        switch (iPos){
            case 0:
                // Letztes Jahr
                cal0.add(Calendar.YEAR, -1);
                data = ds.Event_GetAllEntrys("0", cal0, cal1);
                break;
            case 1:
                // Letzte 6 Monate
                cal0.add(Calendar.MONTH, -6);
                data = ds.Event_GetAllEntrys("0", cal0, cal1);
                break;
            case 2:
                // Letzer Monat
                cal0.add(Calendar.MONTH, -1);
                data = ds.Event_GetAllEntrys("0", cal0, cal1);
                break;
            case 3:
                // Letzte Woche
                cal0.add(Calendar.WEEK_OF_YEAR, -1);
                data = ds.Event_GetAllEntrys("0", cal0, cal1);
                break;
            case 4:
                // Alle
                data = ds.Event_GetAllEntrys("0");
                break;
            case 5:
                // Heute
                data = ds.Event_GetAllEntrys("0", cal0, cal1);
                break;
            case 6:
                // Naechste Woche
                cal1.add(Calendar.WEEK_OF_YEAR, 1);
                data = ds.Event_GetAllEntrys("0", cal0, cal1);
                break;
            case 7:
                // Naechster Monat
                cal1.add(Calendar.MONTH, 1);
                data = ds.Event_GetAllEntrys("0", cal0, cal1);
                break;
            case 8:
                // Naechste 6 Monate
                cal1.add(Calendar.MONTH, 6);
                data = ds.Event_GetAllEntrys("0", cal0, cal1);
                break;
            case 9:
                // Naechstes Jahr
                cal1.add(Calendar.YEAR, 1);
                data = ds.Event_GetAllEntrys("0", cal0, cal1);
                break;
            default:
                data = ds.Event_GetAllEntrys("0");
                break;
        }

        // Daten auslesen
        for (Event_DSs.FirstStrTwArTw item: data) {
            alData.add(item.getInt());
        }

        // Adapter setzen & aktualisieren
        int1 = 0; int2 = 0; int3 = 0;
        evAdapter.setData(alData, data);
        eventList.setAdapter(evAdapter);
    }


    /**
     * @see String Ersteller - Tobias Häuser
     * @see String Gibt das jeweilige FooterDrawable Integerwert zurueck
     */
    Integer parseStringToInteger(String sID){
        // Deklaration
        Integer iID;

        // String parsen
        try                     { iID = Integer.parseInt(sID); }
        catch (Exception ex)    { iID = 0; }

        return iID;
    }


    /**
     * @see String Ersteller - Tobias Häuser
     * @see String Hinzufuegen eines neuen Event, nach Buttonclick
     */
    void addEvent(Integer iId){
        // Deklaration
        DS_Event eveHelper;

        // Initialisierung
        Calendar newCalendar                    = Calendar.getInstance();
        final SimpleDateFormat dateFormatter    = new SimpleDateFormat("dd.MM.yyyy", getResources().getConfiguration().locale);
        final View vStart                       = inflater.inflate(R.layout.scv_event_insert, parent, false);

        // GUI Deklaration
        final EditText edTitle      = (EditText) vStart.findViewById(R.id.edTitle);
        final EditText edDate       = (EditText) vStart.findViewById(R.id.edDate);
        final EditText edInfo       = (EditText) vStart.findViewById(R.id.edInfo);
        final EditText edSubject    = (EditText) vStart.findViewById(R.id.edSubject);
        final EditText edTheme      = (EditText) vStart.findViewById(R.id.edTheme);
        final Spinner spDrw         = (Spinner) vStart.findViewById(R.id.spDrw);
        final Spinner spCol         = (Spinner) vStart.findViewById(R.id.spColor);

        // Event auslesen ueber ID falls vorhanden
        if(iId != null && iId >= 0){
            eveHelper = ds.EVENT_getOneEntrie(iId);
            edTitle.setText(eveHelper.getTitle());
            String sDate = eveHelper.getDate().toString().substring(6,8) + "." + eveHelper.getDate().toString().substring(4,6) +
                    "." + eveHelper.getDate().toString().substring(0,4);
            edDate.setText(sDate);
            edInfo.setText(eveHelper.getInfo());
            edSubject.setText(eveHelper.getSubject());
            edTheme.setText(eveHelper.getThema());
        }
        else
            eveHelper = new DS_Event();

        final DS_Event event = eveHelper;

        // Spinner Adapter erstellen und setzen
        SpinnerAdapter spAda    = new SpinnerAdapter(getContext(), constant.drws);
        ColorAdapter   spACol   = new ColorAdapter(getContext(), constant.colors);
        spDrw.setAdapter(spAda);
        spCol.setAdapter(spACol);

        // Spinner SelectionListener setzen
        spDrw.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Disable
                if(String.valueOf(constant.drws[position]).equals(ds.SETTINGS_getOneEntries("EveDra1"))) {
                    event.setColor(parseStringToInteger(ds.SETTINGS_getOneEntries("EveCol1")));
                    spCol.setVisibility(View.GONE);
                }
                else if(String.valueOf(constant.drws[position]).equals(ds.SETTINGS_getOneEntries("EveDra2"))) {
                    event.setColor(parseStringToInteger(ds.SETTINGS_getOneEntries("EveCol2")));
                    spCol.setVisibility(View.GONE);
                }
                else if(String.valueOf(constant.drws[position]).equals(ds.SETTINGS_getOneEntries("EveDra3"))) {
                    event.setColor(parseStringToInteger(ds.SETTINGS_getOneEntries("EveCol3")));
                    spCol.setVisibility(View.GONE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        // Set SelectedItem  Drw Spinner
        for (int i = 0; i < constant.drws.length; i++) {
            if(constant.drws[i] == eveHelper.getDrw()) {
                spDrw.setSelection(i);
                break;
            }
            if(i+1 == constant.drws.length)
                spDrw.setSelection(0);
        }

        // DatePicker formatieren
        final DatePickerDialog fromDatePickerDialog = new DatePickerDialog(getContext(),
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    EditText dateText = (EditText) vStart.findViewById(R.id.edDate);
                    dateText.setText(dateFormatter.format(newDate.getTime()));
                }
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        // EditText Listener & Inputtype setzen
        edDate.setInputType(InputType.TYPE_NULL);
        edDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(v == edDate) {
                    fromDatePickerDialog.show();
                }
                return false;
            }
        });

        // AlertDialog bilden
        AlertDialog.Builder builder = new AlertDialog.Builder(inflater.getContext())
                .setCancelable(false)
                .setView(vStart)
                .setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Dialog schliessen
                        dialog.dismiss();
                        dialog.cancel();

                        // GUI Variablen Dekla
                        String sTitle, sDate, sInfo, sSubject, sThema;
                        Integer i;

                        // GUI Variablen Init
                        sTitle      = edTitle.getText().toString();
                        sDate       = edDate.getText().toString();
                        sInfo       = edInfo.getText().toString();
                        sSubject    = edSubject.getText().toString();
                        sThema      = edTheme.getText().toString();

                        // Daten aus GUI lesen und Event initialisieren
                        event.setChildId(0);
                        if(!sTitle.isEmpty()) event.setTitle(sTitle);
                        if(!sDate.isEmpty() && sDate.length() >= 10)
                            i = Integer.parseInt(sDate.substring(6, 10)) * 10000 + Integer.parseInt(sDate.substring(3, 5)) * 100 +
                                    Integer.parseInt(sDate.substring(0, 2));
                        else
                            i = 10000000;
                        event.setDate(i);
                        event.setDrw(constant.drws[spDrw.getSelectedItemPosition()]);
                        if(spCol.getVisibility() != View.GONE)
                            event.setColor(constant.colors[spCol.getSelectedItemPosition()]);
                        if(!sInfo.isEmpty()) event.setInfo(sTitle);
                        if(!sSubject.isEmpty()) event.setSubject(sTitle);
                        if(!sThema.isEmpty()) event.setThema(sTitle);

                        // Event sppeichern
                        ds.Event_UpdateOrCreateEntry(event);

                        // Fragmentliste aktualisieren
                        update();
                    }
                })
                .setNegativeButton(getString(R.string.Cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                dialog.cancel();
                            }
                        })
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            dialog.dismiss();
                            dialog.cancel();
                        }
                        return true;
                    }
                });

        // Dialog anzeigen
        builder.create().show();
    }


    /**
     * @see String Ersteller - Tobias Häuser
     * @see String Dialog zum Loeschen eines Termins
     */
    void showDelDia(final Integer iId){
        // Dialog erstellen
        AlertDialog.Builder builder = new AlertDialog.Builder(inflater.getContext())
                .setCancelable(false)
                .setTitle(getString(R.string.QDelEve))
                .setPositiveButton(getString(R.string.Delete), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Termin löschen & Übersicht aktualisieren
                        ds.EVENT_deleteEntry(iId);
                        update();
                        Toast.makeText(inflater.getContext(), getString(R.string.DelEve), Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton(getString(R.string.Cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                dialog.cancel();
                            }
                        })
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            dialog.dismiss();
                            dialog.cancel();
                        }
                        return true;
                    }
                });

        // Dialog anzeigen
        builder.create().show();
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @see String  setzen des Hintergrunddrawable nach API Verion
     */
    @SuppressWarnings("deprecation")
    private void setBackground(View view, Drawable dw){
        view.setBackgroundDrawable(dw);
    }
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setBackground16(View view, Drawable dw){
        view.setBackground(dw);
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @see String  Spinner Adpter - fuer die Icons - Hinzufuegen eines Events
     */
    private class SpinnerAdapter extends BaseAdapter {
        // Deklaration
        LayoutInflater mInflater;
        private int[] dataRecieved;

        // Konstruktor
        SpinnerAdapter(Context context, int[] to) {
            dataRecieved    = to;
            mInflater       = LayoutInflater.from(context);
        }


        @Override
        public int getCount() {
            return dataRecieved.length;
        }
        @Override
        public Object getItem(int i) {
            return null;
        }
        @Override
        public long getItemId(int i) {
            return 0;
        }
        @NonNull
        @Override
        public View getView(int position, View row, @NonNull ViewGroup parent) {
            // Deklaration
            rowHolder holder;

            // Initialisierung
            if (row == null) {
                // Initialisierung
                holder      = new rowHolder();
                row         = mInflater.inflate(R.layout.page_imageview, parent, false);

                // Holderitem beziehen
                holder.icon = (ImageView) (row.findViewById(R.id.imageView3));

                // Tag setzen
                row.setTag(holder);
            }
            else
                // Row beziehen
                holder = (rowHolder) row.getTag();

            // Icon setzen
            holder.icon.setImageResource(dataRecieved[position]);

            return row;
        }


        // RowHolder
        class rowHolder{
            ImageView icon;
        }
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @see String  Spinner Adapter - fuer die Farben . Hinzufuegen eines Events
     */
    private class ColorAdapter extends BaseAdapter {
        // Deklaration
        LayoutInflater mInflater;
        private int[] dataRecieved;

        // Konstruktor
        ColorAdapter(Context context, int[] to) {
            dataRecieved    = to;
            mInflater       = LayoutInflater.from(context);
        }


        @Override
        public int getCount() {
            return dataRecieved.length;
        }
        @Override
        public Object getItem(int i) {
            return null;
        }
        @Override
        public long getItemId(int i) {
            return 0;
        }
        @NonNull
        @Override
        public View getView(int position, View row, @NonNull ViewGroup parent) {
            // Deklaration
            rowHolder holder;

            // Initialisierung
            if (row == null) {
                // Initialisierung
                holder              = new rowHolder();
                row                 = mInflater.inflate(R.layout.page_imageview, parent, false);

                // Holderitem beziehen
                holder.icon = (ImageView) (row.findViewById(R.id.imageView3));

                // Tag setzen
                row.setTag(holder);
            }
            else
                // Row beziehen
                holder = (rowHolder) row.getTag();

            // Icon setzen
            holder.icon.setBackgroundColor(dataRecieved[position]);

            return row;
        }


        // RowHolder der oberen ExpList
        class rowHolder{
            ImageView icon;
        }
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @see String  Expandablelist Adapter der Oberenliste
     */
    private class EventExpListAdapter extends BaseExpandableListAdapter {
        // Deklaration
        private Context context;
        private ArrayList<Event_DSs.FirstStrTwArTw> events;
        private LayoutInflater inflater;
        private ArrayList<Integer> expandableListTitle;
        private ExpandableListView TopExList;

        // Konstruktor
        EventExpListAdapter(Context context,
                                ArrayList<Integer> expandableListTitle,
                                ArrayList<Event_DSs.FirstStrTwArTw> events,
                                ExpandableListView TopExList) {
            this.context                    = context;
            this.expandableListTitle        = expandableListTitle;
            this.events                     = events;
            this.inflater                   = LayoutInflater.from(context);
            this.TopExList                  = TopExList;
        }

        void setData(ArrayList<Integer> expandableListTitle,
                     ArrayList<Event_DSs.FirstStrTwArTw> events) {
            this.expandableListTitle    = expandableListTitle;
            this.events                 = events;
        }


        public int getChildrenCount(int groupPosition) {
            return 1;
        }
        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return events.get(groupPosition);
        }
        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }
        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View row, ViewGroup parent) {
            // Deklaration
            rowHolderTopChild holder;
            SecondLevelExpandableListView secondLevelELV;

            // Initialisierung

            if (row == null) {
                // Initialisierung
                secondLevelELV  = new SecondLevelExpandableListView(context);
                holder          = new rowHolderTopChild();

                // Datenbeschaffung
                Event_DSs.FirstStrTwArTw details        = (Event_DSs.FirstStrTwArTw) getChild(groupPosition, childPosition);
                ArrayList<Event_DSs.ThirdTwoString> al  = new ArrayList<>();

                for (Event_DSs.SecondTwoStrArLTwStr item: details.getHashTwAlTw()) {
                    al.add(item.getTwoStr());
                }

                // Adaptersetzen
                secondLevelELV.setAdapter(new EventExpandableListAdapter(context, al, details.getHashTwAlTw()));

                // Einstellungen vornehmen
                secondLevelELV.setGroupIndicator(null);
                secondLevelELV.setDivider(null);
                secondLevelELV.setDividerHeight(0);
                secondLevelELV.setPadding(35,20,35,20);
                row = secondLevelELV;

                // Holderitem beziehen
                holder.item = secondLevelELV;

                // Tag setzen
                row.setTag(holder);

                // Counter hochzählen
                for (Event_DSs.ThirdTwoString item: al) {
                    switch (item.getDrw()){
                        case R.drawable.speaking_black:
                            int1++;
                            break;
                        case R.drawable.pencil_black:
                            int2++;
                            break;
                        case R.drawable.document_black:
                            int3++;
                            break;
                    }
                }
            }
            else {
                // Row beziehen
                holder = (rowHolderTopChild) row.getTag();
                row = holder.item;
            }

            return row;
        }

        @Override
        public int getGroupCount() {
            return expandableListTitle.size();
        }
        @Override
        public Object getGroup(int groupPosition) {
            return expandableListTitle.get(groupPosition);
        }
        @Override
        public long getGroupId(int groupPosition) {
            return (long)( groupPosition*1024 );  // To be consistent with getChildId
        }
        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View row, ViewGroup parent) {
            // Deklaration
            rowHolderTopParent holder;

            // Initialisierung
            if (row == null) {
                // Initialisierung
                holder              = new rowHolderTopParent();
                row                 = inflater.inflate(R.layout.page_event_lv_header, parent, false);
                String gt           = String.valueOf(getGroup( groupPosition ));

                // Holderitem beziehen
                holder.tv = (TextView) row.findViewById(R.id.tvText);
                if( gt != null )
                    holder.str = gt.substring(6, 8) + "." + gt.substring(4, 6) + "." + gt.substring(0, 4);

                // ???
                TopExList.expandGroup(groupPosition);

                // Tag setzen
                row.setTag(holder);
            }
            else
                // Row beziehen
                holder = (rowHolderTopParent) row.getTag();

            // Text setzen
            holder.tv.setText(holder.str);

            return row;
        }


        @Override
        public boolean hasStableIds() {
            return true;
        }
        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }


        // RowHolder der oberen ExpList
        class rowHolderTopChild{
            SecondLevelExpandableListView item;
        }
        class rowHolderTopParent{
            TextView tv;
            String str;
        }


        /**
         * @see String  Ersteller - Tobias Häuser
         * @see String  Expandablelist Adapter der Unterliste
         */
        private class EventExpandableListAdapter extends BaseExpandableListAdapter {
            private Context context;
            private ArrayList<Event_DSs.ThirdTwoString> expandableListTitle;
            private ArrayList<Event_DSs.SecondTwoStrArLTwStr> expandableListDetail;
            private LayoutInflater inflater;

            EventExpandableListAdapter(Context context, ArrayList<Event_DSs.ThirdTwoString> expandableListTitle,
                                       ArrayList<Event_DSs.SecondTwoStrArLTwStr> expandableListDetail) {
                this.context = context;
                this.expandableListTitle    = expandableListTitle;
                this.expandableListDetail   = expandableListDetail;
                this.inflater               = LayoutInflater.from(context);
            }


            @Override
            public int getChildrenCount(int listPosition) {
                return this.expandableListDetail.get(listPosition).getAlTwoStr().size();
            }
            @Override
            public Object getChild(int groupPosition, int childPosition) {
                return expandableListDetail.get(groupPosition).getAlTwoStr().get(childPosition);
            }
            @Override
            public long getChildId(int listPosition, int expandedListPosition) {
                return expandedListPosition;
            }
            @Override
            public View getChildView(int groupPosition, final int childPosition,
                                     boolean isLastChild, View row, ViewGroup parent) {
                // Deklaration & Initialisierung
                rowHolderSubChildFooter     holderF;
                rowHolderSubChildBody       holderB     = null;
                Drawable                    dw          = null;

                // Initialisierung
                if (row == null) {
                    if (!isLastChild) {
                        // Deklaration & Initialisierung
                        row                 = this.inflater.inflate(R.layout.page_event_exp_body, parent, false);
                        holderB             = new rowHolderSubChildBody();
                        holderB.tv1         = (TextView) row.findViewById(R.id.tvHead);
                        holderB.tv2         = (TextView) row.findViewById(R.id.tvContent);
                        holderB.expListPos  = (Event_DSs.ThirdTwoString) getChild(groupPosition, childPosition);
                        holderB.dw          = ContextCompat.getDrawable(getActivity(), R.drawable.event_exp_body);

                        // Tag setzen
                        row.setTag(holderB);
                    }
                    else {
                        // Deklaration & Initialisierung
                        final Integer iID   = expandableListTitle.get(groupPosition).getId();
                        row                 = this.inflater.inflate(R.layout.page_event_exp_fooder, parent, false);
                        holderF             = new rowHolderSubChildFooter();
                        holderF.bt1         = (Button) row.findViewById(R.id.btnDelete);
                        holderF.bt2         = (Button) row.findViewById(R.id.btnEdit);
                        holderF.dw          = ContextCompat.getDrawable(getActivity(), R.drawable.event_exp_footer);

                        holderF.bt1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showDelDia(iID);
                            }
                        });
                        holderF.bt2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                addEvent(iID);
                            }
                        });

                        // Tag setzen
                        row.setTag(holderF);
                    }
                }
                else if(!isLastChild) {
                    if(row.getTag() instanceof rowHolderSubChildFooter) {
                        // Deklaration & Initialisierung
                        row                 = this.inflater.inflate(R.layout.page_event_exp_body, parent, false);
                        holderB             = new rowHolderSubChildBody();
                        holderB.tv1         = (TextView) row.findViewById(R.id.tvHead);
                        holderB.tv2         = (TextView) row.findViewById(R.id.tvContent);
                        holderB.expListPos  = (Event_DSs.ThirdTwoString) getChild(groupPosition, childPosition);
                        holderB.dw          = ContextCompat.getDrawable(getActivity(), R.drawable.event_exp_body);

                        // Tag setzen
                        row.setTag(holderB);
                    }

                    // Row beziehen
                    holderB = (rowHolderSubChildBody) row.getTag();
                    dw      = holderB.dw;
                }
                else {
                    if(row.getTag() instanceof rowHolderSubChildBody) {
                        // Deklaration & Initialisierung
                        final Integer iID   = expandableListTitle.get(groupPosition).getId();
                        row                 = this.inflater.inflate(R.layout.page_event_exp_fooder, parent, false);
                        holderF             = new rowHolderSubChildFooter();
                        holderF.bt1         = (Button) row.findViewById(R.id.btnEdit);
                        holderF.bt2         = (Button) row.findViewById(R.id.btnDelete);
                        holderF.dw          = ContextCompat.getDrawable(getActivity(), R.drawable.event_exp_footer);

                        holderF.bt1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                addEvent(iID);
                            }
                        });
                        holderF.bt2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showDelDia(iID);
                            }
                        });

                        // Tag setzen
                        row.setTag(holderF);
                    }

                    // Row beziehen
                    holderF = (rowHolderSubChildFooter) row.getTag();
                    dw      = holderF.dw;
                }

                // Text setzen
                if(!isLastChild) {
                    holderB.tv1.setText(holderB.expListPos.get_str1());
                    holderB.tv2.setText(holderB.expListPos.get_str2());
                }

                // BackgroundDrawable setzen
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                    setBackground(row, dw);
                else
                    setBackground16(row, dw);

                return row;
            }


            @Override
            public int getGroupCount() {
                return this.expandableListTitle.size();
            }
            @Override
            public Object getGroup(int listPosition) {
                return this.expandableListTitle.get(listPosition);
            }
            @Override
            public long getGroupId(int listPosition) {
                return listPosition;
            }
            @Override
            public View getGroupView(int listPosition, boolean isExpanded,
                                     View row, ViewGroup parent) {
                // Deklaration & Initialisierung
                rowHolderSubParent          holder;

                // Initialisierung
                if (row == null) {
                    // Deklaration & Initialisierung
                    row                 = this.inflater.inflate(R.layout.page_event_exp_header, parent, false);
                    holder              = new rowHolderSubParent();
                    holder.tv1          = (TextView) row.findViewById(R.id.tvTitle);
                    holder.tv2          = (TextView) row.findViewById(R.id.tvDate);
                    holder.ind          = row.findViewById(R.id.ivHeader);
                    holder.dw           = ContextCompat.getDrawable(getActivity(), R.drawable.event_exp_head);
                    holder.twoString    = (Event_DSs.ThirdTwoString) getGroup(listPosition);

                    holder.tv1.setTypeface(null, Typeface.BOLD);
                    holder.tv2.setTypeface(null, Typeface.BOLD);
                    holder.dw.mutate().setColorFilter(holder.twoString.getColor(), PorterDuff.Mode.MULTIPLY);

                    // Tag setzen
                    row.setTag(holder);

                    if(listPosition >= getGroupCount()-1) {
                        tv1.setText(String.valueOf(int1));
                        tv2.setText(String.valueOf(int2));
                        tv3.setText(String.valueOf(int3));
                    }
                }
                else
                    // Row beziehen
                    holder = (rowHolderSubParent) row.getTag();


                // Text setzen
                holder.tv1.setText(holder.twoString.get_str1());
                holder.tv2.setText(holder.twoString.get_str2());

                // Image setzen
                if (holder.ind != null)
                {
                    ImageView indicator = (ImageView) holder.ind;
                    indicator.setImageDrawable(ContextCompat.getDrawable(context, holder.twoString.getDrw()));
                }

                // BackgroundDrawable setzen
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                    setBackground(row, holder.dw);
                else
                    setBackground16(row, holder.dw);

                return row;
            }


            @Override
            public boolean hasStableIds() {
                return false;
            }
            @Override
            public boolean isChildSelectable(int listPosition, int expandedListPosition) {
                return true;
            }

            // RowHolder der oberen ExpList
            class rowHolderSubChildBody{
                TextView tv1;
                TextView tv2;
                Event_DSs.ThirdTwoString expListPos;
                Drawable dw;
            }
            class rowHolderSubChildFooter{
                Button bt1;
                Button bt2;
                Drawable dw;
            }
            class rowHolderSubParent{
                Event_DSs.ThirdTwoString twoString;
                TextView tv1;
                TextView tv2;
                View ind;
                Drawable dw;
            }
        }
    }


    /**
     * @author Tobias Häuser
     * @see String ListView der SubList
     */
    public class SecondLevelExpandableListView extends ExpandableListView
    {
        // Konstruktor
        public SecondLevelExpandableListView(Context context) {
            super(context);
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            // Layoutheigth maximal seltzen fuer dessen Kalkulation
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(999999, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}