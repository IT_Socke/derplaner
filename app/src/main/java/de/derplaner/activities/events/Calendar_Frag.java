package de.derplaner.activities.events;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.derplaner.another.constant;
import de.derplaner.dataObjects.DS_Event;
import de.derplaner.R;
import de.derplaner.SQLite.SQL_PlanerDataSource;
import de.derplaner.dataObjects.Event_DSs;

import static de.derplaner.another.constant.drws;

public class Calendar_Frag extends Fragment {
    // Globale Variablen
    private LayoutInflater          inflater;
    private SQL_PlanerDataSource    ds;
    private CalenderAdapter         evAdapter;
    private ExpandableListView      eventList;
    private Calendar_View           cv;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState){
        super.onCreateView(inflater, container, savedInstanceState);

        // Deklaration
        View rootView               = inflater.inflate(R.layout.act_event_calender, container, false);
        ArrayList<Event_DSs.FirstStrTwArTw> data;
        ArrayList<Event_DSs.ThirdTwoString> al;

        // Initialisierung
        this.inflater   = inflater;
        cv              = (Calendar_View) rootView.findViewById(R.id.calendar_view);
        ds              = new SQL_PlanerDataSource(getActivity());
        eventList       = (ExpandableListView) rootView.findViewById(R.id.elvMain);
        data            = ds.Event_GetAllEntrys("0", Calendar.getInstance(), Calendar.getInstance());
        al              = new ArrayList<>();

        // Calendar_View initialisieren
        cv.init(ds);

        // EventHandler fuer OnDayClick
        cv.setEventHandler(new Calendar_View.EventHandler() {
            @Override
            public void onDayPress(Date date) {
                // Deklaration & Init
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);

                // Daten updaten
                update(cal);
            }
        });

        // Adapter erstellen
        if(data.size() >= 1)
        {
            // Titel herrausfiltern
            for (Event_DSs.SecondTwoStrArLTwStr item2: data.get(0).getHashTwAlTw()) {
                al.add(item2.getTwoStr());
            }

            // Daten setzen
            evAdapter = new CalenderAdapter(getActivity(), al, data.get(0).getHashTwAlTw());
        }
        else
            evAdapter = new CalenderAdapter(getActivity(), new ArrayList<Event_DSs.ThirdTwoString>(),
                    new ArrayList<Event_DSs.SecondTwoStrArLTwStr>());

        // Adapter setzen
        eventList.setAdapter(evAdapter);

        return rootView;
    }


    /*// Override des Speicherns von savedInstanceState
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        // AlertDialog Inhalt speichern
        if (bAlert) {
            savedInstanceState.putString("Run", "1");
            savedInstanceState.putString("ETInfo", txtInfo.getText().toString());
            savedInstanceState.putString("ETHour", txtHour.getText().toString());
            savedInstanceState.putString("ETThema", txtThema.getText().toString());
            savedInstanceState.putString("ETNotiz", txtNotiz.getText().toString());
        }

    }


    // Override des Wiederherstellens von savedInstanceState
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // AlertDialog Inhalt wiederherstellen
        if(savedInstanceState != null) {
            if (savedInstanceState.getString("Run", "0").equals("1")) {
                txtInfo.setText(savedInstanceState.getString("ETInfo", ""));
                txtHour.setText(savedInstanceState.getString("ETHour", ""));
                txtThema.setText(savedInstanceState.getString("ETThema", ""));
                txtNotiz.setText(savedInstanceState.getString("ETNotiz", ""));
            }
        }
    }*/


    /**
     * @see String Ersteller - Tobias Häuser
     * @see String Daten neu auslesen und den Adapter der Fragmentlste aktualisieren
     */
    public void update(){
        update(Calendar.getInstance());
        cv.updateCalendar(ds);
    }


    /**
     * @see String Ersteller - Tobias Häuser
     * @see String Daten neu auslesen und den Adapter der Fragmentlste aktualisieren
     */
    void update(Calendar cal){
        // Deklaration
        ArrayList<Event_DSs.ThirdTwoString> al      = new ArrayList<>();
        ArrayList<Event_DSs.FirstStrTwArTw> data;

        // Daten auslesen
        if(cal != null) {
            data = ds.Event_GetAllEntrys("0", cal, cal);

            if(data.size() >= 1)
            {
                // Titel herrausfiltern
                for (Event_DSs.SecondTwoStrArLTwStr item2: data.get(0).getHashTwAlTw()) {
                    al.add(item2.getTwoStr());
                }

                // Daten setzen
                evAdapter.setData(al, data.get(0).getHashTwAlTw());
            }
            else
                evAdapter = new CalenderAdapter(getActivity(), new ArrayList<Event_DSs.ThirdTwoString>(),
                        new ArrayList<Event_DSs.SecondTwoStrArLTwStr>());
        }

        // Adapter setzen & aktualisieren
        eventList.setAdapter(evAdapter);
    }


    /**
     * @see String Ersteller - Tobias Häuser
     * @see String Gibt das jeweilige FooterDrawable Integerwert zurueck
     */
    Integer parseStringToInteger(String sID){
        // Deklaration
        Integer iID;

        // String parsen
        try                     { iID = Integer.parseInt(sID); }
        catch (Exception ex)    { iID = 0; }

        return iID;
    }


    /**
     * @see String Ersteller - Tobias Häuser
     * @see String Hinzufuegen eines neuen Event, nach Buttonclick
     */
    void addEvent(Integer iId){
        // Deklaration
        DS_Event eveHelper;

        // Initialisierung
        Calendar newCalendar                    = Calendar.getInstance();
        final SimpleDateFormat dateFormatter    = new SimpleDateFormat("dd.MM.yyyy", getResources().getConfiguration().locale);
        @SuppressLint("InflateParams")
        final View vStart                       = inflater.inflate(R.layout.scv_event_insert, null);

        // GUI Deklaration
        final EditText edTitle      = (EditText) vStart.findViewById(R.id.edTitle);
        final EditText edDate       = (EditText) vStart.findViewById(R.id.edDate);
        final EditText edInfo       = (EditText) vStart.findViewById(R.id.edInfo);
        final EditText edSubject    = (EditText) vStart.findViewById(R.id.edSubject);
        final EditText edTheme      = (EditText) vStart.findViewById(R.id.edTheme);
        final Spinner spDrw         = (Spinner) vStart.findViewById(R.id.spDrw);
        final Spinner spCol         = (Spinner) vStart.findViewById(R.id.spColor);

        // Event auslesen ueber ID falls vorhanden
        if(iId != null && iId >= 0){
            eveHelper = ds.EVENT_getOneEntrie(iId);
            edTitle.setText(eveHelper.getTitle());
            String sDate = eveHelper.getDate().toString().substring(6,8) + "." + eveHelper.getDate().toString().substring(4,6) +
                    "." + eveHelper.getDate().toString().substring(0,4);
            edDate.setText(sDate);
            edInfo.setText(eveHelper.getInfo());
            edSubject.setText(eveHelper.getSubject());
            edTheme.setText(eveHelper.getThema());
        }
        else
            eveHelper = new DS_Event();

        final DS_Event event = eveHelper;

        // Spinner Adapter erstellen und setzen
        SpinnerAdapter spAda    = new SpinnerAdapter(getContext(), constant.drws);
        ColorAdapter spACol   = new ColorAdapter(getContext(), constant.colors);
        spDrw.setAdapter(spAda);
        spCol.setAdapter(spACol);

        // Spinner SelectionListener setzen
        spDrw.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Disable
                if(String.valueOf(drws[position]).equals(ds.SETTINGS_getOneEntries("EveDra1"))) {
                    event.setColor(parseStringToInteger(ds.SETTINGS_getOneEntries("EveCol1")));
                    spCol.setVisibility(View.GONE);
                }
                else if(String.valueOf(drws[position]).equals(ds.SETTINGS_getOneEntries("EveDra2"))) {
                    event.setColor(parseStringToInteger(ds.SETTINGS_getOneEntries("EveCol2")));
                    spCol.setVisibility(View.GONE);
                }
                else if(String.valueOf(drws[position]).equals(ds.SETTINGS_getOneEntries("EveDra3"))) {
                    event.setColor(parseStringToInteger(ds.SETTINGS_getOneEntries("EveCol3")));
                    spCol.setVisibility(View.GONE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        // Set SelectedItem  Drw Spinner
        for (int i = 0; i < drws.length; i++) {
            if(drws[i] == eveHelper.getDrw()) {
                spDrw.setSelection(i);
                break;
            }
            if(i+1 == drws.length)
                spDrw.setSelection(0);
        }

        // DatePicker formatieren
        final DatePickerDialog fromDatePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        EditText dateText = (EditText) vStart.findViewById(R.id.edDate);
                        dateText.setText(dateFormatter.format(newDate.getTime()));
                    }
                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        // EditText Listener & Inputtype setzen
        edDate.setInputType(InputType.TYPE_NULL);
        edDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(v == edDate) {
                    fromDatePickerDialog.show();
                }
                return false;
            }
        });

        // AlertDialog bilden
        AlertDialog.Builder builder = new AlertDialog.Builder(inflater.getContext())
                .setCancelable(false)
                .setView(vStart)
                .setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Dialog schliessen
                        dialog.dismiss();
                        dialog.cancel();

                        // GUI Variablen Dekla
                        String sTitle, sDate, sInfo, sSubject, sThema;
                        Integer i;

                        // GUI Variablen Init
                        sTitle      = edTitle.getText().toString();
                        sDate       = edDate.getText().toString();
                        sInfo       = edInfo.getText().toString();
                        sSubject    = edSubject.getText().toString();
                        sThema      = edTheme.getText().toString();

                        // Daten aus GUI lesen und Event initialisieren
                        event.setChildId(0);
                        if(!sTitle.isEmpty()) event.setTitle(sTitle);
                        if(!sDate.isEmpty() && sDate.length() >= 10)
                            i = Integer.parseInt(sDate.substring(6, 10)) * 10000 + Integer.parseInt(sDate.substring(3, 5)) * 100 +
                                    Integer.parseInt(sDate.substring(0, 2));
                        else
                            i = 10000000;
                        event.setDate(i);
                        event.setDrw(drws[spDrw.getSelectedItemPosition()]);
                        if(spCol.getVisibility() != View.GONE)
                            event.setColor(constant.colors[spCol.getSelectedItemPosition()]);
                        if(!sInfo.isEmpty()) event.setInfo(sTitle);
                        if(!sSubject.isEmpty()) event.setSubject(sTitle);
                        if(!sThema.isEmpty()) event.setThema(sTitle);

                        // Event sppeichern
                        ds.Event_UpdateOrCreateEntry(event);

                        // Fragmentliste aktualisieren
                        update(null);
                    }
                })
                .setNegativeButton(getString(R.string.Cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                dialog.cancel();
                            }
                        })
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            dialog.dismiss();
                            dialog.cancel();
                        }
                        return true;
                    }
                });

        // Dialog anzeigen
        builder.create().show();
    }


    /**
     * @see String Ersteller - Tobias Häuser
     * @see String Dialog zum Loeschen eines Termins
     */
    void showDelDia(final Integer iId){
        // Dialog erstellen
        AlertDialog.Builder builder = new AlertDialog.Builder(inflater.getContext())
                .setCancelable(false)
                .setTitle(getString(R.string.QDelEve))
                .setPositiveButton(getString(R.string.Delete), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Termin löschen & Übersicht aktualisieren
                        ds.EVENT_deleteEntry(iId);
                        update(null);
                        Toast.makeText(inflater.getContext(), getString(R.string.DelEve), Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton(getString(R.string.Cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                dialog.cancel();
                            }
                        })
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            dialog.dismiss();
                            dialog.cancel();
                        }
                        return true;
                    }
                });

        // Dialog anzeigen
        builder.create().show();
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @see String  setzen des Hintergrunddrawable nach API Verion
     */
    @SuppressWarnings("deprecation")
    private void setBackground(View view, Drawable dw){
        view.setBackgroundDrawable(dw);
    }
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setBackground16(View view, Drawable dw){
        view.setBackground(dw);
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @see String  Spinner Adpter - fuer die Icons - Hinzufuegen eines Events
     */
    private class SpinnerAdapter extends BaseAdapter {
        // Deklaration
        LayoutInflater mInflater;
        private int[] dataRecieved;

        // Konstruktor
        SpinnerAdapter(Context context, int[] to) {
            dataRecieved    = to;
            mInflater       = LayoutInflater.from(context);
        }


        @Override
        public int getCount() {
            return dataRecieved.length;
        }
        @Override
        public Object getItem(int i) {
            return null;
        }
        @Override
        public long getItemId(int i) {
            return 0;
        }
        @NonNull
        @Override
        public View getView(int position, View row, @NonNull ViewGroup parent) {
            // Deklaration
            rowHolder holder;

            // Initialisierung
            if (row == null) {
                // Initialisierung
                holder      = new rowHolder();
                row         = mInflater.inflate(R.layout.page_imageview, parent, false);

                // Holderitem beziehen
                holder.icon = (ImageView) (row.findViewById(R.id.imageView3));

                // Tag setzen
                row.setTag(holder);
            }
            else
                // Row beziehen
                holder = (rowHolder) row.getTag();

            // Icon setzen
            holder.icon.setImageResource(dataRecieved[position]);

            return row;
        }


        // RowHolder
        class rowHolder{
            ImageView icon;
        }
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @see String  Spinner Adapter - fuer die Farben . Hinzufuegen eines Events
     */
    private class ColorAdapter extends BaseAdapter {
        // Deklaration
        LayoutInflater mInflater;
        private int[] dataRecieved;

        // Konstruktor
        ColorAdapter(Context context, int[] to) {
            dataRecieved    = to;
            mInflater       = LayoutInflater.from(context);
        }


        @Override
        public int getCount() {
            return dataRecieved.length;
        }
        @Override
        public Object getItem(int i) {
            return null;
        }
        @Override
        public long getItemId(int i) {
            return 0;
        }
        @NonNull
        @Override
        public View getView(int position, View row, @NonNull ViewGroup parent) {
            // Deklaration
            rowHolder holder;

            // Initialisierung
            if (row == null) {
                // Initialisierung
                holder              = new rowHolder();
                row                 = mInflater.inflate(R.layout.page_imageview, parent, false);

                // Holderitem beziehen
                holder.icon = (ImageView) (row.findViewById(R.id.imageView3));

                // Tag setzen
                row.setTag(holder);
            }
            else
                // Row beziehen
                holder = (rowHolder) row.getTag();

            // Icon setzen
            holder.icon.setBackgroundColor(dataRecieved[position]);

            return row;
        }


        // RowHolder der oberen ExpList
        class rowHolder{
            ImageView icon;
        }
    }


    /**
     * @author Tobias Häuser
     * @see String  Expandablelist Adapter der Unterliste
     */
    private class CalenderAdapter extends BaseExpandableListAdapter {
        private Context context;
        private ArrayList<Event_DSs.ThirdTwoString> expandableListTitle;
        private ArrayList<Event_DSs.SecondTwoStrArLTwStr> expandableListDetail;
        private LayoutInflater inflater;

        CalenderAdapter(Context context, ArrayList<Event_DSs.ThirdTwoString> expandableListTitle,
                                   ArrayList<Event_DSs.SecondTwoStrArLTwStr> expandableListDetail) {
            this.context = context;
            this.expandableListTitle    = expandableListTitle;
            this.expandableListDetail   = expandableListDetail;
            this.inflater               = LayoutInflater.from(context);
        }

        void setData(ArrayList<Event_DSs.ThirdTwoString> expandableListTitle,
                     ArrayList<Event_DSs.SecondTwoStrArLTwStr> expandableListDetail) {
            this.expandableListTitle    = expandableListTitle;
            this.expandableListDetail   = expandableListDetail;
        }


        @Override
        public int getChildrenCount(int listPosition) {
            return this.expandableListDetail.get(listPosition).getAlTwoStr().size();
        }
        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return expandableListDetail.get(groupPosition).getAlTwoStr().get(childPosition);
        }
        @Override
        public long getChildId(int listPosition, int expandedListPosition) {
            return expandedListPosition;
        }
        @Override
        public View getChildView(int groupPosition, final int childPosition,
                                 boolean isLastChild, View row, ViewGroup parent) {
            // Deklaration & Initialisierung
            rowHolderSubChildFooter holderF;
            rowHolderSubChildBody holderB     = null;
            Drawable dw          = null;

            // Initialisierung
            if (row == null) {
                if (!isLastChild) {
                    // Deklaration & Initialisierung
                    row                 = this.inflater.inflate(R.layout.page_event_exp_body, parent, false);
                    holderB             = new rowHolderSubChildBody();
                    holderB.tv1         = (TextView) row.findViewById(R.id.tvHead);
                    holderB.tv2         = (TextView) row.findViewById(R.id.tvContent);
                    holderB.expListPos  = (Event_DSs.ThirdTwoString) getChild(groupPosition, childPosition);
                    holderB.dw          = ContextCompat.getDrawable(getActivity(), R.drawable.event_exp_body);

                    // Tag setzen
                    row.setTag(holderB);
                }
                else {
                    // Deklaration & Initialisierung
                    final Integer iID   = expandableListTitle.get(groupPosition).getId();
                    row                 = this.inflater.inflate(R.layout.page_event_exp_fooder, parent, false);
                    holderF             = new rowHolderSubChildFooter();
                    holderF.bt1         = (Button) row.findViewById(R.id.btnDelete);
                    holderF.bt2         = (Button) row.findViewById(R.id.btnEdit);
                    holderF.dw          = ContextCompat.getDrawable(getActivity(), R.drawable.event_exp_footer);

                    holderF.bt1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showDelDia(iID);
                        }
                    });
                    holderF.bt2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            addEvent(iID);
                        }
                    });

                    // Tag setzen
                    row.setTag(holderF);
                }
            }
            else if(!isLastChild) {
                if(row.getTag() instanceof rowHolderSubChildFooter) {
                    // Deklaration & Initialisierung
                    row                 = this.inflater.inflate(R.layout.page_event_exp_body, parent, false);
                    holderB             = new rowHolderSubChildBody();
                    holderB.tv1         = (TextView) row.findViewById(R.id.tvHead);
                    holderB.tv2         = (TextView) row.findViewById(R.id.tvContent);
                    holderB.expListPos  = (Event_DSs.ThirdTwoString) getChild(groupPosition, childPosition);
                    holderB.dw          = ContextCompat.getDrawable(getActivity(), R.drawable.event_exp_body);

                    // Tag setzen
                    row.setTag(holderB);
                }

                // Row beziehen
                holderB = (rowHolderSubChildBody) row.getTag();
                dw      = holderB.dw;
            }
            else {
                if(row.getTag() instanceof rowHolderSubChildBody) {
                    // Deklaration & Initialisierung
                    final Integer iID   = expandableListTitle.get(groupPosition).getId();
                    row                 = this.inflater.inflate(R.layout.page_event_exp_fooder, parent, false);
                    holderF             = new rowHolderSubChildFooter();
                    holderF.bt1         = (Button) row.findViewById(R.id.btnEdit);
                    holderF.bt2         = (Button) row.findViewById(R.id.btnDelete);
                    holderF.dw          = ContextCompat.getDrawable(getActivity(), R.drawable.event_exp_footer);

                    holderF.bt1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            addEvent(iID);
                        }
                    });
                    holderF.bt2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showDelDia(iID);
                        }
                    });

                    // Tag setzen
                    row.setTag(holderF);
                }

                // Row beziehen
                holderF = (rowHolderSubChildFooter) row.getTag();
                dw      = holderF.dw;
            }

            // Text setzen
            if(!isLastChild) {
                holderB.tv1.setText(holderB.expListPos.get_str1());
                holderB.tv2.setText(holderB.expListPos.get_str2());
            }

            // BackgroundDrawable setzen
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                setBackground(row, dw);
            else
                setBackground16(row, dw);

            return row;
        }


        @Override
        public int getGroupCount() {
            return this.expandableListTitle.size();
        }
        @Override
        public Object getGroup(int listPosition) {
            return this.expandableListTitle.get(listPosition);
        }
        @Override
        public long getGroupId(int listPosition) {
            return listPosition;
        }
        @Override
        public View getGroupView(int listPosition, boolean isExpanded,
                                 View row, ViewGroup parent) {
            // Deklaration & Initialisierung
            rowHolderSubParent holder;

            // Initialisierung
            if (row == null) {
                // Deklaration & Initialisierung
                row                 = this.inflater.inflate(R.layout.page_event_exp_header, parent, false);
                holder              = new rowHolderSubParent();
                holder.tv1          = (TextView) row.findViewById(R.id.tvTitle);
                holder.tv2          = (TextView) row.findViewById(R.id.tvDate);
                holder.ind          = row.findViewById(R.id.ivHeader);
                holder.dw           = ContextCompat.getDrawable(getActivity(), R.drawable.event_exp_head);
                holder.twoString    = (Event_DSs.ThirdTwoString) getGroup(listPosition);

                holder.tv1.setTypeface(null, Typeface.BOLD);
                holder.tv2.setTypeface(null, Typeface.BOLD);
                holder.dw.mutate().setColorFilter(holder.twoString.getColor(), PorterDuff.Mode.MULTIPLY);

                // Tag setzen
                row.setTag(holder);
            }
            else
                // Row beziehen
                holder = (rowHolderSubParent) row.getTag();


            // Text setzen
            holder.tv1.setText(holder.twoString.get_str1());
            holder.tv2.setText(holder.twoString.get_str2());

            // Image setzen
            if (holder.ind != null)
            {
                ImageView indicator = (ImageView) holder.ind;
                indicator.setImageDrawable(ContextCompat.getDrawable(context, holder.twoString.getDrw()));
            }

            // BackgroundDrawable setzen
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                setBackground(row, holder.dw);
            else
                setBackground16(row, holder.dw);

            return row;
        }


        @Override
        public boolean hasStableIds() {
            return false;
        }
        @Override
        public boolean isChildSelectable(int listPosition, int expandedListPosition) {
            return true;
        }

        // RowHolder der oberen ExpList
        class rowHolderSubChildBody{
            TextView tv1;
            TextView tv2;
            Event_DSs.ThirdTwoString expListPos;
            Drawable dw;
        }
        class rowHolderSubChildFooter{
            Button bt1;
            Button bt2;
            Drawable dw;
        }
        class rowHolderSubParent{
            Event_DSs.ThirdTwoString twoString;
            TextView tv1;
            TextView tv2;
            View ind;
            Drawable dw;
        }
    }
}
