package de.derplaner.activities.events;

import androidx.annotation.NonNull;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.derplaner.R;
import de.derplaner.SQLite.SQL_PlanerDataSource;
import de.derplaner.dataObjects.Event_DSs;

public class Calendar_View extends LinearLayout {
    // Constanten
    private static final int DAYS_COUNT = 42;
    private static final String DATE_FORMAT = "MMMM yyyy";

    // Eventhandling
    private EventHandler eventHandler = null;

    // Control Deklaration
    private LinearLayout header;
    private ImageView btnPrev;
    private ImageView btnNext;
    private TextView txtDate;
    private GridView grid;

    // Konstruktoren
    public Calendar_View(Context context)
    {
        super(context);
    }
    public Calendar_View(Context context, AttributeSet attrs) {
        super(context, attrs);
        initGUI(context);
    }
    public Calendar_View(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initGUI(context);
    }


    /**
     * @see String  Ersteller           - Tobias Häuser
     * @see String  Calendar Adapter    - Initialisierung der GUI
     */
    void initGUI(Context context) {
        // Deklaration & Init
        LayoutInflater inflater     = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.calendar, this);

        // Initialisierung das Controls
        header      = (LinearLayout)findViewById(R.id.calendar_header);
        btnPrev     = (ImageView)findViewById(R.id.calendar_prev_button);
        btnNext     = (ImageView)findViewById(R.id.calendar_next_button);
        txtDate     = (TextView)findViewById(R.id.calendar_date_display);
        grid        = (GridView)findViewById(R.id.calendar_grid);
    }


    /**
     * @see String  Ersteller           - Tobias Häuser
     * @see String  Calendar Adapter    - Initialisierung des DB & Calendars
     */
    public void init(SQL_PlanerDataSource ds) {
        // Controlevents setzten
        GUIClickHandlers(Calendar.getInstance(), ds);

        // Calendar init
        updateCalendar(Calendar.getInstance(), ds);
    }


    /**
     * @see String  Ersteller           - Tobias Häuser
     * @see String  Calendar Adapter    - Erstellen der EventHandler der GUI
     */
    void GUIClickHandlers(final Calendar currentDate, final SQL_PlanerDataSource ds) {
        // Monatswechsel
        btnPrev.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                currentDate.add(Calendar.MONTH, -1);
                updateCalendar(currentDate, ds);
            }
        });
        btnNext.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                currentDate.add(Calendar.MONTH, 1);
                updateCalendar(currentDate, ds);
            }
        });

        // Gridday Cleck
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(eventHandler == null)
                    return;

                // Datum beziehen &uebergeben
                eventHandler.onDayPress((Date) parent.getItemAtPosition(position));
            }
        });
    }


    /**
     * @see String  Ersteller           - Tobias Häuser
     * @see String  Calendar Adapter    - Aktualisieren des Kalenders
     */
    public void updateCalendar(SQL_PlanerDataSource ds){
        updateCalendar(Calendar.getInstance(), ds);
    }


    /**
     * @see String  Ersteller           - Tobias Häuser
     * @see String  Calendar Adapter    - Aktualisieren des Kalenders
     */
    void updateCalendar(final Calendar currentDate, SQL_PlanerDataSource ds) {
        // Deklaration
        ArrayList<Date> cells   = new ArrayList<>();
        Calendar calendar       = (Calendar)currentDate.clone();
        SimpleDateFormat sdf    = new SimpleDateFormat(DATE_FORMAT, getResources().getConfiguration().locale);
        int monthBeginningCell;

        // Initialisierung des Calandars
        calendar.set(Calendar.DAY_OF_MONTH, 0);
        monthBeginningCell = calendar.get(Calendar.DAY_OF_WEEK) - 2;
        calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);

        // Zellen füllen
        while (cells.size() < DAYS_COUNT) {
            cells.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        // Monatsansicht Header setzen
        txtDate.setText(sdf.format(currentDate.getTime()));
        header.setBackgroundColor(Color.parseColor("#DCDCDC"));

        // Adapter setzen
        grid.setAdapter(new CalendarAdapter(getContext(), cells, currentDate, ds));
    }


    /**
     * @author      Ersteller           - Tobias Häuser
     * @see String  Calendar Adapter    - fuer die angezeigten Tage
     */
    private class CalendarAdapter extends ArrayAdapter<Date> {
        // Deklaration
        private ArrayList<Date> alDays;
        private Calendar curDate;
        private LayoutInflater inflater;
        List<String> event = new ArrayList<>();
        SQL_PlanerDataSource    ds;

        // Konstruktor
        CalendarAdapter(Context context, ArrayList<Date> alDays, Calendar curDate, SQL_PlanerDataSource ds) {
            super(context, R.layout.calendar_day_page, alDays);
            this.alDays     = alDays;
            this.curDate    = curDate;
            this.ds         = ds;
            inflater        = LayoutInflater.from(context);

            // Tage auslesen an denen Eintraege sind
            ArrayList<Event_DSs.FirstStrTwArTw> data = (new SQL_PlanerDataSource(getContext())).Event_GetAllEntrys("0");
            for (Event_DSs.FirstStrTwArTw item: data) {
                event.add(String.valueOf(item.getInt()));
            }
        }


        @Override
        @NonNull
        public View getView(int position, View row, @NonNull ViewGroup parent) {
            // Dekklaration
            Date date;
            Calendar calDate    = Calendar.getInstance();
            Calendar calToday   = Calendar.getInstance();
            int acDay, acMonth, acYear;
            rowHolder holder;

            // Initialisierung
            date            = alDays.get(position);
            calDate.setTime(date);
            acDay           = calDate.get(Calendar.DAY_OF_MONTH);
            acMonth         = calDate.get(Calendar.MONTH);
            acYear          = calDate.get(Calendar.YEAR);

            // Initialisierung der Row
            if (row == null) {
                // Initialisierung
                holder      = new rowHolder();
                row         = inflater.inflate(R.layout.calendar_day_page, parent, false);

                // Holderitems setzen
                holder.tv       = (TextView) row;
                holder.text     = (String.valueOf(calDate.get(Calendar.DAY_OF_MONTH)));
                holder.tv.setTypeface(null, Typeface.NORMAL);
                holder.tv.setTextColor(Color.BLACK);

                // Auslesen ob es Termine an dem Tag gibt
                if(ds != null) {
                    int iCount = ds.Event_GetCountEntrysOfDay("0", String.valueOf(acYear * 10000 + (acMonth + 1) * 100 + acDay));
                    if (iCount >= 1) {
                        holder.tv.setTextColor(Color.parseColor("#FF4000"));
                    }
                }

                // Formatierung des TVs
                if (acMonth != curDate.get(Calendar.MONTH) || acYear != curDate.get(Calendar.YEAR)) {
                    // Zeigt Tage anderer Monate grau an
                    holder.tv.setTextColor(Color.parseColor("#C7C7C7"));
                } else if (acDay == calToday.get(Calendar.DAY_OF_MONTH) && acMonth == calToday.get(Calendar.MONTH)
                        && acYear == calToday.get(Calendar.YEAR)) {
                    // Zeigt den aktuellen Tag blau und fett an
                    holder.tv.setTypeface(null, Typeface.BOLD);
                    holder.tv.setTextColor(Color.parseColor("#4B82FF"));
                }

                // Tag setzen
                row.setTag(holder);
            }
            else{
                // Row beziehen
                holder = (rowHolder) row.getTag();
            }

            // Text neu setzen
            holder.tv.setText(holder.text);
            row.setBackgroundResource(0);

            return row;
        }

        // RowHolder
        class rowHolder{
            TextView    tv;
            String      text;
        }
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @see String  Setzen des EventHandlers
     */
    public void setEventHandler(EventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @see String  Dieses Interface definiert die public Events
     */
    interface EventHandler  {
        void onDayPress(Date date);
    }
}