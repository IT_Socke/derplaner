package de.derplaner.activities.settings;

import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.Locale;

import de.derplaner.R;
import de.derplaner.SQLite.SQL_ExportImportDB;
import de.derplaner.SQLite.SQL_PlanerDataSource;
import de.derplaner.activities.menu.SideBar;
import de.derplaner.activities.timetable.timettedit;
import de.derplaner.another.constant;

public class Settings extends Fragment {
    private SQL_PlanerDataSource ds;
    private Switch sw_StudIP;
    private Button btn_Info;
    private Spinner firstHour;
    private Spinner lastHour;
    private int iPosLang = 0;

    // Initialisierung GUI
    Spinner spDrw1;
    Spinner spDrw2;
    Spinner spDrw3;
    Spinner spCol1;
    Spinner spCol2;
    Spinner spCol3;

    EditText edWishGrade;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState){
        super.onCreateView(inflater, container, savedInstanceState);
        // Initialisierung
        final View rootView;


        /*
        ds              = new SQL_PlanerDataSource(getActivity());

        final CheckBox cbAGS = (CheckBox) rootView.findViewById(R.id.cbAGSstudent);
        final CheckBox cbRemindDate = (CheckBox) rootView.findViewById(R.id.cbRemindDate);


        if (ds.getOneEntries_SERVICE("AGSstudent").equals("TRUE")) {
            cbAGS.setChecked(true);
        }

        if (ds.getOneEntries_SERVICE("RemindDate").equals("TRUE")) {
            cbRemindDate.setChecked(true);
        }

        cbAGS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Service.setName("AGSstudent");

                if (cbAGS.isChecked()) {
                    Service.setValue("TRUE");

                    getActivity().startService(new Intent(getActivity(), de.derplaner.functions.Service.class));
                } else {
                    Service.setValue("FALSE");

                    if(ds.getCountEntries_SERVICE() == 0)
                        getActivity().stopService(new Intent(getActivity(), de.derplaner.functions.Service.class));
                    else
                        getActivity().startService(new Intent(getActivity(), de.derplaner.functions.Service.class));
                }
                ds.open();
                ds.updateEntry_SERVICE(Service);
                ds.close();
            }
        });

        cbRemindDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Service.setName("RemindDate");

                // DB oeffnen
                ds.open();

                if (cbRemindDate.isChecked()) {
                    Service.setValue("TRUE");

                    ds.updateEntry_SERVICE(Service);

                    getActivity().startService(new Intent(getActivity(), de.derplaner.functions.Service.class));
                } else {
                    Service.setValue("FALSE");

                    ds.updateEntry_SERVICE(Service);

                    if(ds.getCountEntries_SERVICE() == 0)
                        getActivity().stopService(new Intent(getActivity(), de.derplaner.functions.Service.class));
                    else
                        getActivity().startService(new Intent(getActivity(), de.derplaner.functions.Service.class));
                }

                // DB schließen
                ds.close();
            }
        });*/

        // Deklaration
        Button btnSave;
        Button btnExport;
        Button btnImport;
        Button btnEditTimes;

        // Initialisierung
        rootView        = inflater.inflate(R.layout.act_settings, container, false);
        btnSave         = (Button) rootView.findViewById(R.id.btn_SettSave);
        btnExport         = (Button) rootView.findViewById(R.id.btnExport);
        btnEditTimes    = (Button) rootView.findViewById(R.id.btnEditTimes);
        btnImport       = (Button) rootView.findViewById(R.id.btnImport);
        ds              = new SQL_PlanerDataSource(getActivity());
        sw_StudIP       = (Switch) rootView.findViewById(R.id.sw_SettStud);
        firstHour       = (Spinner) rootView.findViewById(R.id.spErsteStunde);
        lastHour        = (Spinner) rootView.findViewById(R.id.spLetzteStunde);
        edWishGrade     = (EditText) rootView.findViewById(R.id.edWishGrade);
        String sIntval  = ds.SETTINGS_getOneEntries("STUDIPINTERVAL");
        int iSel        = 0;
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());

        // Erste und Letzte Stunde auslesen
        Integer int1    = Integer.parseInt(ds.SETTINGS_getOneEntries("FirstHour"));
        Integer int2    = Integer.parseInt(ds.SETTINGS_getOneEntries("LastHour"));
        String lang = settings.getString("LOCAL", "");
        final Button btnInfo = (Button) rootView.findViewById(R.id.button_info);
        btn_Info = btnInfo;
        edWishGrade.setText(ds.SETTINGS_getOneEntries(constant.SET_WishGrade));

        // Spinner Event Drawable & Color
        spDrw1        = (Spinner) rootView.findViewById(R.id.spDrw1);
        spDrw2        = (Spinner) rootView.findViewById(R.id.spDrw2);
        spDrw3        = (Spinner) rootView.findViewById(R.id.spDrw3);
        spCol1        = (Spinner) rootView.findViewById(R.id.spColor1);
        spCol2        = (Spinner) rootView.findViewById(R.id.spColor2);
        spCol3        = (Spinner) rootView.findViewById(R.id.spColor3);

        // Spinner Adapter erstellen und setzen
        SpinnerAdapter spAda    = new SpinnerAdapter(getContext(), constant.drws);
        ColorAdapter   spACol   = new ColorAdapter(getContext(), constant.colors);
        spDrw1.setAdapter(spAda);
        spDrw2.setAdapter(spAda);
        spDrw3.setAdapter(spAda);
        spCol1.setAdapter(spACol);
        spCol2.setAdapter(spACol);
        spCol3.setAdapter(spACol);
        // TODO else Zweig fehlt
        if(ds.SETTINGS_getOneEntries("EveDra1").isEmpty())  spDrw1.setSelection(0);
            else spDrw1.setSelection(spinnerDrwColInt(ds.SETTINGS_getOneEntries("EveDra1"), constant.drws));
        if(ds.SETTINGS_getOneEntries("EveDra2").isEmpty())  spDrw2.setSelection(1);
            else spDrw2.setSelection(spinnerDrwColInt(ds.SETTINGS_getOneEntries("EveDra2"), constant.drws));
        if(ds.SETTINGS_getOneEntries("EveDra3").isEmpty())  spDrw3.setSelection(2);
            else spDrw3.setSelection(spinnerDrwColInt(ds.SETTINGS_getOneEntries("EveDra3"), constant.drws));
        if(ds.SETTINGS_getOneEntries("EveCol1").isEmpty())  spCol1.setSelection(0);
            else spCol1.setSelection(spinnerDrwColInt(ds.SETTINGS_getOneEntries("EveCol1"), constant.colors));
        if(ds.SETTINGS_getOneEntries("EveCol2").isEmpty())  spCol2.setSelection(1);
            else spCol2.setSelection(spinnerDrwColInt(ds.SETTINGS_getOneEntries("EveCol2"), constant.colors));
        if(ds.SETTINGS_getOneEntries("EveCol3").isEmpty())  spCol3.setSelection(2);
            else spCol3.setSelection(spinnerDrwColInt(ds.SETTINGS_getOneEntries("EveCol3"), constant.colors));

        // Befuellen der Spinner
        final Spinner sp_user = (Spinner)rootView.findViewById(R.id.spIntervall);
        String[] items_user = new String[]{"1", "6", "12", "24","48", "300"};
        ArrayAdapter<String> adapter_user = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items_user);
        sp_user.setAdapter(adapter_user);

        // Sprache waehlen
        Spinner sp_language = (Spinner)rootView.findViewById(R.id.spSprache);
        final String[] items_language = new String[]{"Deutsch", "Englisch"};
        ArrayAdapter<String> adapter_language = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items_language);
        sp_language.setAdapter(adapter_language);

        // Erste Stunde waehlen
        String[] items_firstHour = new String[]{"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"};
        ArrayAdapter<String> adapter_firstHour = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items_firstHour);
        firstHour.setAdapter(adapter_firstHour);
        firstHour.setSelection(int1-1);

        // Letzte Stunde waehlen
        String[] items_lastHour = new String[]{"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"};
        ArrayAdapter<String> adapter_lastHour = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items_lastHour);
        lastHour.setAdapter(adapter_lastHour);
        lastHour.setSelection(int2-1);

        int ia = 0;
        for (String item: items_language) {
            if(item.equals(lang)){
                sp_language.setSelection(ia);
                iPosLang = ia;
                break;
            }
            ia++;
        }


        //this is our TextView element, obtained by id from our XML layout
        final EditText ed_studip = (EditText) rootView.findViewById(R.id.ed_studip);
        ed_studip.setText("https://studip.fh-schmalkalden.de/dispatch.php/ical/index/dBBfGNmG");

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveClick(ed_studip, sp_user);
            }
        });

        // Need TODO
        final TextView txt1 = (TextView) rootView.findViewById(R.id.tvNutzer);
        final TextView txt2 = (TextView) rootView.findViewById(R.id.tvInterval);

        if(ds.SETTINGS_getOneEntries("STUDIP").equals("TRUE"))
        {
            sw_StudIP.setChecked(true);
            txt1.setEnabled(true);
            txt2.setEnabled(true);
            ed_studip.setEnabled(true);
            ed_studip.setText(ds.SETTINGS_getOneEntries("STUDIPLINK"));
            sp_user.setVisibility(View.VISIBLE);
            for (int i = 0; i < items_user.length; i++)
            {
                if(items_user[i].equals(sIntval))
                    iSel = i;
            }
            sp_user.setSelection(iSel);
        }

        sw_StudIP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sw_StudIP.isChecked()){
                    txt1.setEnabled(true);
                    txt2.setEnabled(true);
                    ed_studip.setEnabled(true);
                    sp_user.setVisibility(View.VISIBLE);
                    ds.SETTINGS_UpdateOrCreateEntry("STUDIP", "TRUE");
                }
                else {
                    txt1.setEnabled(false);
                    txt2.setEnabled(false);
                    ed_studip.setEnabled(false);
                    sp_user.setVisibility(View.INVISIBLE);
                    ds.SETTINGS_UpdateOrCreateEntry("STUDIP", "FALSE");
                }
            }
        });

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), getString(R.string.infolanguage),
                        Toast.LENGTH_LONG).show();
            }
        });


        final SQL_ExportImportDB exIM = new SQL_ExportImportDB(ds, getActivity());
        btnExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exIM.exportDB();
            }
        });
        btnImport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exIM.importDB();
            }
        });

        btnEditTimes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent refresh = new Intent(getActivity(), timettedit.class);
                startActivity(refresh);
            }
        });

        sp_language.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(iPosLang != position) {
                    String test = items_language[position];
                    btnInfo.setVisibility(View.VISIBLE);
                    setLocale(test);
                    iPosLang = position;
                    Toast.makeText(getActivity(), getActivity().getString(R.string.infolanguage),
                            Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        return rootView;
    }

    int spinnerDrwColInt(String sDrw, int[] iArray){
        // Deklaration
        Integer iInt;

        try{
            iInt = Integer.parseInt(sDrw);
        }
        catch (Exception ex){
            iInt = 0;

        }

        for (int i = 0; i < iArray.length; i++) {
            if(iInt == iArray[i])
                return i;
        }
        return 0;
    }

    private void setLocale(String lang) {
        String sLangShort;
        if (lang.toUpperCase().equals("DEUTSCH"))
            sLangShort = "de";
        else if (lang.toUpperCase().equals("ENGLISCH"))
            sLangShort = "en";
        else
            sLangShort = "en";

        Locale locale = new Locale(sLangShort);
        Locale.setDefault(locale);
        Configuration config = getActivity().getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        settings.edit().putString("LOCAL",lang).apply();
    }

    private void saveClick(EditText ed_studip, Spinner sp_Interval){
        // Deklaration
        int iFirstHour   = 0;
        int iLastHour    = 0;


        ds.SETTINGS_UpdateOrCreateEntry("EveDra1", String.valueOf(constant.drws[spDrw1.getSelectedItemPosition()]));
        ds.SETTINGS_UpdateOrCreateEntry("EveDra2", String.valueOf(constant.drws[spDrw2.getSelectedItemPosition()]));
        ds.SETTINGS_UpdateOrCreateEntry("EveDra3", String.valueOf(constant.drws[spDrw3.getSelectedItemPosition()]));

        if(!ds.SETTINGS_getOneEntries("EveCol1").equals(String.valueOf(constant.colors[spCol1.getSelectedItemPosition()]))) {
            ds.Event_UpdateEntrys(ds.SETTINGS_getOneEntries("EveDra1"), constant.colors[spCol1.getSelectedItemPosition()]);
            ds.SETTINGS_UpdateOrCreateEntry("EveCol1", String.valueOf(constant.colors[spCol1.getSelectedItemPosition()]));
        }
        if(!ds.SETTINGS_getOneEntries("EveCol2").equals(String.valueOf(constant.colors[spCol2.getSelectedItemPosition()]))) {
            ds.Event_UpdateEntrys(ds.SETTINGS_getOneEntries("EveDra2"), constant.colors[spCol2.getSelectedItemPosition()]);
            ds.SETTINGS_UpdateOrCreateEntry("EveCol2", String.valueOf(constant.colors[spCol2.getSelectedItemPosition()]));
        }
        if(!ds.SETTINGS_getOneEntries("EveCol3").equals(String.valueOf(constant.colors[spCol3.getSelectedItemPosition()]))) {
            ds.Event_UpdateEntrys(ds.SETTINGS_getOneEntries("EveDra3"), constant.colors[spCol3.getSelectedItemPosition()]);
            ds.SETTINGS_UpdateOrCreateEntry("EveCol3", String.valueOf(constant.colors[spCol3.getSelectedItemPosition()]));
        }


        if(sw_StudIP.isChecked()) {
            //
            ds.SETTINGS_UpdateOrCreateEntry("STUDIPLINK", ed_studip.getText().toString());
            ds.SETTINGS_UpdateOrCreateEntry("STUDIPINTERVAL", sp_Interval.getSelectedItem().toString());
            ds.SETTINGS_UpdateOrCreateEntry("STUDIPHELP", "TRUE");
            //
            getActivity().startService(new Intent(getActivity(), de.derplaner.functions.DPService.class));
        }
        else {
            //
            ds.SETTINGS_UpdateOrCreateEntry("STUDIPLINK", "");
            ds.SETTINGS_UpdateOrCreateEntry("STUDIPINTERVAL", "");
            //
            getActivity().stopService(new Intent(getActivity(), de.derplaner.functions.DPService.class));
        }

        try{
            iFirstHour  = Integer.parseInt(firstHour.getSelectedItem().toString().trim());
            iLastHour   = Integer.parseInt(lastHour.getSelectedItem().toString().trim());
        }
        catch (Exception ex){
            int sd = 0;
        }
        ds.EDITTIMES_DeleteTimes(iFirstHour, iLastHour);
        ds.SETTINGS_UpdateOrCreateEntry("FirstHour", String.valueOf(iFirstHour));
        ds.SETTINGS_UpdateOrCreateEntry("LastHour", String.valueOf(iLastHour));

        Double dGrade = Double.valueOf(edWishGrade.getText().toString().substring(0,4));
        String test = ((new DecimalFormat("0.00").format(dGrade)).replace(',','.'));
        ds.SETTINGS_UpdateOrCreateEntry(constant.SET_WishGrade, test);

        if(btn_Info.getVisibility() == View.VISIBLE)
            getActivity().startActivity(new Intent(getActivity().getApplication(), SideBar.class));
        else
            Toast.makeText(getActivity().getBaseContext(), "Gespeichert", Toast.LENGTH_SHORT).show();
    }

    /**
     * @see String  Ersteller - Tobias Häuser
     * @see String  Spinner Adpter - fuer die Icons - Hinzufuegen eines Events
     */
    private class SpinnerAdapter extends BaseAdapter {
        // Deklaration
        LayoutInflater mInflater;
        private int[] dataRecieved;

        // Konstruktor
        SpinnerAdapter(Context context, int[] to) {
            dataRecieved    = to;
            mInflater       = LayoutInflater.from(context);
        }


        @Override
        public int getCount() {
            return dataRecieved.length;
        }
        @Override
        public Object getItem(int i) {
            return null;
        }
        @Override
        public long getItemId(int i) {
            return 0;
        }
        @NonNull
        @Override
        public View getView(int position, View row, @NonNull ViewGroup parent) {
            // Deklaration
            rowHolder holder;

            // Initialisierung
            if (row == null) {
                // Initialisierung
                holder      = new rowHolder();
                row         = mInflater.inflate(R.layout.page_imageview, parent, false);

                // Holderitem beziehen
                holder.icon = (ImageView) (row.findViewById(R.id.imageView3));

                // Tag setzen
                row.setTag(holder);
            }
            else
                // Row beziehen
                holder = (rowHolder) row.getTag();

            // Icon setzen
            holder.icon.setImageResource(dataRecieved[position]);

            return row;
        }


        // RowHolder
        class rowHolder{
            ImageView icon;
        }
    }


    /**
     * @see String  Ersteller - Tobias Häuser
     * @see String  Spinner Adapter - fuer die Farben . Hinzufuegen eines Events
     */
    private class ColorAdapter extends BaseAdapter {
        // Deklaration
        LayoutInflater mInflater;
        private int[] dataRecieved;

        // Konstruktor
        ColorAdapter(Context context, int[] to) {
            dataRecieved    = to;
            mInflater       = LayoutInflater.from(context);
        }


        @Override
        public int getCount() {
            return dataRecieved.length;
        }
        @Override
        public Object getItem(int i) {
            return null;
        }
        @Override
        public long getItemId(int i) {
            return 0;
        }
        @NonNull
        @Override
        public View getView(int position, View row, @NonNull ViewGroup parent) {
            // Deklaration
            rowHolder holder;

            // Initialisierung
            if (row == null) {
                // Initialisierung
                holder              = new rowHolder();
                row                 = mInflater.inflate(R.layout.page_imageview, parent, false);

                // Holderitem beziehen
                holder.icon = (ImageView) (row.findViewById(R.id.imageView3));

                // Tag setzen
                row.setTag(holder);
            }
            else
                // Row beziehen
                holder = (rowHolder) row.getTag();

            // Icon setzen
            holder.icon.setBackgroundColor(dataRecieved[position]);

            return row;
        }


        // RowHolder der oberen ExpList
        class rowHolder{
            ImageView icon;
        }
    }
}
