package de.derplaner.activities.menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import de.derplaner.R;
import de.derplaner.SQLite.SQL_PlanerDataSource;
import de.derplaner.activities.events.Calendar_Frag;
import de.derplaner.activities.events.List_Frag;
import de.derplaner.activities.grade.grades;
import de.derplaner.activities.info.Info;
import de.derplaner.Main;
import de.derplaner.activities.settings.Settings;
import de.derplaner.activities.timetable.timett;
import de.derplaner.another.blankPage;
import de.derplaner.another.rebuildPage;

public class SideBar extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    // Globale Deklaration
    private SQL_PlanerDataSource ds;
    private ActionBarDrawerToggle toggle;
    private LayoutInflater inflater;
    private ViewPager vp_pages;
    private String title;
    private SectionsPagerAdapter pagerAdapter;
    private int iSave;
    private boolean bDataReset = false;

    // Globale Initialisierung
    private Menu menu           = null;
    private String bundle       = "";
    private boolean bCreate     = true;
    private boolean bItem1      = true;
    private boolean bItem2      = false;
    private boolean bItem3      = false;

    // Constanten
    private final String sTitels    = "titel";
    private final String sViewp     = "viewpager";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialisierung
        setContentView(R.layout.act_menu);
        inflater        = this.getLayoutInflater();
        ds              = new SQL_PlanerDataSource(this);
        vp_pages        = (ViewPager) findViewById(R.id.fickdich);
        pagerAdapter    = new SectionsPagerAdapter(getSupportFragmentManager());

        // Deklaration & Initialisierung
        Toolbar toolbar                     = (Toolbar) findViewById(R.id.toolbar);
        DrawerLayout drawer                 = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView       = (NavigationView) findViewById(R.id.nav_view);

        // Toolbar setzen
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        // Termin ViewPager setzen
        if(vp_pages != null)
            vp_pages.setAdapter(pagerAdapter);

        // Navigationmenue & NavButton
        toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();
        if(navigationView != null)
            navigationView.setNavigationItemSelectedListener(this);
        if(drawer != null)
            drawer.addDrawerListener(toggle);

        // Init der ersten Aufruseite
        if(savedInstanceState == null)
            displayView(R.id.events);

        // Titel setzen
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Erstellung des Optionmenus
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);

        menu.findItem(R.id.evAdd).setVisible(bItem1);
        menu.findItem(R.id.ttAdd).setVisible(bItem2);
        menu.findItem(R.id.ttEdit).setVisible(bItem3);
        return true;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Ereingnis des Selectierten OptionItems
        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        else if(item == menu.findItem(R.id.evAdd)) {
            bundle = "ADD_EV";
            displayView(R.id.events);
        }
        else if(item == menu.findItem(R.id.ttAdd)) {
            bundle = "ADD";
            displayView(R.id.timetable);
        }
        else if(item == menu.findItem(R.id.ttEdit)) {
            bundle = "EDIT";
            displayView(R.id.timetable);
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        // Initialisierung
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Drayer schliessen
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } // Schließen des Timetable Option
        else if (bundle.equals("ADD_EV"))
        {
            bundle = "";
            displayView(R.id.events);
        }// Schließen des Timetable Option
        else if (bundle.equals("ADD") || bundle.equals("EDIT"))
        {
            bundle = "";
            displayView(R.id.timetable);
        } // App beenden
        else {
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        // Notwendige Daten speicher
        if(getSupportActionBar() != null && getSupportActionBar().getTitle() != null)
            savedInstanceState.putString(sTitels, getSupportActionBar().getTitle().toString());
        savedInstanceState.putInt(sViewp, vp_pages.getVisibility());

        if(menu != null) {
            savedInstanceState.putBoolean("ITEM1", menu.findItem(R.id.evAdd).isVisible());
            savedInstanceState.putBoolean("ITEM2", menu.findItem(R.id.ttAdd).isVisible());
            savedInstanceState.putBoolean("ITEM3", menu.findItem(R.id.ttEdit).isVisible());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // Notwendige Daten wiederherstellen
        title = savedInstanceState.getString(sTitels);
        switch (savedInstanceState.getInt(sViewp)){
            case View.GONE:
                vp_pages.setVisibility(View.GONE);
                break;
            case View.VISIBLE:
                vp_pages.setVisibility(View.VISIBLE);
                break;
        }

        bItem1 = savedInstanceState.getBoolean("ITEM1", false);
        bItem2 = savedInstanceState.getBoolean("ITEM2", false);
        bItem3 = savedInstanceState.getBoolean("ITEM3", false);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        bundle = "";
        displayView(item.getItemId());
        return true;
    }


    // Fragment setzen/ersetzen
    public void displayView(int viewId) {
        // Deklaration
        Fragment fragment   = null;
        Bundle bBundle      = new Bundle();

        // Init Optionmenu Items(Timetable)
        MenuItem item1 = null;
        MenuItem item2 = null;
        MenuItem item3 = null;
        if(menu != null) {
            item1 = menu.findItem(R.id.evAdd);
            item2 = menu.findItem(R.id.ttAdd);
            item3 = menu.findItem(R.id.ttEdit);
            item1.setVisible(false);
            item2.setVisible(false);
            item3.setVisible(false);
        }

        // Initialisierung
        title = getString(R.string.app_name);
        vp_pages.setVisibility(View.GONE);

        // Orientation nicht definiert setzen
        setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        // Aufrufen des jeweiligen Fragments & dessen Einstellungen
        switch (viewId) {
            case R.id.events:
                // Orientaton setzen, View update & sichtbar machn
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

                //
                if(bCreate)
                    bCreate = false;
                else {
                    if(bundle == null)
                        bundle = "";

                    bBundle.putString("KEY", bundle);

                    pagerAdapter.setBudle(bBundle);
                    pagerAdapter.notifyDataSetChanged();
                }

                // Sichtbar machen
                if(item1 != null) item1.setVisible(true);
                vp_pages.setVisibility(View.VISIBLE);

                fragment        = new blankPage();
                title           = getString(R.string.title_section1);
                break;
            case R.id.grades:
                fragment = new grades();
                title = getString(R.string.title_section2);
                break;
            case R.id.timetable:
                if(bundle == null)
                    bundle = "";

                bBundle.putString("KEY", bundle);
                fragment = new timett();
                fragment.setArguments(bBundle);

                // Optionitems sichtbar setzen
                if(item2 != null) item2.setVisible(true);
                if(item3 != null) item3.setVisible(true);

                title = getString(R.string.title_section3);
                break;
            case R.id.reset_data:
                // Alertdialog - Daten zuruecksetzen bilden
                final AlertDialog alert = ADdataReset(ds, inflater);

                // Alertdialog anzeigen
                alert.show();

                alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if(bDataReset) {
                            bDataReset = false;
                            startActivity(new Intent(getApplication(), Main.class));
                        }
                        else
                            displayView(iSave);
                    }
                });

                break;
            case R.id.settings:
                fragment = new Settings();
                title = getString(R.string.settings);
                break;
            case R.id.info:
                fragment = new Info();
                title = getString(R.string.info);
                break;
            case R.id.help:
                fragment = new rebuildPage();
                title = getString(R.string.help);
                break;
        }

        // Fragment sichern
        if(viewId !=  R.id.reset_data)
            iSave = viewId;

        // Hintergrundlayout ersetzen
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        // Toolbartital setzen
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

        // Linke Sidabar schliessen
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if(drawer != null)  drawer.closeDrawer(GravityCompat.START);
    }


    public AlertDialog ADdataReset(final SQL_PlanerDataSource ds, final LayoutInflater inflater)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(inflater.getContext())
                .setCancelable(false)
                .setTitle("Wollen Sie Ihre Daten löschen?")
                .setPositiveButton("Löschen", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Datenbank reset
                        ds.open();
                        ds.newCreate();
                        ds.close();

                        Toast.makeText(inflater.getContext(), "Daten wurden zurückgesetzt.", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        dialog.cancel();

                        bDataReset = true;

                    }
                })
                .setNegativeButton("Abbrechen",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                dialog.cancel();
                            }
                        })
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            dialog.dismiss();
                            dialog.cancel();
                        }
                        return true;
                    }
                });

        // create an alert dialog
        return builder.create();
    }


    // Klasse der SwitchPage im Termin Fragment
    private class SectionsPagerAdapter extends FragmentPagerAdapter {
        // Deklaration
        Bundle bundle;

        void setBudle(Bundle bundle) {
            this.bundle = bundle;
        }

        // Konstruktor
        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getItemPosition(Object object) {
            if(object instanceof List_Frag) {
                List_Frag f = (List_Frag) object;
                    f.update(bundle);
            }
            else if(object instanceof Calendar_Frag) {
                Calendar_Frag f = (Calendar_Frag) object;
                f.update();
            }
            return super.getItemPosition(object);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return new Calendar_Frag();
                case 1:
                    return new List_Frag();
                default:
                    return new rebuildPage();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
