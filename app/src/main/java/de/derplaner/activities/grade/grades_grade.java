package de.derplaner.activities.grade;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import de.derplaner.R;
import de.derplaner.SQLite.SQL_PlanerDataSource;
import de.derplaner.dataObjects.DS_Grade;
import de.derplaner.dataObjects.DS_Subject;


public class grades_grade extends AppCompatActivity {
    // Datenbank
    private SQL_PlanerDataSource ds;
    String sFach;

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<DS_Grade> expandableListTitle;
    HashMap<DS_Grade, List<DS_Grade>> expandableListDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Init
        ds                      = new SQL_PlanerDataSource(this);

        Bundle b = getIntent().getExtras();
        sFach = b.getString("FACH");

        setContentView(R.layout.act_grades_grade);
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        expandableListDetail = getData();
        expandableListTitle = new ArrayList<>(expandableListDetail.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);





        Button btnNewGrade = (Button) findViewById(R.id.button2);
        btnNewGrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddGrade();
            }
        });

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle(sFach);
        }

        expandableListView.setGroupIndicator(getResources().getDrawable(R.drawable.my_group_statelist));
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View clickedView, int groupPosition, long rowId) {
                ImageView groupIndicator = (ImageView) clickedView.findViewById(R.id.imageView2);
                if (parent.isGroupExpanded(groupPosition)) {
                    parent.collapseGroup(groupPosition);
                    groupIndicator.setImageResource(R.drawable.expand_max);
                } else {
                    parent.expandGroup(groupPosition);
                    groupIndicator.setImageResource(R.drawable.expand_min);
                }
                return true;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                /*Toast.makeText(
                        getApplicationContext(),
                        expandableListTitle.get(groupPosition)
                                + " -> "
                                + expandableListDetail.get(
                                expandableListTitle.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT
                ).show();*/
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event

        onBackPressed(); return true;

        //return super.onOptionsItemSelected(item);
    }

    private void AddGrade(){
        // AD & Dismiss bilden - FirstStart Text
        LayoutInflater inflater = this.getLayoutInflater();

        // AD & Dismiss bilden - FirstConfig Text
        final AlertDialog alert = ADStartConfig(inflater);

        AlertDialog alert2 = ADStartLongClick(inflater);
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //alert2.show();
            }
        });

        // AD FirstStart aufrufen
        alert.show();
    }


    private class CustomExpandableListAdapter extends BaseExpandableListAdapter {

        private Context context;
        private List<DS_Grade> expandableListTitle;
        private HashMap<DS_Grade, List<DS_Grade>> expandableListDetail;

        CustomExpandableListAdapter(Context context, List<DS_Grade> expandableListTitle,
                                           HashMap<DS_Grade, List<DS_Grade>> expandableListDetail) {
            this.context = context;
            this.expandableListTitle = expandableListTitle;
            this.expandableListDetail = expandableListDetail;
        }

        @Override
        public Object getChild(int listPosition, int expandedListPosition) {
            return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                    .get(expandedListPosition);
        }

        @Override
        public long getChildId(int listPosition, int expandedListPosition) {
            return expandedListPosition;
        }

        @Override
        public View getChildView(int listPosition, final int expandedListPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
            final DS_Grade expandedListText = (DS_Grade) getChild(listPosition, expandedListPosition);
            if (convertView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) this.context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.list_item, null);
            }
            TextView expandedListTextView = (TextView) convertView
                    .findViewById(R.id.expandedListItem);
            expandedListTextView.setText(expandedListText.getThema());
            return convertView;
        }

        @Override
        public int getChildrenCount(int listPosition) {
            return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                    .size();
        }

        @Override
        public Object getGroup(int listPosition) {
            return this.expandableListTitle.get(listPosition);
        }

        @Override
        public int getGroupCount() {
            return this.expandableListTitle.size();
        }

        @Override
        public long getGroupId(int listPosition) {
            return listPosition;
        }

        @Override
        public View getGroupView(int listPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {

            DS_Grade listGroud = (DS_Grade) getGroup(listPosition);
            if (convertView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) this.context.
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.list_group, null);
            }
            TextView tvGrade = (TextView) convertView.findViewById(R.id.tvGrade);
            TextView tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            TextView tvDate = (TextView) convertView.findViewById(R.id.tv_Date);
            tvGrade.setTypeface(null, Typeface.BOLD);
            tvTitle.setTypeface(null, Typeface.BOLD);
            tvDate.setTypeface(null, Typeface.BOLD);
            tvGrade.setText(String.valueOf(listGroud.getGrade()));
            tvTitle.setText(listGroud.getThema());

            //Image view which you put in row_group_list.xml
            View ind = convertView.findViewById(R.id.imageView2);
            if (ind != null)
            {
                ImageView indicator = (ImageView) ind;
                if (getChildrenCount(listPosition) == 0)
                {
                    tvDate.setText(listGroud.getDate());
                    indicator.setVisibility(View.GONE);
                }
                else {
                    indicator.setVisibility(View.VISIBLE);
                    tvDate.setText("");
                }
            }

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int listPosition, int expandedListPosition) {
            return true;
        }
    }


    public AlertDialog ADStartLongClick(LayoutInflater inflater)
    {
        View vStart  = inflater.inflate(R.layout.scv_grade_insert, null);

        // AlertDialog zusammensetzen
        // TODO Beschreibung hinzufügen was alles gesetzt wird
        AlertDialog.Builder builder = new AlertDialog.Builder(inflater.getContext())
                .setCancelable(false)
                .setView(vStart)
                .setPositiveButton("Weiter", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        dialog.cancel();

                    }
                })
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            dialog.dismiss();
                            dialog.cancel();
                            return true;
                        }
                        return false;
                    }
                });

        return builder.create();
    }

    private AlertDialog ADStartConfig(LayoutInflater inflater)
    {
        final View vStart    = inflater.inflate(R.layout.scv_grade_insert, null);
        AlertDialog.Builder builder;
        AlertDialog alert;

        final EditText ed1 = (EditText) vStart.findViewById(R.id.ed_room3);
        EditText ed2 = (EditText) vStart.findViewById(R.id.ed_room4);
        final EditText ed3 = (EditText) vStart.findViewById(R.id.ed_room);
        final EditText ed4 = (EditText) vStart.findViewById(R.id.ed_room5);
        final EditText ed5 = (EditText) vStart.findViewById(R.id.ed_room1);
        final EditText ed6 = (EditText) vStart.findViewById(R.id.ed_room2);


        /*vStart.findViewById(R.id.rdBwGUg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRadioButton(v.getId(), vStart);
            }
        });*/


        // AlertDialog zusammensetzen
        // TODO Beschreibung hinzufügen was alles gesetzt wird
        builder = new AlertDialog.Builder(inflater.getContext())
                .setCancelable(false)
                .setView(vStart)
                .setPositiveButton("Speichern", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DS_Grade nGrade = new DS_Grade();
                        nGrade.setSubject(sFach);
                        nGrade.setThema(ed4.getText().toString());
                        nGrade.setTeacher(ed5.getText().toString());
                        nGrade.setDate(ed3.getText().toString());
                        int iNote = Integer.parseInt(ed1.getText().toString());
                        nGrade.setGrade(iNote);
                        nGrade.setInfo(ed6.getText().toString());


                        //Eintrag anlegen
                        try {
                            ds.open();
                            ds.createEntry_GRADES(nGrade);
                            ArrayList<String> test = ds.getAllEntries_GRADES(sFach, "","");
                            int iiNote = 0;
                            double iNC = 0.00;
                            double iGes = 0;

                            for (int i = 2; i < test.size(); i=i+3) {

                                iiNote = Integer.parseInt(test.get(i));
                                iGes = iGes + iiNote;
                            }

                            if(test.size() > 0)
                                iNC = iGes / (test.size()/3);
                            else
                                iNC = 0.00;

                            DS_Subject values = new DS_Subject();
                            values.setSubject(sFach);
                            values.setNC(iNC);
                            ds.SUBJECTS_UpdateOrCreateEntry(values);
                            ds.close();
                        } catch (Exception ex) {
                            Toast.makeText(grades_grade.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                        expandableListDetail = getData();
                        expandableListTitle = new ArrayList<>(expandableListDetail.keySet());
                        expandableListAdapter = new CustomExpandableListAdapter(grades_grade.this, expandableListTitle, expandableListDetail);
                        expandableListView.setAdapter(expandableListAdapter);

                        dialog.dismiss();
                        dialog.cancel();

                    }
                })
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            dialog.dismiss();
                            dialog.cancel();
                            return true;
                        }
                        return false;
                    }
                });

        // Erstellen des AlertDialoges
        alert = builder.create();

        return alert;
    }

    public HashMap<DS_Grade, List<DS_Grade>> getData() {
        HashMap<DS_Grade, List<DS_Grade>> expandableListDetail = new HashMap<>();



        //Alle anzeigen
        List<DS_Grade> gradeList = ds.getAllEntries_GRADES("0" , sFach);



        DS_Grade informatik = new DS_Grade();
        DS_Grade infosub = new DS_Grade();
        informatik.setThema("Informatik");
        informatik.setGrade(1.00);
        List<DS_Grade> info = new ArrayList<>();
        infosub.setDate("12.12.2017");
        infosub.setThema("TI");
        infosub.setGrade(2.00);
        info.add(infosub);
        infosub = new DS_Grade();
        infosub.setDate("27.05.2017");
        infosub.setThema("Programmierung 1");
        infosub.setGrade(1.00);
        info.add(infosub);

        for (DS_Grade item: gradeList) {
            expandableListDetail.put(item, new ArrayList<DS_Grade>());
        }

        expandableListDetail.put(informatik, info);


        return expandableListDetail;
    }
}