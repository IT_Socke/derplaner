package de.derplaner.activities.grade;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.text.DecimalFormat;
import java.util.ArrayList;

import de.derplaner.R;
import de.derplaner.SQLite.SQL_PlanerDataSource;
import de.derplaner.another.constant;
import de.derplaner.dataObjects.DS_Subject;

public class grades extends Fragment {

    private View rootView;
    private ListView subjectList;
    private SQL_PlanerDataSource ds;
    private ArrayList<DS_Subject> mySubjects = new ArrayList<>();
    private Boolean bHelper = true;
    private GradeAdapter gaAdapter;
    private int spPositionClass = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        // Initialisierung
        rootView            = inflater.inflate(R.layout.act_grades, container, false);
        Button addSubject   = (Button) rootView.findViewById(R.id.button2);
        subjectList         = (ListView) rootView.findViewById(R.id.lvGrades);
        ds                  = new SQL_PlanerDataSource(getActivity());


        final ArrayList<String> lClasses = ds.SUBJECTS_getClasses("0");
        String dsAll = "Alle";
        lClasses.add(0, dsAll);


        String[] mStringArray = new String[lClasses.size()];
        mStringArray = lClasses.toArray(mStringArray);

        // Sprache waehlen
        final Spinner sp_language = (Spinner)rootView.findViewById(R.id.spSprache);
        ArrayAdapter<String> adapter_language = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, mStringArray);
        sp_language.setAdapter(adapter_language);

        sp_language.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mySubjects = ds.getAllEntries_SUBJECTS("0", lClasses.get(position));
                if(mySubjects.size() > 0)
                    spPositionClass = position;

                setGrade();
                gaAdapter = new GradeAdapter(getActivity(), mySubjects);
                subjectList.setAdapter(gaAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Fächer auslesen
        try {
            mySubjects = ds.getAllEntries_SUBJECTS("0", dsAll);
        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
            return rootView;
        }

        //
        addSubject.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showSubjectDialog(null);
            }
        });

        subjectList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!bHelper) {
                    bHelper = true;
                    return;
                }

                //TH
                String sFach;

                if (mySubjects != null) {
                    sFach = mySubjects.get(position).getSubject();

                    if (!mySubjects.get(position).getFinalGradeB()) {
                        Intent intent = new Intent(getActivity(), grades_grade.class);
                        intent.putExtra("FACH", sFach);
                        startActivity(intent);
                    }
                }

                //END TH
            }
        });

        subjectList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

                // setup a dialog window
                alertDialog
                        .setCancelable(false)
                        .setNeutralButton("Löschen", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                try {
                                    ds.deleteEntry_SUBJECTS(mySubjects.get(position));
                                    mySubjects = ds.getAllEntries_SUBJECTS("0", sp_language.getSelectedItem().toString());
                                    ds.close();
                                } catch (Exception ex) {
                                    Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                                gaAdapter = new GradeAdapter(getActivity(), mySubjects);
                                subjectList.setAdapter(gaAdapter);
                            }
                        })
                        .setNegativeButton("Ändern", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            showSubjectDialog(mySubjects.get(position));
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                                if (keyCode == KeyEvent.KEYCODE_BACK) {
                                    dialog.dismiss();
                                }
                                return true;
                            }
                        });
                // create an alert dialog
                AlertDialog alert = alertDialog.create();
                alert.show();

                return false;
            }
        });

        setGrade();

        // ListAdapter setzen
        gaAdapter = new GradeAdapter(getActivity(), mySubjects);
        subjectList.setAdapter(gaAdapter);

        return rootView;
    }

    private void setGrade(){
        double ia = 0;
        double iZahl = 0;
        for (DS_Subject item: mySubjects) {
            try{
                String sdas = String.valueOf(item.getNC());

                if(!sdas.equals("0.00")) {
                    ia += (Double.valueOf(sdas) * item.getWertung());
                    iZahl += (1 * item.getWertung());
                }
            }
            catch (Exception ex) {
                //
            }
        }
        ia = ia / iZahl;
        Double dDou = Double.valueOf((new DecimalFormat("0.00").format(ia)).replace(',','.'));


        String test = ((new DecimalFormat("0.00").format(ia)).replace(',','.') + " Ø");
        TextView tvGrade = (TextView) rootView.findViewById(R.id.textView8);
        TextView tvGrade2 = (TextView) rootView.findViewById(R.id.tvWishGrade);
        tvGrade2.setText((ds.SETTINGS_getOneEntries(constant.SET_WishGrade) + " Ø"));
        tvGrade.setText(test);

        // TODO beachten wenn kein text drinne ist
        Double dDou2 = Double.valueOf(tvGrade2.getText().toString().substring(0,4));
        if(dDou2 > dDou)
            tvGrade.setTextColor(Color.GREEN);
        else if(dDou2 < dDou)
            tvGrade.setTextColor(Color.RED);
        else
            tvGrade.setTextColor(Color.BLACK);
    }

    @Override
    public void onResume() {
        gaAdapter.notifyDataSetChanged();
        super.onResume();
    }

    private void showSubjectDialog(final DS_Subject dsSubject) {
        // Deklaration
        AlertDialog.Builder builder;
        ArrayList<String> myList = new ArrayList<>();

        // Initialisierung
        View subjetView                 = View.inflate(getActivity(), R.layout.scv_subject_add, null);
        final Spinner spSubject         = (Spinner) subjetView.findViewById(R.id.spSubject);
        final Spinner spClass           = (Spinner) subjetView.findViewById(R.id.spClass);
        final EditText edSubject        = (EditText) subjetView.findViewById(R.id.edSubject);
        final EditText edWertung        = (EditText) subjetView.findViewById(R.id.edWertung);
        final EditText edFinalGrade     = (EditText) subjetView.findViewById(R.id.edFinalGrade);
        final EditText edWishGrade      = (EditText) subjetView.findViewById(R.id.edWishGrade);
        final EditText edCurClass       = (EditText) subjetView.findViewById(R.id.edClass);
        final CheckBox cbFinalGrade     = (CheckBox) subjetView.findViewById(R.id.cbFinalGrade);
        final TextView tvFinalGrade     = (TextView) subjetView.findViewById(R.id.tvFinalGrade);
        String[] subjectArray           = getActivity().getResources().getStringArray(R.array.subjectList);

        final ArrayList<String> lClasses = ds.SUBJECTS_getClasses("0");
        String dsAll = "Alle";
        lClasses.add(0, dsAll);

        for (String item: subjectArray) {
            Boolean bbHelper = false;

            for (int i = 0; i < mySubjects.size(); i++)
            {
                if(item.trim().toUpperCase().equals(mySubjects.get(i).getSubject().trim().toUpperCase())) {
                    bbHelper = true;
                    break;
                }
            }

            if(!bbHelper) {
                myList.add(item);
            }
        }

        String[] mStringArray2 = new String[lClasses.size()];
        mStringArray2 = lClasses.toArray(mStringArray2);

        ArrayAdapter<String> adapter_lastHour2 = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_spinner_dropdown_item, mStringArray2);
        spClass.setAdapter(adapter_lastHour2);


        String[] mStringArray = new String[myList.size()];
        mStringArray = myList.toArray(mStringArray);

        // Letzte Stunde waehlen
        ArrayAdapter<String> adapter_lastHour = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_spinner_dropdown_item, mStringArray);
        spSubject.setAdapter(adapter_lastHour);

        if(dsSubject != null) {
            int i = 0;
            for (String item: myList) {
                if(item.equals(dsSubject.getSubject())) {
                    spSubject.setSelection(i);
                    break;
                }
                i++;
            }
            if(i >= myList.size())
                spSubject.setSelection(1);

            i = 0;
            for (String item: lClasses) {
                if(item.equals(dsSubject.getCurClass())) {
                    spPositionClass = i;
                    break;
                }
                i++;
            }

            edSubject.setText(dsSubject.getSubject());
            edWertung.setText(String.valueOf(dsSubject.getWertung()));
            edFinalGrade.setText(String.valueOf(dsSubject.getNC()));
            if(dsSubject.getWishNC() != 0.0)
                edWishGrade.setText(String.valueOf(dsSubject.getWishNC()));
            edCurClass.setText(dsSubject.getCurClass());
            if(dsSubject.getFinalGradeB()) {
                cbFinalGrade.setChecked(true);
                edFinalGrade.setEnabled(true);
            }
        }



        cbFinalGrade.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                edFinalGrade.setEnabled(isChecked);
                tvFinalGrade.setEnabled(isChecked);
            }
        });

        spSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 1)
                    edSubject.setEnabled(true);
                else
                {
                    edSubject.setText("");
                    edSubject.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spClass.setSelection(spPositionClass);

        spClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0)
                    edCurClass.setEnabled(true);
                else
                {
                    edCurClass.setText("");
                    edCurClass.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // AlertDialog zusammensetzen
        builder = new AlertDialog.Builder(getActivity().getLayoutInflater().getContext())
                .setCancelable(false)
                .setView(subjetView)
                .setPositiveButton("Speichern", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DS_Subject subi;
                        if(dsSubject == null)
                            subi = new DS_Subject();
                        else
                            subi = dsSubject;

                        try {
                            if(!edSubject.isEnabled())
                                subi.setSubject(spSubject.getSelectedItem().toString());
                            else
                                subi.setSubject(edSubject.getText().toString());
                            subi.setWertung(Double.valueOf(edWertung.getText().toString()));
                            if(cbFinalGrade.isChecked())
                                subi.setNC(Double.valueOf(edFinalGrade.getText().toString()));
                            subi.setFinalGradeB(cbFinalGrade.isChecked());
                            if(!edCurClass.isEnabled())
                                subi.setCurClass(spClass.getSelectedItem().toString());
                            else
                                subi.setCurClass(edCurClass.getText().toString());
                            ds.SUBJECTS_UpdateOrCreateEntry(subi);
                        }
                        catch (Exception ex) {
                            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                        }


                        mySubjects = null;
                        try {
                            mySubjects = ds.getAllEntries_SUBJECTS("0", spClass.getSelectedItem().toString());
                        }
                        catch (Exception ex) {
                            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                        // ListAdapter setzen
                        subjectList.setAdapter(new GradeAdapter(getActivity(), mySubjects));
                        dialog.dismiss();
                        dialog.cancel();

                    }
                })
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            dialog.dismiss();
                            dialog.cancel();
                            return true;
                        }
                        return false;
                    }
                });

        // Erstellen des AlertDialoges
        final AlertDialog alert = builder.create();

        //TH
        String curClass;
        if(!edCurClass.isEnabled())
            curClass = spClass.getSelectedItem().toString();
        else
            curClass = edCurClass.getText().toString();

        try {
            mySubjects = ds.getAllEntries_SUBJECTS("0", curClass);
        }
        catch (Exception ex) {
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

        alert.show();
    }


    private class GradeAdapter extends ArrayAdapter<DS_Subject> {
        private ArrayList<DS_Subject> grades;
        private Context context;

        GradeAdapter(Context context, ArrayList<DS_Subject> grades) {
            super(context, R.layout.page_grades, grades);
            this.context = context;
            this.grades = grades;
        }

        @NonNull
        @Override
        public View getView(final int position, View row, @NonNull ViewGroup parent) {
            // Deklaration
            rowHolder holder;

            if (row == null) {
                // Initialisierung
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                row = inflater.inflate(R.layout.page_grades, parent, false);
                holder = new rowHolder();


                // Holderitem beziehen
                holder.item = grades.get(position);

                // Holder Objecte setzen
                holder.subject = (TextView) row.findViewById(R.id.tvTop);
                holder.grade = (TextView) row.findViewById(R.id.tvCenterTop);

                // ???
                row.setTag(holder);
            }
            else
                holder = (rowHolder) row.getTag();

            // object item based on the position
            DS_Subject objectItem = grades.get(position);

            // assign values if the object is not null
            if(objectItem != null) {
                // get the TextView from the ViewHolder and then set the text (item name) and tag (item ID) values
                holder.subject.setTag(objectItem.getId());
                holder.grade.setTag(objectItem.getId());

                holder.subject.setText(objectItem.getSubject());
                String sGrade = ((new DecimalFormat("0.00").format(objectItem.getNC())).replace(',','.') + " Ø");
                holder.grade.setText(String.valueOf(sGrade));
            }

            return row;
        }

        @Override
        public void notifyDataSetChanged() {
            this.grades = mySubjects;
            super.notifyDataSetChanged();
        }

        class rowHolder {
            DS_Subject item;
            TextView subject;
            TextView grade;
        }
    }
}
