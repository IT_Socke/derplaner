package de.derplaner.activities.info;

import androidx.annotation.Nullable;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import de.derplaner.R;
import de.derplaner.SQLite.SQL_PlanerDataSource;

public class Info extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState){
        super.onCreateView(inflater, container, savedInstanceState);
        // Deklaration
        String sDate;
        TextView tv;
        View rootView;
        SQL_PlanerDataSource ds;

        // Initialisierung
        rootView        = inflater.inflate(R.layout.act_info, container, false);
        ds              = new SQL_PlanerDataSource(getActivity());
        tv              = (TextView) rootView.findViewById(R.id.tv_infoStudIP_time);
        sDate           = ds.SETTINGS_getOneEntries("TIMESTUDIP");

        // Aktuallisierungzeit setzen
        tv.setText(sDate);

        return rootView;

    }
}
