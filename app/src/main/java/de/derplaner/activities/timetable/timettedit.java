package de.derplaner.activities.timetable;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import de.derplaner.R;
import de.derplaner.SQLite.SQL_PlanerDataSource;
import de.derplaner.dataObjects.DS_TimeEdit;


public class timettedit extends AppCompatActivity {
    private ArrayList<myRow> lTime = new ArrayList<>();
    private SQL_PlanerDataSource ds;
    private ListView lvTimes;
    int selItem = -1;

    private InputFilter hourFilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source.length() == 0) {
                return null;// deleting, keep original editing
            }
            String result = "";
            result += dest.toString().substring(0, dstart);
            result += source.toString().substring(start, end);
            result += dest.toString().substring(dend, dest.length());

            if (result.length() > 2) {
                return "";// do not allow this edit
            }
            boolean allowEdit = true;
            char c;
            if (result.length() > 0) {
                c = result.charAt(0);
                allowEdit = (c >= '0' && c <= '2');
            }
            if (result.length() > 1) {
                c = result.charAt(1);
                if(result.charAt(0) == '0' || result.charAt(0) == '1')
                    allowEdit &= (c >= '0' && c <= '9');
                else
                    allowEdit &= (c >= '0' && c <= '3');
            }
            return allowEdit ? null : "";
        }
    };

    private InputFilter minFilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source.length() == 0) {
                return null;// deleting, keep original editing
            }
            String result = "";
            result += dest.toString().substring(0, dstart);
            result += source.toString().substring(start, end);
            result += dest.toString().substring(dend, dest.length());

            if (result.length() > 2) {
                return "";// do not allow this edit
            }
            boolean allowEdit = true;
            char c;
            if (result.length() > 0) {
                c = result.charAt(0);
                allowEdit = (c >= '0' && c <= '5');
            }
            if (result.length() > 1) {
                c = result.charAt(1);
                allowEdit &= (c >= '0' && c <= '9');
            }
            return allowEdit ? null : "";
        }
    };




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_edittimes);

        // Deklaration
        myRow item;
        Button btnClear;
        Button btnSave;
        Integer curHour;
        Integer iHelp;

        // Initialisierung
        lvTimes     = (ListView) findViewById(R.id.lv_timeedit);
        ds          = new SQL_PlanerDataSource(this);
        btnClear    = (Button) findViewById(R.id.btn_clear);
        btnSave     = (Button) findViewById(R.id.btn_save);

        // Erste und Letzte Stunde auslesen
        Integer int1    = Integer.parseInt(ds.SETTINGS_getOneEntries("FirstHour"));
        Integer int2    = Integer.parseInt(ds.SETTINGS_getOneEntries("LastHour"));
        curHour         = int1;

        // ButtonClickFunktion setzen
        if(btnSave != null) {
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    saveEditTimes();
                    Toast.makeText(timettedit.this, getString(R.string.save),
                            Toast.LENGTH_LONG).show();
                }
            });  }
        if(btnClear != null) {
            btnClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clearEditTimes();
                }
            });  }

        ArrayList<DS_TimeEdit> mList = ds.getAllEntries_EDITTIMES();

        // TimeAdapter mit den Stunden fuellen
        if(mList.size() > 0) {
            int index = 0;
            for (int i = int1; i <= int2; i++) {
                if(index < mList.size()) {
                    if (mList.get(index).getHour().equals(String.valueOf(i))) {
                        String frTime = mList.get(index).getFromTime();
                        String toTime = mList.get(index).getToTime();

                        if (frTime.isEmpty() || toTime.isEmpty())
                            item = new myRow(Integer.toString(i) + ". Stunde:", "", "", "", "");
                        else {
                            item = new myRow(Integer.toString(i) + ". Stunde:", frTime.substring(0, 2), frTime.substring(3, 5),
                                    toTime.substring(0, 2), toTime.substring(3, 5));
                        }
                        index++;
                    }
                    else {
                        item = new myRow(Integer.toString(i) + ". Stunde:", "", "", "", "");
                    }
                }
                else {
                    item = new myRow(Integer.toString(i) + ". Stunde:", "", "", "", "");
                }

                lTime.add(item);
            }
        }
        else {
            for (int i = int1; i <= int2; i++) {
                item = new myRow(Integer.toString(i) + ". Stunde:", "", "", "", "");
                lTime.add(item);
            }
        }

        // Actionbar Title setzen
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle("Unterrichtszeiten");
        }

        // ListAdapter setzen
        lvTimes.setAdapter(new TimeAdapter(this, R.layout.page_edittimes, lTime));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed(); return true;
    }


    private void saveEditTimes(){
        // Deklarationen
        DS_TimeEdit dsEdit;
        View v1;
        EditText edtFromHour;
        EditText edtFromMin;
        EditText edtToHour;
        EditText edtToMin;
        TextView txtHour;

        for(int i = 0; i < lvTimes.getChildCount(); i++){
            // Initialisierung
            dsEdit          = new DS_TimeEdit();
            v1              = lvTimes.getChildAt(i);
            txtHour         = (TextView) v1.findViewWithTag(R.id.tv_hour);
            edtFromHour     = (EditText) v1.findViewWithTag(R.id.et_fromhour);
            edtFromMin      = (EditText) v1.findViewWithTag(R.id.et_frommin);
            edtToHour       = (EditText) v1.findViewWithTag(R.id.et_tohour);
            edtToMin        = (EditText) v1.findViewWithTag(R.id.et_tomin);

            // DS fuellen
            dsEdit.setHour(txtHour.getText().subSequence(0, txtHour.getText().toString().indexOf(".")).toString());

            if(edtFromHour.getText().toString().isEmpty() || edtFromMin.getText().toString().isEmpty())
                dsEdit.setFromTime("");
            else
                dsEdit.setFromTime(edtFromHour.getText().toString() + ":" + edtFromMin.getText().toString());

            if(edtToHour.getText().toString().isEmpty() || edtToMin.getText().toString().isEmpty())
                dsEdit.setToTime("");
            else
                dsEdit.setToTime(edtToHour.getText().toString() + ":" + edtToMin.getText().toString());


            // DS der Datenbankhinzufuegn
            ds.EDITTIMES_UpdateOrCreateEntry(dsEdit);
        }
    }


    private void clearEditTimes(){
        // Deklarationen
        View v1;

        for(int i = 0; i < lvTimes.getChildCount(); i++){
            // Initialisierung
            v1              = lvTimes.getChildAt(i);

            // EditTextes leeren
            ((EditText) v1.findViewWithTag(R.id.et_fromhour)).setText("");
            ((EditText) v1.findViewWithTag(R.id.et_frommin)).setText("");
            ((EditText) v1.findViewWithTag(R.id.et_tohour)).setText("");
            ((EditText) v1.findViewWithTag(R.id.et_tomin)).setText("");
        }
    }


    private class TimeAdapter extends ArrayAdapter<myRow> {
        private ArrayList<myRow> times;
        private int layoutResourceId;
        private Context context;

        TimeAdapter(Context context, int layoutResourceId, ArrayList<myRow> times) {
            super(context, layoutResourceId, times);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.times = times;
        }

        @NonNull
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // Deklaration
            View row;


            // Initialisierung
            LayoutInflater inflater     = ((Activity) context).getLayoutInflater();
            final rowHolder holder      = new rowHolder();
            row                         = inflater.inflate(layoutResourceId, parent, false);

            // Holderitem beziehen
            holder.item = times.get(position);

            // Holder Objecte setzen
            holder.titel        = (TextView) row.findViewById(R.id.tv_hour);
            holder.from_hour    = (EditText) row.findViewById(R.id.et_fromhour);
            holder.from_min     = (EditText) row.findViewById(R.id.et_frommin);
            holder.to_hour      = (EditText) row.findViewById(R.id.et_tohour);
            holder.to_min       = (EditText) row.findViewById(R.id.et_tomin);

            // ???
            row.setTag(holder);
            setupItem(holder);

            // ???
            holder.titel.setId(position);
            holder.from_hour.setId(position);
            holder.from_min.setId(position);
            holder.to_hour.setId(position);
            holder.to_min.setId(position);

            // TODO
            holder.titel.setTag(R.id.tv_hour);
            holder.from_hour.setTag(R.id.et_fromhour);
            holder.from_min.setTag(R.id.et_frommin);
            holder.to_hour.setTag(R.id.et_tohour);
            holder.to_min.setTag(R.id.et_tomin);

            // OnFocusChangeListener setzen
            holder.titel.setOnFocusChangeListener( new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        final int id = v.getId();
                        final TextView field = ((TextView) v);
                        times.get(id).set_titel(field.getText().toString());
                    }
                }
            });
            holder.from_hour.setOnFocusChangeListener( new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        final int id = v.getId();
                        final EditText field = ((EditText) v);
                        times.get(id).set_from_hour(field.getText().toString());
                    }
                }
            });
            holder.from_min.setOnFocusChangeListener( new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        final int id = v.getId();
                        final EditText field = ((EditText) v);
                        times.get(id).set_from_min(field.getText().toString());
                    }
                }
            });
            holder.to_hour.setOnFocusChangeListener( new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        final int id = v.getId();
                        final EditText field = ((EditText) v);
                        times.get(id).set_to_hour(field.getText().toString());
                    }
                }
            });
            holder.to_min.setOnFocusChangeListener( new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        final int id = v.getId();
                        final EditText field = ((EditText) v);
                        times.get(id).set_to_min(field.getText().toString());
                    }
                }
            });

            // TextChangeListener setzen
            holder.from_hour.addTextChangedListener(new skipField(holder.from_hour, holder.from_min));
            holder.from_min.addTextChangedListener(new skipField(holder.from_min, holder.to_hour));
            holder.to_hour.addTextChangedListener(new skipField(holder.to_hour, holder.to_min));

            // Filter setzen
            holder.from_hour.setFilters(new InputFilter[]{hourFilter});
            holder.from_min.setFilters(new InputFilter[]{minFilter});
            holder.to_hour.setFilters(new InputFilter[]{hourFilter});
            holder.to_min.setFilters(new InputFilter[]{minFilter});

            return row;
        }

        private void setupItem(rowHolder holder) {
            holder.titel.setText(holder.item.get_titel());
            holder.from_hour.setText(holder.item.get_from_hour());
            holder.from_min.setText(holder.item.get_from_min());
            holder.to_hour.setText(holder.item.get_to_hour());
            holder.to_min.setText(holder.item.get_to_min());
        }

        class rowHolder {
            myRow item;
            TextView titel;
            EditText from_hour;
            EditText from_min;
            EditText to_hour;
            EditText to_min;
        }
    }


    private class skipField implements TextWatcher{
        EditText edt;
        EditText edtNext;
        String sText = "";

        skipField(EditText edt1, EditText edt2){
            edt = edt1;
            edtNext = edt2;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(edt.length() == 2) {
                edt.clearFocus();
                edtNext.requestFocus();
                edtNext.setCursorVisible(true);
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if(s.length() == 2)
                sText = s.toString();
        }

        public void afterTextChanged(Editable s) {
            if(s.length() > 2) {
                edt.setText(sText);
                sText = "";
            }
        }
    }

    private class myRow {
        private String titel;
        private String from_hour;
        private String from_min;
        private String to_hour;
        private String to_min;


        myRow(String titel, String from_hour, String from_min, String to_hour, String to_min)
        {
            this.titel = titel;
            this.from_hour = from_hour;
            this.from_min = from_min;
            this.to_hour = to_hour;
            this.to_min = to_min;
        }

        String get_titel() {
            return titel;
        }
        void set_titel(String titel) {
            this.titel = titel;
        }


        String get_from_hour() {
            return from_hour;
        }
        void set_from_hour(String from_hour) {
            this.from_hour = from_hour;
        }


        String get_from_min() {
            return from_min;
        }
        void set_from_min(String from_min) {
            this.from_min = from_min;
        }


        String get_to_hour() {
            return to_hour;
        }
        void set_to_hour(String to_hour) {
            this.to_hour = to_hour;
        }


        String get_to_min() {
            return to_min;
        }
        void set_to_min(String to_min) {
            this.to_min = to_min;
        }

    }
}