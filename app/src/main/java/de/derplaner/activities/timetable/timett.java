package de.derplaner.activities.timetable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

import de.derplaner.R;
import de.derplaner.SQLite.SQL_PlanerDataSource;
import de.derplaner.activities.widget.WidgetProvider1;
import de.derplaner.another.constant;
import de.derplaner.dataObjects.DS_Hour;
import de.derplaner.dataObjects.DS_TimeEdit;


public class timett extends Fragment {
    private GridView gvTime;
    private GridView gvHour;
    private ArrayList<DS_TimeEdit> lTime = new ArrayList<>();
    private ArrayList<DS_Hour> lHour = new ArrayList<>();
    private ArrayList<Integer> lFilter = new ArrayList<>();
    private ArrayList<Integer> lSave = new ArrayList<>();
    private SQL_PlanerDataSource ds;
    private int lastPos = 0;
    private Integer int1;
    private Integer int2;
    private int btnColor = Color.WHITE;

    private TextView txtWeek;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.onSaveInstanceState(savedInstanceState);
        // Deklaration
        final View rootView;
        String key;
        Calendar cal = Calendar.getInstance();

        // Initialisierung
        rootView = inflater.inflate(R.layout.act_timetable, container, false);
        gvTime = (GridView) rootView.findViewById(R.id.gvTime);
        gvHour = (GridView) rootView.findViewById(R.id.gvHour);
        txtWeek = (TextView) rootView.findViewById(R.id.tvWeek);
        ds = new SQL_PlanerDataSource(getActivity());

        String sSetting = ds.SETTINGS_getOneEntries(constant.SET_TTWeek);

        // Variablen
        key = getArguments().getString("KEY");

        // Erste und Letzte Stunde auslesen
        int1 = Integer.parseInt(ds.SETTINGS_getOneEntries("FirstHour"));
        int2 = Integer.parseInt(ds.SETTINGS_getOneEntries("LastHour"));

        //TODO
        update("");

        // Setzen der Wochensektion
        if(sSetting.equals(constant.TTWEEK[1])) {
            if(cal.get(Calendar.WEEK_OF_YEAR)%2 ==0)
                txtWeek.setText("G");
            else
                txtWeek.setText("U");
        } // TODO Verbesserung
        else if(sSetting.equals(constant.TTWEEK[2])) {
            txtWeek.setText("A");
        }

        // Scrollen des TimeGrids verhindern
        gvTime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return event.getAction() == MotionEvent.ACTION_MOVE;
            }
        });

        txtWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tvWeek;
                String sSetting = ds.SETTINGS_getOneEntries(constant.SET_TTWeek);

                if(sSetting.equals(constant.TTWEEK[1])) {
                    tvWeek = (TextView) v;
                    switch (tvWeek.getText().toString()){
                        case "G":
                            tvWeek.setText("U");
                            update(tvWeek.getText().toString());
                            break;
                        case "U":
                            tvWeek.setText("G");
                            update(tvWeek.getText().toString());
                            break;
                    }
                }
                else if(sSetting.equals(constant.TTWEEK[2])) {
                    // TODO
                }
            }
        });

        // Synchronisieren der GrirdViews gvHour und gvTime
        gvHour.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                // Scrollen synchronisieren
                if (view.getChildCount() != 0) {
                    int ie;

                    // Scrolldistance berechnen
                    if (lastPos != (firstVisibleItem / 5))
                        ie = -((view.getChildAt(0).getTop()) - ((gvTime.getChildAt(0).getTop())
                                + (view.getChildAt(0).getHeight()) * ((firstVisibleItem / 5) - lastPos)));
                    else
                        ie = -(view.getChildAt(0).getTop() - (gvTime.getChildAt(0).getTop()));

                    // Scollen das anderen Grids
                    gvTime.smoothScrollBy(ie, 0);
                }
            }
        });

        // OnScrollListener gvTime setzen
        gvTime.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                // Letzte Position dieses Grids setzen
                if (view.getChildCount() != 0) {
                    lastPos = firstVisibleItem;
                }
            }
        });

        // Layout visible
        if(key != null) {
            switch (key)
            {
                case "ADD":
                    add(rootView, new myHour());
                    break;
                case "EDIT":
                    edit(rootView);
                    break;
                default:
                    start(rootView);
                    break;
            }
        }

        return rootView;
    }

    private void start(final View rootView)
    {
        // OnItemClickListener setzen TODO
        gvHour.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showInputDialog(position, rootView);
            }
        });
    }

    private void add(final View rootView, final myHour curHour)
    {
        // Deklaration
        DS_Hour hour;
        Drawable dw;

        // Initialisierung
        EditText ed_subject = (EditText) rootView.findViewById(R.id.ed_subject);
        EditText ed_teacher = (EditText) rootView.findViewById(R.id.ed_teacher);
        EditText ed_room    = (EditText) rootView.findViewById(R.id.ed_room);
        ds = new SQL_PlanerDataSource(getActivity());
        btnColor = curHour.get_hour().getColor();


        // Erste und Letzte Stunde auslesen
        int1 = Integer.parseInt(ds.SETTINGS_getOneEntries("FirstHour"));
        int2 = Integer.parseInt(ds.SETTINGS_getOneEntries("LastHour"));

        // Edittext setzen
        ed_subject.setText(curHour.get_hour().getSubject());
        ed_teacher.setText(curHour.get_hour().getTeacher());
        ed_room.setText(curHour.get_hour().getRoom());

        // Final Variablen
        final LayoutInflater inflat = LayoutInflater.from(this.getActivity());
        final ArrayList<Integer> lList = new ArrayList<>();
        final Button btn1 = (Button) rootView.findViewById(R.id.btn_color);
        final Button btn2 = (Button) rootView.findViewById(R.id.btn_save);

        //???
        dw = getContext().getResources().getDrawable(R.drawable.round_button);
        dw.mutate().setColorFilter(btnColor, PorterDuff.Mode.MULTIPLY);
        // TODO new
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
            setBackground(btn1, dw);
        else
            setBackground16(btn1, dw);

        // Liste mit den TV und Border Booleans fuellen
        // TODO lTT = orderTT(lHour);

        for (DS_Hour item: lHour)
        {
            if (item.getSubject().equals(curHour.get_hour().getSubject()) &&
                    item.getTeacher().equals(curHour.get_hour().getTeacher()) &&
                    item.getRoom().equals(curHour.get_hour().getRoom()) &&
                    item.getColor() == curHour.get_hour().getColor() &&
                    !curHour.get_hour().getSubject().equals("") &&
                    !curHour.get_hour().getTeacher().equals("") &&
                    !curHour.get_hour().getRoom().equals("")) {
                item.setShort("");
                item.setTeacher("");
                item.setRoom("");
                int test = (item.getHour()-int1)*5 + item.getDay();
                lSave.add(test);
                item.setColor(Color.GREEN);
            } else if (!item.getSubject().equals("") ||
                    !item.getTeacher().equals("") ||
                    !item.getRoom().equals("")) {
                item.setShort("");
                item.setTeacher("");
                item.setRoom("");
                item.setColor(Color.GRAY);
            }

        }

        // Liste fuer das Erstellen des Colorgrids
        for (int i = 0; i < 9; i++) {
            lList.add(i);
        }

        // Layout visible
        rootView.findViewById(R.id.timetable_buttom).setVisibility(View.VISIBLE);

        // OnClickListener setzen fuer den Color Btn
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Final Variablen
                final AlertDialog.Builder adbColor;
                final AlertDialog adColor;

                // Initialisierung
                View v1 = inflat.inflate(R.layout.ad_color, null);
                GridView gvTest = (GridView) v1.findViewById(R.id.gridColor);

                // GV Adapter setzen
                gvTest.setAdapter(new ColorAdapter(getActivity(), lList));

                // Color AD deklarieren
                adbColor = new AlertDialog.Builder(inflat.getContext())
                        .setView(v1)
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialog2, int keyCode, KeyEvent event) {
                                if (keyCode == KeyEvent.KEYCODE_BACK)
                                    dialog2.dismiss();

                                return true;
                            }
                        });

                // Color AD erstellen
                adColor = adbColor.create();

                // OnClickListener setzen fuer das Color Grid
                gvTest.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // Deklaration
                        Integer color = lFilter.get(position);

                        // Buttonfarbe setzen und Grid Schliessen
                        Drawable dw = getContext().getResources().getDrawable(R.drawable.round_button);
                        dw.mutate().setColorFilter(color, PorterDuff.Mode.MULTIPLY);
                        btnColor = color;
                        // TODO new
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                            setBackground(btn1, dw);
                        else
                            setBackground16(btn1, dw);
                        adColor.dismiss();
                    }
                });

                // AD ColorGrid anzeigen
                adColor.show();

                // TODO bearbeiten - Breite spezifischer setzen
                adColor.getWindow().setLayout(700, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveHour(rootView);
            }
        });



        // OnItemClickListener setzen TODO
        gvHour.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Drawable dw = getContext().getResources().getDrawable(R.drawable.border);

                if(lSave.indexOf(position) >= 0) {
                    lSave.remove(lSave.indexOf(position));

                    if(!lHour.get(position).getSubject().trim().equals(""))
                        lHour.get(position).setColor(Color.GRAY);
                    else
                        lHour.get(position).setColor(Color.parseColor("#EEEEEE"));

                    dw.mutate().setColorFilter(lHour.get(position).getColor(), PorterDuff.Mode.MULTIPLY);

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                        setBackground(view, dw);
                    else
                        setBackground16(view, dw);
                }
                else {
                    lSave.add(position);
                    lHour.get(position).setColor(Color.GREEN);
                    dw.mutate().setColorFilter(lHour.get(position).getColor(), PorterDuff.Mode.MULTIPLY);

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                        setBackground(view, dw);
                    else
                        setBackground16(view, dw);
                }
            }
        });

        gvHour.setAdapter(new HourAdapter(getActivity(), lHour));
    }

    private void edit(final View rootView)
    {
        //TODO also need
        // Deklaration
        final AlertDialog.Builder builder;
        final AlertDialog alert;
        LayoutInflater inflater;
        View promptView;

        // Initialisierung
        inflater = LayoutInflater.from(rootView.getContext());
        promptView  = inflater.inflate(R.layout.scv_timetable, null);

        Button btn2 = (Button) promptView.findViewById(R.id.btn_deletehour);


        //TODO
        final ArrayList<myHour> lHour = new ArrayList<>();
        ArrayList<DS_Hour> lHours = ds.getUniqeEntries_HOURS("0");

        for (DS_Hour item : lHours) {
            myHour mH = new myHour(item, false);
            lHour.add(mH);
        }

        final ListView lvHour = (ListView) promptView.findViewById(R.id.lv_hours);




        // AD deklarieren
        builder = new AlertDialog.Builder(inflater.getContext())
                .setView(promptView)
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK)
                            dialog.dismiss();
                        return true;
                    }
                });

        // AD erstellen
        alert = builder.create();


        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (myHour item: lHour) {
                    item.btn = true;
                }

                lvHour.setAdapter(new EditHour(getActivity(), R.layout.page_deletehour, lHour));
            }
        });


        lvHour.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                myHour test = lHour.get(position);
                add(rootView, test);
                alert.dismiss();
            }
        });




        // Background tranparent setzen
        if (alert.getWindow() != null)
            alert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));



        lvHour.setAdapter(new EditHour(getActivity(), R.layout.page_deletehour, lHour));

        // AD Stunde bearbeiten anzeigen
        alert.show();
    }


    private void update(String sWeek){
        DS_Hour hour;
        DS_TimeEdit editTime;

        // TimeAdapter mit den Stunden fuellen
        lTime = ds.getAllEntries_EDITTIMES();

        // HourAdampter mit leeren befuellen
        // TODO childid setzen
        for (int i = 0; i < (int2 - int1 + 1) * 5; i++) {
            hour = new DS_Hour();
            hour.setDay(i%5);
            hour.setHour(i/5+int1);
            lHour.add(hour);
        }

        // Liste ergaenzen falls etwas zu wenig ist
        if(lTime.size() != (int2-int1+1)) {
            for (int i = int1; i <= int2; i++) {
                editTime = new DS_TimeEdit();
                editTime.setHour(Integer.toString(i));
                editTime.setFromTime("");
                editTime.setToTime("");
                lTime.add(editTime);
            }
        }

        // Liste mit den vorhandenen DS ersetzen
        if(sWeek.isEmpty())
            lHour = ds.getAllEntries_HOURS_forTT("0", int1, lHour);
        else
            lHour = ds.getAllEntries_HOURS("", sWeek);

        // GridAdapter setzen
        gvTime.setAdapter(new TimeAdapter(getActivity(), lTime));
        gvHour.setAdapter(new HourAdapter(getActivity(), lHour));
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state

// Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }


    // Adapter fuer das GridView gvTime
    private class TimeAdapter extends ArrayAdapter<DS_TimeEdit> {
        private ArrayList<DS_TimeEdit> lTimes = new ArrayList<>();
        private LayoutInflater inflater;

        // Konstruktor
        TimeAdapter(Context context, ArrayList<DS_TimeEdit> times) {
            super(context, R.layout.page_timetable_time, times);
            this.lTimes = times;
            inflater = LayoutInflater.from(context);
        }

        @Override
        @NonNull
        public View getView(int position, View view,@NonNull ViewGroup parent) {
            if (view == null)
                view = inflater.inflate(R.layout.page_timetable_time, parent, false);

            // Deklaration
            Drawable dw;

            // Initialisierung
            TextView tvFromTime = (TextView) view.findViewById(R.id.tvFromTime);
            TextView tvHour = (TextView) view.findViewById(R.id.tvHour);
            TextView tvToTime = (TextView) view.findViewById(R.id.tvToTime);

            // TV Settings setzen
            tvHour.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
            tvHour.setTextColor(Color.BLACK);

            // TVs Text setzen
            tvFromTime.setText(lTimes.get(position).getFromTime());
            tvHour.setText(lTimes.get(position).getHour());
            tvToTime.setText(lTimes.get(position).getToTime());

            // Hintergrundsettings setzen
            dw = getContext().getResources().getDrawable(R.drawable.border);
            dw.mutate().setColorFilter(Color.parseColor("#EEEEEE"), PorterDuff.Mode.MULTIPLY);

            // Hintergrund dem View uebergeben
            // TODO new
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                setBackground(view, dw);
            else
                setBackground16(view, dw);

            return view;
        }
    }


    // Adapter fuer das GridView gvHour
    private class HourAdapter extends ArrayAdapter<DS_Hour> {
        private ArrayList<DS_Hour> lHours = new ArrayList<>();
        private LayoutInflater inflater;

        // Konstruktor
        private HourAdapter(Context context, ArrayList<DS_Hour> times) {
            super(context, R.layout.page_timetable_hour, times);
            this.lHours = times;
            inflater = LayoutInflater.from(context);
        }

        @Override
        @NonNull
        public View getView(int position, View view,@NonNull ViewGroup parent) {
            if (view == null)
                view = inflater.inflate(R.layout.page_timetable_hour, parent, false);

            // Deklaration
            Drawable dw;

            // Initialisierung
            DS_Hour hour = lHours.get(position);
            TextView tvTop = (TextView) view.findViewById(R.id.tvTop);
            TextView tvCenterTop = (TextView) view.findViewById(R.id.tvCenterTop);
            TextView tvCenterBottum = (TextView) view.findViewById(R.id.tvCenterBottum);
            TextView tvBottum = (TextView) view.findViewById(R.id.tvBottum);

            // Texte initialisieren
            tvTop.setText("");
            tvCenterTop.setText("");
            tvCenterBottum.setText("");
            tvBottum.setText("");

            // Borderhintergrund waehlen setzen
            switch (hour.getBorder())
            {
                case "TB":
                    dw = getContext().getResources().getDrawable(R.drawable.border_no_tb);
                    break;
                case "T":
                    dw = getContext().getResources().getDrawable(R.drawable.border_no_t);
                    break;
                case "B":
                    dw = getContext().getResources().getDrawable(R.drawable.border_no_b);
                    break;
                default:
                    dw = getContext().getResources().getDrawable(R.drawable.border);
                    break;
            }

            // TVs Text setzen
            switch (hour.getTV())
            {
                case "T":
                    tvTop.setText(hour.getRoom());
                    break;
                case "B":
                    tvBottum.setText(hour.getShort());
                    break;
                case "CENTER":
                    tvCenterTop.setText(hour.getShort());
                    tvCenterBottum.setText(hour.getRoom());
                    break;
            }

            // Hintergrund Settings setzen
            if (hour.getColor() != 0)
                dw.mutate().setColorFilter(hour.getColor(), PorterDuff.Mode.MULTIPLY);
            else
                dw.mutate().setColorFilter(Color.parseColor("#EEEEEE"), PorterDuff.Mode.MULTIPLY);

            // Hintergrund dem View uebergeben
            // TODO new
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                setBackground(view, dw);
            else
                setBackground16(view, dw);

            return view;
        }
    }

    @SuppressWarnings("deprecation")
    private void setBackground(View view, Drawable dw){
        view.setBackgroundDrawable(dw);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setBackground16(View view, Drawable dw){
        view.setBackground(dw);
    }


    // AD für das Stunde bearbeiten anzeigen
    private void showInputDialog(final int iPos, View rootView) {
        // Deklaration
        final DS_Hour dsTime = lHour.get(iPos);

        // Initialisierung
        TextView et1 = (TextView) rootView.findViewById(R.id.ed_subject2);
        TextView et2 = (TextView) rootView.findViewById(R.id.ed_teacher2);
        TextView et3 = (TextView) rootView.findViewById(R.id.ed_room2);
        Button btn1 = (Button) rootView.findViewById(R.id.btn_color2);

        if(!dsTime.getSubject().trim().isEmpty())
            rootView.findViewById(R.id.timetable_buttomtv).setVisibility(View.VISIBLE);
        else {
            rootView.findViewById(R.id.timetable_buttomtv).setVisibility(View.GONE);
            rootView.findViewById(R.id.timetable_buttom).setVisibility(View.GONE);
        }



        // ETs Text setzen
        et1.setText(dsTime.getSubject());
        et2.setText(dsTime.getTeacher());
        et3.setText(dsTime.getRoom());

        // Hintergrundfarbe des Button setzen
        if (dsTime.getColor() != 0) {
            // Hintergrundsettings setzen
            Drawable dw = getContext().getResources().getDrawable(R.drawable.round_button);
            dw.mutate().setColorFilter(dsTime.getColor(), PorterDuff.Mode.MULTIPLY);
            // Hiintergrund dem View uebergeben
            // TODO new
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                setBackground(btn1, dw);
            else
                setBackground16(btn1, dw);
        }
    }


    //  DS speichern
    private void saveHour(View v1) {
        // Deklaration
        ArrayList<Integer> lUpdate = new ArrayList<>();
        String sInDays;
        DS_Hour dsHour;
        int hour;
        int day;

        // Initialisierung
        EditText edt1 = (EditText) v1.findViewById(R.id.ed_subject);
        EditText edt2 = (EditText) v1.findViewById(R.id.ed_teacher);
        EditText edt3 = (EditText) v1.findViewById(R.id.ed_room);

        for (Integer item: lSave) {
            dsHour = new DS_Hour();

            // Stunde und Tag herrausfinden
            hour = item / 5 + int1;
            day = item % 5;

            // Eingenschaften des DS Setzen
            dsHour.setChildId(0);
            dsHour.setSubject(edt1.getText().toString());
            dsHour.setShort(edt1.getText().toString().substring(0, 3));
            dsHour.setRoom(edt3.getText().toString());
            dsHour.setTeacher(edt2.getText().toString());
            dsHour.setHour(hour);
            dsHour.setDay(day);
            dsHour.setColor(btnColor);
            dsHour.setTV("CENTER");
            dsHour.setWeek(txtWeek.getText().toString());

            // DS der DB hinzufuegen
            // todo ersetzen mit replace in lhour
            ds.HOUR_UpdateOrCreateEntry(dsHour);

            // ??
            if(lUpdate.indexOf(day) < 0)
                lUpdate.add(day);
        }

        // Where Bedingung IN DAYS bauen
        sInDays = "(" + lUpdate.get(0);
        for (int i = 1; i < lUpdate.size(); i++)
        {
            sInDays += ", " + lUpdate.get(i);
        }
        sInDays += ")";

        // TODO
        ArrayList<DS_Hour> alHours = ds.getAllEntries_HOURS("0", int1, sInDays);
        orderTT(alHours);

        // TODO new
        /*AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.getActivity());
        int appWidgetIds[] = appWidgetManager.getAppWidgetIds(
                new ComponentName(this.getActivity(), WidgetProvider1.class));
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.lv1);*/


        Intent intent = new Intent(this.getActivity(), WidgetProvider1.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
// Use an array and EXTRA_APPWIDGET_IDS instead of AppWidgetManager.EXTRA_APPWIDGET_ID,
// since it seems the onUpdate() is only fired on that:
        int ids[] = AppWidgetManager.getInstance(this.getActivity().getApplication()).getAppWidgetIds(new ComponentName(this.getActivity().getApplication(), WidgetProvider1.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        this.getActivity().sendBroadcast(intent);
}


    // Adapter fuer das GridView der Farben
    private class ColorAdapter extends ArrayAdapter<Integer> {
        private ArrayList<Integer> useless = new ArrayList<>();
        private LayoutInflater inflater;

        // Konstruktor
        ColorAdapter(Context context, ArrayList<Integer> useless) {
            super(context, R.layout.page_timetable_hour, useless);
            this.useless = useless;
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, View view,@NonNull ViewGroup parent) {
            if (view == null)
                view = inflater.inflate(R.layout.page_one_text, parent, false);

            // Deklaration
            Drawable dw;

            // Initialisierung
            TextView tvOneTXT = (TextView)view.findViewById(R.id.tvOneTXT);

            // Textfarbe und Text setzen
            tvOneTXT.setTextColor(Color.BLACK);
            tvOneTXT.setText("Text");

            // Hintergrundsettings setzen
            dw = getContext().getResources().getDrawable(R.drawable.border);

            // Entscheiden welche Hintergrundfarbe gesetzt werden soll
            // und ob diese der Liste hinzugefuegt wird
            switch (position)
            {
                case 0:
                    dw.mutate().setColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY);
                    if(lFilter.size() == 0)
                        lFilter.add(0, Color.GREEN);
                    break;
                case 1:
                    dw.mutate().setColorFilter(Color.BLUE, PorterDuff.Mode.MULTIPLY);
                    if(lFilter.size() == 1)
                        lFilter.add(1, Color.BLUE);
                    break;
                case 2:
                    dw.mutate().setColorFilter(Color.CYAN, PorterDuff.Mode.MULTIPLY);
                    if(lFilter.size() == 2)
                        lFilter.add(2, Color.CYAN);
                    break;
                case 3:
                    dw.mutate().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                    if(lFilter.size() == 3)
                        lFilter.add(3, Color.GRAY);
                    break;
                case 4:
                    dw.mutate().setColorFilter(Color.MAGENTA, PorterDuff.Mode.MULTIPLY);
                    if(lFilter.size() == 4)
                    lFilter.add(4, Color.MAGENTA);
                    break;
                case 5:
                    dw.mutate().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
                    if(lFilter.size() == 5)
                        lFilter.add(5, Color.RED);
                    break;
                case 6:
                    dw.mutate().setColorFilter(Color.LTGRAY, PorterDuff.Mode.MULTIPLY);
                    if(lFilter.size() == 6)
                        lFilter.add(6, Color.LTGRAY);
                    break;
                case 7:
                    dw.mutate().setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY);
                    if(lFilter.size() == 7)
                        lFilter.add(7, Color.YELLOW);
                    break;
                case 8:
                    dw.mutate().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
                    if(lFilter.size() == 8)
                        lFilter.add(8, Color.WHITE);
                    break;
            }

            // Hiintergrund dem View uebergeben
            // TODO new
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                setBackground(view, dw);
            else
                setBackground16(view, dw);

            return view;
        }
    }


    // Sortierung der Border- & TV Settings
    private void orderTT(ArrayList<DS_Hour> lHours) {
        int[] iAnzahl = {0,0,0,0,0};
        DS_Hour currHour;
        DS_Hour lastHour;
        DS_Hour nextHour;

        for (int i = 0; i < lHours.size(); i++) {
            nextHour = new DS_Hour();
            lastHour = new DS_Hour();

            currHour = lHours.get(i);
            int day = lHours.get(i).getDay();

            // TODO NEW
            if(currHour.getSubject().isEmpty() && currHour.getTeacher().isEmpty() &&
                    currHour.getRoom().isEmpty())
                continue;

            if(i > 0 && lHours.get(i).getDay() == lHours.get(i-1).getDay())
                lastHour = lHours.get(i-1);

            if(i < lHours.size()-1 && lHours.get(i).getDay() == lHours.get(i+1).getDay())
                nextHour = lHours.get(i+1);


            if(nextHour != null && lastHour != null) {
                if (currHour.getSubject().equals(nextHour.getSubject()) &&
                        currHour.getTeacher().equals(nextHour.getTeacher()) &&
                        currHour.getRoom().equals(nextHour.getRoom()) &&
                        currHour.getColor() == nextHour.getColor() &&
                        currHour.getDay() == nextHour.getDay() &&
                        currHour.getSubject().equals(lastHour.getSubject()) &&
                        currHour.getTeacher().equals(lastHour.getTeacher())&&
                        currHour.getRoom().equals(lastHour.getRoom()) &&
                        currHour.getColor() == lastHour.getColor() &&
                        currHour.getDay() == lastHour.getDay()) {
                    // Border Boolean setzen
                    lHours.get(i).setBorder("TB");
                    iAnzahl[day]++;
                }
                else if (currHour.getSubject().equals(lastHour.getSubject()) &&
                        currHour.getTeacher().equals(lastHour.getTeacher())&&
                        currHour.getRoom().equals(lastHour.getRoom()) &&
                        currHour.getColor() == lastHour.getColor() &&
                        currHour.getDay() == lastHour.getDay()) {
                    // Border Boolean setzen
                    lHours.get(i).setBorder("T");
                    iAnzahl[day]++;

                    // Tv Booleans setzen
                    if(iAnzahl[day] == 2) {
                        // TODO
                        lHours.get(i-1).setBorder("B");
                        lHours.get(i-1).setTV("B");
                        lHours.get(i).setTV("T");
                    }
                    else if (iAnzahl[day]%2==1) {
                        lHours.get(i - (iAnzahl[day]/2)).setTV("CENTER");
                    }
                    else if (iAnzahl[day]%2==0) {
                        lHours.get(i - (iAnzahl[day]/2)).setTV("B");
                        lHours.get(i - (iAnzahl[day]/2-1)).setTV("T");
                    }

                    iAnzahl[day] = 1;
                }
                else if (currHour.getSubject().equals(nextHour.getSubject()) &&
                        currHour.getTeacher().equals(nextHour.getTeacher()) &&
                        currHour.getRoom().equals(nextHour.getRoom()) &&
                        currHour.getColor() == nextHour.getColor() &&
                        currHour.getDay() == nextHour.getDay()
                        ) {
                    // Border Boolean setzen
                    lHours.get(i).setBorder("B");
                    iAnzahl[day]++;
                }
                else {
                    lHours.get(i).setTV("CENTER");
                }
            }
            else if(nextHour != null) {
                if (currHour.getSubject().equals(nextHour.getSubject()) &&
                        currHour.getTeacher().equals(nextHour.getTeacher()) &&
                        currHour.getRoom().equals(nextHour.getRoom()) &&
                        currHour.getColor() == nextHour.getColor() &&
                        currHour.getDay() == nextHour.getDay()) {
                    // Border Boolean setzen
                    lHours.get(i).setBorder("B");
                    iAnzahl[day]++;
                }
                else {
                    lHours.get(i).setTV("CENTER");
                }
            }
            else if(lastHour != null) {
                if (currHour.getSubject().equals(lastHour.getSubject()) &&
                        currHour.getTeacher().equals(lastHour.getTeacher())&&
                        currHour.getRoom().equals(lastHour.getRoom()) &&
                        currHour.getColor() == lastHour.getColor() &&
                        currHour.getDay() == lastHour.getDay()) {
                    // Border Boolean setzen
                    lHours.get(i).setBorder("T");
                    iAnzahl[day]++;

                    // Tv Booleans setzen
                    if(iAnzahl[day] == 2) {
                        lHours.get(i).setTV("T");
                        lHours.get(i-1).setBorder("B");
                    }
                    else if (iAnzahl[day]%2==1) {
                        lHours.get(lHours.size()-(iAnzahl[day]/2)).setTV("CENTER");
                    }
                    else if (iAnzahl[day]%2==0) {
                        lHours.get(i - (iAnzahl[day]/2)).setTV("B");
                        lHours.get(i - (iAnzahl[day]/2-1)).setTV("T");
                    }

                    iAnzahl[day] = 1;
                }
                else {
                    lHours.get(i).setTV("CENTER");
                }
            }
            else {
                lHours.get(i).setTV("CENTER");
            }
        }


        // TODO ueberprüfen
        for (DS_Hour item: lHours) {
            ds.HOUR_UpdateOrCreateEntry(item);
        }
    }



    private class EditHour extends ArrayAdapter<myHour> {
        private ArrayList<myHour> myHours;
        private int layoutResourceId;
        private Context context;

        EditHour(Context context, int layoutResourceId, ArrayList<myHour> myHours) {
            super(context, layoutResourceId, myHours);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.myHours = myHours;
        }

        @Override
        @NonNull
        public View getView(final int position, View convertView,@NonNull ViewGroup parent) {
            // Deklaration
            View row;
            Drawable dw;


            // Initialisierung
            LayoutInflater inflater             = ((Activity) context).getLayoutInflater();
            final rowHolder holder              = new rowHolder();
            row                                 = inflater.inflate(layoutResourceId, parent, false);

            // Holderitem beziehen
            holder.item = myHours.get(position);

            // Holder Objecte setzen
            holder.text     = (TextView) row.findViewById(R.id.tv_edithour);
            holder.btn      = (Button) row.findViewById(R.id.btn_edithour);

            // ???
            row.setTag(holder);
            setupItem(holder);

            // Hintergrund Settings setzen
            dw = getContext().getResources().getDrawable(R.drawable.border);
            dw.mutate().setColorFilter(holder.item.hour.getColor(), PorterDuff.Mode.MULTIPLY);

            // Hintergrund dem View uebergeben
            // TODO new
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                setBackground(row, dw);
            else
                setBackground16(row, dw);


            // ???
            holder.text.setId(position);
            holder.btn.setId(position);

            // ???
            holder.text.setTag(R.id.tv_edithour);
            holder.btn.setTag(R.id.btn_edithour);

            return row;
        }

        private void setupItem(rowHolder holder) {
            holder.text.setText(holder.item.get_hour().getSubject());
            if(holder.item.get_btn())
                holder.btn.setVisibility(Button.VISIBLE);
            else
                holder.btn.setVisibility(Button.GONE);
        }

        class rowHolder {
            myHour item;
            TextView text;
            Button btn;
        }
    }


    private class myHour {
        private DS_Hour hour = new DS_Hour();
        private Boolean btn = false;


        myHour(DS_Hour hour, Boolean btn)
        {
            this.hour = hour;
            this.btn = btn;
        }

        myHour(){};

        DS_Hour get_hour(){ return hour; }
        void set_hour(DS_Hour hour) { this.hour = hour; }

        Boolean get_btn() {
            return btn;
        }
        void set_btn(Boolean btn) {
            this.btn = btn;
        }
    }


    // Datensatzklasse fuer die Sortierung der TT Stunden
    private class DS_TT{
        DS_Hour dsHour;
        boolean bTvTop           = false;
        boolean bTvCenter        = false;
        boolean bTvButtom        = false;
        boolean bBorTop          = false;
        boolean bBorTB           = false;
        boolean bBorButtom       = false;
    }
}