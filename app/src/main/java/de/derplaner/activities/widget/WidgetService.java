package de.derplaner.activities.widget;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.widget.RemoteViewsService;

import de.derplaner.SQLite.SQL_PlanerDataSource;
import de.derplaner.activities.widget.ListProvider;


/**
 * Created by Paradox on 03.01.2017.
 */

public class WidgetService extends RemoteViewsService {
/*
* So pretty simple just defining the Adapter of the listview
* here Adapter is ListProvider
* */private SQL_PlanerDataSource ds;

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        // Init
        ds                      = new SQL_PlanerDataSource(this);

        int appWidgetId = intent.getIntExtra(
                AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);

        return (new ListProvider(this.getApplicationContext(), intent, ds));
    }
}
