package de.derplaner.activities.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import java.util.ArrayList;

import de.derplaner.R;
import de.derplaner.SQLite.SQL_PlanerDataSource;
import de.derplaner.dataObjects.DS_Hour;

/**
 * Created by Paradox on 03.01.2017.
 */

public class ListProvider implements RemoteViewsService.RemoteViewsFactory {
    private ArrayList<String> listItemList = new ArrayList<>();
    private Context context = null;
    private int appWidgetId;
    private SQL_PlanerDataSource ds;

    public ListProvider(Context context, Intent intent, SQL_PlanerDataSource ds) {
        this.context = context;
        this.ds = ds;
        appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);

        populateListItem();
    }

    private void populateListItem() {
        /*ArrayList<DS_Hour> lList = ds.getAllEntries_HOURS("0");

        for (DS_Hour item: lList) {
            if(item.getDay() == 1)
            {
                String listItem;
                listItem = "Stunde: " + item.getSubject();
                listItemList.add(listItem);
            }
        }*/
    }

    @Override
    public int getCount() {
        return listItemList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     *Similar to getView of Adapter where instead of View
     *we return RemoteViews
     *
     */
    @Override
    public RemoteViews getViewAt(int position) {
        final RemoteViews remoteView = new RemoteViews(
                context.getPackageName(), R.layout.page_wdg);
        String listItem = listItemList.get(position);
        remoteView.setTextViewText(R.id.heading, listItem);
        remoteView.setTextViewText(R.id.content, listItem);

        return remoteView;
    }


    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void onCreate() {
    }

    @Override
    public void onDataSetChanged() {
        listItemList = new ArrayList<>();
        populateListItem();
    }

    @Override
    public void onDestroy() {
    }

}