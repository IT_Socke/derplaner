package de.derplaner.activities.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.derplaner.R;
import de.derplaner.SQLite.SQL_PlanerDataSource;

/**
 * Created by Paradox on 10.01.2017.
 */

public class wp_studIP extends AppWidgetProvider {
    private static final String btnPrev     = "PrevClick";
    private static final String btnNext     = "NextClick";
    private static final String llDay       = "LlDay";
    /*
     * this method is called every 30 mins as specified on widgetinfo.xml
     * this method is also called on every phone reboot
     */
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {

        for (int widgetId : appWidgetIds) {
            RemoteViews mView = initViews(context, appWidgetManager, widgetId);
            appWidgetManager.updateAppWidget(widgetId, mView);
            appWidgetManager.notifyAppWidgetViewDataChanged(widgetId, R.id.lv_studip);
        }


        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);

        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        String sDate = (new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN).format(Calendar.getInstance().getTime()));
        editor.putString("WDG_LBL_DATE", sDate);
        editor.commit();
    }

    private RemoteViews initViews(Context context,
                                  AppWidgetManager widgetManager, int widgetId) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);

        String sDate = pref.getString("WDG_LBL_DATE", "");
        String date2;
        RemoteViews mView = new RemoteViews(context.getPackageName(),
                R.layout.widget_studip);

        try {
            if (sDate.isEmpty()) {
                sDate = (new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN).format(Calendar.getInstance().getTime()));
                date2 = (new SimpleDateFormat("yyyyMMdd", Locale.GERMAN).format(Calendar.getInstance().getTime()));
            } else {
                date2 = sDate.substring(6, 10) + sDate.substring(3, 5) + sDate.substring(0, 2);
            }

            String day;
            try {
                day = (new SimpleDateFormat("EEEE", Locale.GERMAN).format((new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN).parse(sDate))));
            } catch (Exception ex) {
                day = (new SimpleDateFormat("EEEE", Locale.GERMAN).format(Calendar.getInstance().getTime()));
            }

            editor.putString("WDG_LBL_DATE", sDate);
            editor.putInt(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
            editor.commit();

            mView.setTextViewText(R.id.tv_wdgStud, day);
            mView.setTextViewText(R.id.tv_wdgStud2, sDate);

            mView.setOnClickPendingIntent(R.id.btnPrevious,
                    getPendingSelfIntent(context, btnPrev));
            mView.setOnClickPendingIntent(R.id.btnNext,
                    getPendingSelfIntent(context, btnNext));
            mView.setOnClickPendingIntent(R.id.ll_day,
                    getPendingSelfIntent(context, llDay));

            Intent adapter = new Intent(context, ws_studIP.class);
            adapter.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
            adapter.putExtra("DATE", date2);

            adapter.setData(Uri.parse(adapter.toUri(Intent.URI_INTENT_SCHEME)));
            mView.setRemoteAdapter(R.id.lv_studip, adapter);
        }
        catch (Exception ex) {
            int i = 0;
        }

        return mView;
    }

    protected PendingIntent getPendingSelfIntent(Context context, String action) {
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

    @Override
    public void onReceive(Context c, Intent intent) {
        super.onReceive(c, intent);

        try {
            if (btnPrev.equals(intent.getAction())) {
                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(c).edit();
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(c);

                String sDate = pref.getString("WDG_LBL_DATE", "");
                Integer iID = pref.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, 0);
                int[] appWidgetsIds = {iID};


                Calendar cal = Calendar.getInstance();
                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                Date date2;

                // Datum konvertieren
                try {
                    date2 = format.parse(sDate);
                } catch (ParseException e) {
                    date2 = Calendar.getInstance().getTime();
                }

                cal.setTime(date2);
                cal.add(Calendar.DATE, -1);

                String date4 = (new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN).format(cal.getTime()));

                editor.putString("WDG_LBL_DATE", date4);
                editor.commit();

                if (appWidgetsIds != null && appWidgetsIds.length > 0) {
                    this.onUpdate(c, AppWidgetManager.getInstance(c), appWidgetsIds);
                }
            } else if (btnNext.equals(intent.getAction())) {
                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(c).edit();
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(c);

                String sDate = pref.getString("WDG_LBL_DATE", "");
                Integer iID = pref.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, 0);
                int[] appWidgetsIds = {iID};


                Calendar cal = Calendar.getInstance();
                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN);
                Date date2;

                // Datum konvertieren
                try {
                    date2 = format.parse(sDate);
                } catch (ParseException e) {
                    date2 = Calendar.getInstance().getTime();
                }

                cal.setTime(date2);
                cal.add(Calendar.DATE, 1);

                String date4 = (new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN).format(cal.getTime()));

                editor.putString("WDG_LBL_DATE", date4);
                editor.commit();

                if (appWidgetsIds != null && appWidgetsIds.length > 0) {
                    this.onUpdate(c, AppWidgetManager.getInstance(c), appWidgetsIds);
                }
            } else if (llDay.equals(intent.getAction())) {
                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(c).edit();
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(c);

                Integer iID = pref.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, 0);
                int[] appWidgetsIds = {iID};

                String sDate = (new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN).format(Calendar.getInstance().getTime()));
                editor.putString("WDG_LBL_DATE", sDate);
                editor.commit();

                if (appWidgetsIds != null && appWidgetsIds.length > 0) {
                    this.onUpdate(c, AppWidgetManager.getInstance(c), appWidgetsIds);
                }
            }
        }
        catch (Exception ex) {
            int i = 0;
        }
    }
}