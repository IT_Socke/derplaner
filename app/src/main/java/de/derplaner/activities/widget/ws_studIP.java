package de.derplaner.activities.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import java.util.ArrayList;

import de.derplaner.R;
import de.derplaner.SQLite.SQL_PlanerDataSource;
import de.derplaner.dataObjects.DS_StudIP;


/**
 * Created by Paradox on 03.01.2017.
 */

public class ws_studIP extends RemoteViewsService {
/*
* So pretty simple just defining the Adapter of the listview
* here Adapter is ListProvider
* */private SQL_PlanerDataSource ds;

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        // Init
        ds                      = new SQL_PlanerDataSource(this);


        int appWidgetId = intent.getIntExtra(
                AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);

        return (new lp_StudIP(this.getApplicationContext(), intent, ds));
    }

    private class lp_StudIP implements RemoteViewsService.RemoteViewsFactory {
        private ArrayList<DS_StudIP> listItemList = new ArrayList<>();
        private Context context = null;
        private Intent inten;
        private int appWidgetId;
        private String day;
        private SQL_PlanerDataSource ds;

        lp_StudIP(Context context, Intent intent, SQL_PlanerDataSource ds) {
            this.context = context;
            this.ds = ds;
            //appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
            //AppWidgetManager.INVALID_APPWIDGET_ID);
            inten = intent;
            day = intent.getStringExtra("DATE");

            populateListItem();
        }

        private void populateListItem() {
            try {
                day = inten.getStringExtra("DATE");
                listItemList = ds.STUDIP_getAllEntries("0", day);
                int i = 0;
            }
            catch (Exception ex) {
                int i = 0;
            }
        }

        @Override
        public int getCount() {
            return listItemList.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        /*
         *Similar to getView of Adapter where instead of View
         *we return RemoteViews
         *
         */
        @Override
        public RemoteViews getViewAt(int position) {
            final RemoteViews remoteView = new RemoteViews(
                    context.getPackageName(), R.layout.page_wdgstudip);

            try {
                String sTime;
                String sStart, sEnd;
                remoteView.setTextViewText(R.id.tv_studSubject, listItemList.get(position).getSubject());
                remoteView.setTextViewText(R.id.tv_studRoom, listItemList.get(position).getRoom());

                sStart = String.valueOf(listItemList.get(position).getStartTime());
                sEnd = String.valueOf(listItemList.get(position).getEndTime());
                // TODO
                if(sEnd.length() == 3 && sStart.length() == 3)
                    sTime = sStart.substring(0, 1) + ":" + sStart.substring(1, 3) + " - " + sEnd.substring(0, 1) + ":" + sEnd.substring(1, 3);
                else if(sStart.length() == 3)
                    sTime = sStart.substring(0, 1) + ":" + sStart.substring(1, 3) + " - " + sEnd.substring(0, 2) + ":" + sEnd.substring(2, 4);
                else if(sEnd.length() == 3)
                    sTime = sStart.substring(0, 2) + ":" + sStart.substring(2, 4) + " - " + sEnd.substring(0, 1) + ":" + sEnd.substring(1, 3);
                else
                    sTime = sStart.substring(0, 2) + ":" + sStart.substring(2, 4) + " - " + sEnd.substring(0, 2) + ":" + sEnd.substring(2, 4);
                remoteView.setTextViewText(R.id.tv_studTime, sTime);
            }
            catch (Exception ex) {
                int i = 0;
            }

            return remoteView;
        }


        @Override
        public RemoteViews getLoadingView() {
            return null;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public void onCreate() {
        }

        @Override
        public void onDataSetChanged() {
            listItemList = new ArrayList<>();
            populateListItem();
        }

        @Override
        public void onDestroy() {
        }

    }
}
