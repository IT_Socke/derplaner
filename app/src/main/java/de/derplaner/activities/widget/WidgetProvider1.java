package de.derplaner.activities.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;

import de.derplaner.R;

/**
 * Created by Paradox on 10.01.2017.
 */

public class WidgetProvider1 extends AppWidgetProvider {

    /*
     * this method is called every 30 mins as specified on widgetinfo.xml
     * this method is also called on every phone reboot
     */
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {

        for (int widgetId : appWidgetIds) {
            RemoteViews mView = initViews(context, appWidgetManager, widgetId);
            appWidgetManager.updateAppWidget(widgetId, mView);
            appWidgetManager.notifyAppWidgetViewDataChanged(widgetId, R.id.lv1);
        }


        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }


    private RemoteViews initViews(Context context,
                                  AppWidgetManager widgetManager, int widgetId) {

        RemoteViews mView = new RemoteViews(context.getPackageName(),
                R.layout.widget_layout);

        Intent adapter = new Intent(context, WidgetService.class);
        adapter.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);

        adapter.setData(Uri.parse(adapter.toUri(Intent.URI_INTENT_SCHEME)));
        mView.setRemoteAdapter(R.id.lv1, adapter);

        return mView;
    }
}