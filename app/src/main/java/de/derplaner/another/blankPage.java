package de.derplaner.another;

import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.derplaner.R;

public class blankPage extends Fragment {
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState){
        super.onCreateView(inflater, container, savedInstanceState);

        // Initialisierung
        rootView        = inflater.inflate(R.layout.act_blank, container, false);

        return rootView;

    }
}