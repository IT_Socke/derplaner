package de.derplaner.another;

import android.graphics.Color;

import de.derplaner.R;

public class constant {
    // Setting Section
    public static String SET_WishGrade      = "SET_WISHGRADE";
    public static String SET_TTWeek         = "SET_TTWEEK";


    public static final int[] drws          = { R.drawable.speaking_black,
                                                R.drawable.pencil_black,
                                                R.drawable.document_black};
    public static final int[] colors        = { Color.BLUE,
                                                Color.RED,
                                                Color.GREEN};

    public static String[] TTWEEK = {"NON", "GUG", "ABC"};
}
